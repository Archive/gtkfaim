#include <newim.h>
#include <xpm/pixmaps.h>

curfont_t *curfont;
gchar font_name[] = "-adobe-courier-medium-r-normal--*-120-*-*-*-*-*-*";
gchar font_bold_name[] = "-adobe-courier-bold-r-normal--*-120-*-*-*-*-*-*";
im_window_t *im_window = NULL;

int main(int argc, char *argv[])
{
	/* Init everything */
	gtk_init(&argc, &argv);

	/* Create our window */
	create_window();

	/* Do our main loop */
	gtk_main();

	return 0;
}

void create_window()
{
	/* Make our variables */
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *txt_in;
	GtkWidget *tbar_font;
	GtkWidget *txt_out;
	GtkWidget *tbar_action;
	GtkWidget *toolbarbutton;
	GtkWidget *vscroll;
	GtkWidget *hbox;
	GdkColor tmp;
	GdkColormap *clrmap;
	
	/* Create our window */
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect(GTK_OBJECT(window), "destroy",
			GTK_SIGNAL_FUNC(destroy_window), (gpointer)window);
	gtk_window_set_title((GtkWindow *)window, "Quick Test");
   	gtk_window_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	
	/* Create a VBOX to hold everything */
	vbox = gtk_vbox_new(FALSE,0);
	gtk_widget_show(vbox);
	
	/* Create the hbox for the text and scroll */
	hbox = gtk_hbox_new(FALSE,0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE,TRUE,0);
	gtk_widget_show(hbox);
	
	/* Create the text in area */
	txt_in = gtk_text_new(NULL, NULL);
	gtk_text_set_editable(GTK_TEXT(txt_in), FALSE);
	gtk_box_pack_start(GTK_BOX(hbox), txt_in, TRUE,TRUE,0);
	gtk_widget_show(txt_in);
	
	/* Create the curfont struct */
	clrmap = gtk_widget_get_colormap(txt_in);
	tmp.red = 0;
	tmp.blue = 0;
	tmp.green = 0;
	gdk_color_alloc(clrmap, &tmp);

	curfont = g_new(struct cur_font_struct,1);
	curfont->font = txt_in->style->font;
	curfont->bold = 0;
	curfont->italic = 0;
	curfont->underline = 0;
	curfont->fg = tmp;
	curfont->bg = tmp;
	
	/* Create the VSCROLL for the text */
	vscroll = gtk_vscrollbar_new(GTK_TEXT(txt_in)->vadj);
	gtk_box_pack_start(GTK_BOX(hbox), vscroll, FALSE, FALSE,0);
	gtk_widget_show(vscroll);
	
	/* Create a toolbar for the font settings */
	tbar_font = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_ICONS);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"FG",
			"Foreground Text Color",
			NULL,
			new_pixmap(mini_fg_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"FG",
			"Foreground Text Color",
			NULL,
			new_pixmap(mini_fg_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"FG",
			"Foreground Text Color",
			NULL,
			new_pixmap(mini_fg_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	gtk_toolbar_append_space(GTK_TOOLBAR(tbar_font));
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"FG",
			"Foreground Text Color",
			NULL,
			new_pixmap(mini_fg_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"BG",
			"Background Text Color",
			NULL,
			new_pixmap(mini_bg_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	gtk_toolbar_append_space(GTK_TOOLBAR(tbar_font));
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"Bold",
			"Bold Text",
			NULL, 
			new_pixmap(mini_bold_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]), 
			tool_toggle_cb,
			&curfont->bold);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"Italic",
			"Italic Text",
			NULL, 
			new_pixmap(mini_italic_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			tool_toggle_cb,
			&curfont->italic);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"Underline",
			"Underlined Text",
			NULL,
			new_pixmap(mini_underline_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			tool_toggle_cb,
			&curfont->underline);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_font),
			"Link",
			"Insert Hypertext Link",
			NULL, NULL,
	/*		new_pixmap(warn_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]), */
			NULL,
			toolbarbutton);
	gtk_toolbar_set_button_relief(GTK_TOOLBAR(tbar_font), GTK_RELIEF_NONE);
	gtk_toolbar_set_space_style(GTK_TOOLBAR(tbar_font), GTK_TOOLBAR_SPACE_LINE);
	/* I like the font stuff center so we'll put it in a hbox */
	hbox = gtk_hbox_new(TRUE,0);
	gtk_box_pack_start(GTK_BOX(hbox), tbar_font, FALSE,FALSE,0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE,0);
	gtk_widget_show(tbar_font);
	gtk_widget_show(hbox);
	
	/* Create the HBOX for the text and scroller */
	hbox = gtk_hbox_new(FALSE,0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE,TRUE,0);
	gtk_widget_show(hbox);
	
	/* Create the out text window */
	txt_out = gtk_text_new(NULL, NULL);
	gtk_text_set_editable(GTK_TEXT(txt_out), TRUE);
	gtk_box_pack_start(GTK_BOX(hbox), txt_out, TRUE,TRUE,0);
	gtk_widget_show(txt_out);
	
	/* Create the scrollbar */
	vscroll = gtk_vscrollbar_new(GTK_TEXT(txt_out)->vadj);
	gtk_box_pack_start(GTK_BOX(hbox), vscroll, FALSE,FALSE,0);
	gtk_widget_show(vscroll);
	
	/* Create the action toolbar */
	tbar_action = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_action),
			"Warn",
			"Warn User",
			NULL,
			new_pixmap(warn_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_action),
			"Block",
			"Block User",
			NULL,
			new_pixmap(block_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	gtk_toolbar_append_space(GTK_TOOLBAR(tbar_action));
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_action),
			"Add Buddy",
			"Add Buddy to Buddylist",
			NULL,
			new_pixmap(add_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_action),
			"Close",
			"Close This Window",
			NULL,
			NULL,
			destroy_window,
			window);
	gtk_toolbar_append_space(GTK_TOOLBAR(tbar_action));
	toolbarbutton = gtk_toolbar_append_item( GTK_TOOLBAR(tbar_action),
			"Send",
			"Send Message",
			NULL,
			new_pixmap(arrow_xpm, window->window, &window->style->bg[GTK_STATE_NORMAL]),
			NULL,
			NULL);
	gtk_toolbar_set_space_style(GTK_TOOLBAR(tbar_action), GTK_TOOLBAR_SPACE_LINE);
	gtk_toolbar_set_button_relief(GTK_TOOLBAR(tbar_action), GTK_RELIEF_NONE);
	gtk_box_pack_start(GTK_BOX(vbox), tbar_action, FALSE, TRUE, 2);
	gtk_widget_show(tbar_action);
	
	/* Pack the vbox into the app */
	gtk_container_add(GTK_CONTAINER(window), vbox);
	
	im_window = g_new(struct im_window_struct,1);
	
	im_window->window = window;
	im_window->txt_in = txt_in;
	im_window->txt_out = txt_out;

	gtk_widget_show(window);
	
	return;
	
}

void destroy_window(GtkWidget *widget, gpointer data)
{
	/* Free everything */
	gtk_widget_destroy(im_window->window);

	/* Free the font */
	g_free(curfont);
	
	/* Free the window struct */
	g_free(im_window);
	
	exit ( 1 );
}

GtkWidget* new_pixmap (gchar *filename[], GdkWindow *window, GdkColor  *background)
{
  GtkWidget *wpixmap;
  GdkPixmap *pixmap;
  GdkBitmap *mask;
  pixmap = gdk_pixmap_create_from_xpm_d (window, &mask, background,filename);
  wpixmap = gtk_pixmap_new (pixmap, mask);
  return wpixmap;
}

void tool_toggle_cb (GtkWidget *widget, gpointer data)
{
	gint *option;

	option = data;
	switch (gtk_button_get_relief(GTK_BUTTON(widget)))
	{
	case GTK_RELIEF_NORMAL:
	{
     		gtk_button_set_relief(GTK_BUTTON(widget), GTK_RELIEF_NONE);
		*option = 0;
		break;
	}
	case GTK_RELIEF_NONE:
	{
		gtk_button_set_relief(GTK_BUTTON(widget), GTK_RELIEF_NORMAL);
		*option = 1;
		break;
	}
	default:
	{
		g_print("Uhhh... why am I here?!\n");
		gtk_button_set_relief(GTK_BUTTON(widget), GTK_RELIEF_NONE);
		*option = 0;
		break;
	}
	}

	if (curfont->bold)	
		curfont->font = gdk_font_load(font_bold_name);
	else
		curfont->font = gdk_font_load(font_name);
	
	
	g_print("Current Settings:\n");
	g_print("bold:\t\t%d\n", curfont->bold);
	g_print("italic:\t\t%d\n", curfont->italic);
	g_print("underline:\t%d\n\n", curfont->underline);
	return;
}

void tool_test(GtkWidget *widget, gpointer data)
{
	
	return;
}
