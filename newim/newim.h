#include <stdio.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>
#include <warn.xpm>

typedef struct cur_font_struct 		curfont_t;
typedef struct im_window_struct		im_window_t;

struct cur_font_struct
{
	GdkFont *font;
	gint bold;
	gint italic;
	gint underline;
	gint size;
	GdkColor fg;
	GdkColor bg;
};

struct im_window_struct
{
	GtkWidget *window;
	GtkWidget *txt_in;
	GtkWidget *txt_out;
};

extern im_window_t *im_window;

void create_window();
void destroy_window(GtkWidget *, gpointer);
GtkWidget *new_pixmap(gchar *[], GdkWindow *, GdkColor *);
void tool_toggle_cb(GtkWidget *, gpointer);
void tool_test(GtkWidget *, gpointer);
