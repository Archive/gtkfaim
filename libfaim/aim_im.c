/*
  aim_im.c

  The routines for sending/receiving Instant Messages.

 */

#include "aim.h"


int aim_send_im(char *destsn, char *msg)
{   

  int i;
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1; /* lock struct */
  newpacket.type = 0x02; /* IMs are always family 0x02 */
  
  i = strlen(destsn); /* used as offset later */
  
  newpacket.commandlen = 20+1+strlen(destsn)+1+1+2+7+2+4+strlen(msg);

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x04;      
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x06;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  
  /* [6] through [17] are msg specific, but 0x00's are fine  */
#if 0
  newpacket.data[6] = 0x00;
  newpacket.data[7] = 0x00;
  newpacket.data[8] = 0x00;
  newpacket.data[9] = 0x00;
#endif
  newpacket.data[6] = (aim_snac_nextid >> 24) & 0xFF;
  newpacket.data[7] = (aim_snac_nextid >> 16) & 0xFF;
  newpacket.data[8] = (aim_snac_nextid >>  8) & 0xFF;
  newpacket.data[9] = (aim_snac_nextid) & 0xFF;

  newpacket.data[10] = 0x00;
  newpacket.data[11] = 0x00;
  newpacket.data[12] = 0x00;
  newpacket.data[13] = 0x00;

  newpacket.data[14] = 0x00;
  newpacket.data[15] = 0x00;
  newpacket.data[16] = 0x00;
  newpacket.data[17] = 0x00;
  
  newpacket.data[18] = 0x00;
  newpacket.data[19] = 0x01;
  
  newpacket.data[20] = i;
  memcpy(&(newpacket.data[21]), destsn, i);
  newpacket.data[21+i] = 0x00;
  
  newpacket.data[22+i] = 0x02;
  
  newpacket.data[23+i] = (char) ( (strlen(msg) + 0xD) >> 8);
  newpacket.data[24+i] = (char) ( (strlen(msg) + 0xD) & 0xFF);
  
  newpacket.data[25+i] = 0x05;
  newpacket.data[26+i] = 0x01;
  newpacket.data[27+i] = 0x00;
  newpacket.data[28+i] = 0x01;
  newpacket.data[29+i] = 0x01;
  newpacket.data[30+i] = 0x01;
  newpacket.data[31+i] = 0x01;
  
  newpacket.data[32+i] = (char) ( (strlen(msg) + 4) >> 8);
  newpacket.data[33+i] = (char) ( (strlen(msg) + 4) & 0xFF);
  
  newpacket.data[34+i] = 0x00;
  newpacket.data[35+i] = 0x00;
  newpacket.data[36+i] = 0x00;
  newpacket.data[37+i] = 0x00;
  
  memcpy(&(newpacket.data[38+i]), msg, strlen(msg));
  
  aim_tx_enqueue(&newpacket);
#ifdef USE_SNAC_FOR_IMS
 {
    struct aim_snac_t snac;

    snac.id = aim_snac_nextid;
    snac.family = 0x0004;
    snac.type = 0x0006;
    snac.flags = 0x0000;

    snac.data = malloc(strlen(destsn)+1);
    memcpy(snac.data, destsn, strlen(destsn)+1);

    aim_newsnac(&snac);
  }

 aim_cleansnacs(60); /* clean out all SNACs over 60sec old */
#endif

  return (aim_snac_nextid++);
}


int aim_send_im_away(char *destsn, char *msg)
{   

  int i;
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1; /* lock struct */
  newpacket.type = 0x02; /* IMs are always family 0x02 */
  
  i = strlen(destsn); /* used as offset later */
  
  newpacket.commandlen = 20+1+strlen(destsn)+4+1+1+2+7+2+4+strlen(msg);

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x04;      
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x06;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  
  /* [6] through [17] are msg specific, but 0x00's are fine  */
  newpacket.data[6] = 0x00;
  newpacket.data[7] = 0x00;
  newpacket.data[8] = 0x00;
  newpacket.data[9] = 0x00;
  newpacket.data[10] = 0x00;
  newpacket.data[11] = 0x00;
  newpacket.data[12] = 0x00;
  newpacket.data[13] = 0x00;
  newpacket.data[14] = 0x00;
  newpacket.data[15] = 0x00;
  newpacket.data[16] = 0x00;
  newpacket.data[17] = 0x00;
  
  newpacket.data[18] = 0x00;
  newpacket.data[19] = 0x01;
  
  newpacket.data[20] = i;
  memcpy(&(newpacket.data[21]), destsn, i);

  /* add TLV t(0004) l(0000) v(NULL) */
  newpacket.data[21+i] = 0x00;
  newpacket.data[22+i] = 0x04;
  newpacket.data[23+i] = 0x00;
  newpacket.data[24+i] = 0x00;
  i += 4;

  newpacket.data[21+i] = 0x00;
  
  newpacket.data[22+i] = 0x02;
  
  newpacket.data[23+i] = (char) ( (strlen(msg) + 0xD) >> 8);
  newpacket.data[24+i] = (char) ( (strlen(msg) + 0xD) & 0xFF);
  
  newpacket.data[25+i] = 0x05;
  newpacket.data[26+i] = 0x01;
  newpacket.data[27+i] = 0x00;
  newpacket.data[28+i] = 0x01;
  newpacket.data[29+i] = 0x01;
  newpacket.data[30+i] = 0x01;
  newpacket.data[31+i] = 0x01;
  
  newpacket.data[32+i] = (char) ( (strlen(msg) + 4) >> 8);
  newpacket.data[33+i] = (char) ( (strlen(msg) + 4) & 0xFF);
  
  newpacket.data[34+i] = 0x00;
  newpacket.data[35+i] = 0x00;
  newpacket.data[36+i] = 0x00;
  newpacket.data[37+i] = 0x00;
  
  memcpy(&(newpacket.data[38+i]), msg, strlen(msg));
  
  aim_tx_enqueue(&newpacket);

  return 0;
  
}


/*
  parse_incoming_im(struct command_rx_struct *, struct connection_info_struct *)

  Takes a packet (normally from parse_generic()) and assumes its a 
  incoming IM.  For now, prints some stuff out.  

  Incoming IMs look like this one: (from midendian "hello")
     2a  2 86 31  0 94        FLAP hdr

      0  4  0  7  0  0        SNAC hdr
      81 29 6d d1             SNAC hdr (reqid)

                 fe 1e       \__ Cookie
     5b 64 2e 6a d7 5e       /       Cookie, continued

      0  1                   ICBM channel ID

      9 6d 69 64 65 6e 64 69 \__  Screen 
     61 6e                   /       Name (length byte prefix)

      0  0                    warning level

      0  4                    # of upcoming TLVs (4 for free/trial, 3 for AOL)

      0  1                    \
      0  2                     | -- class TLV
      0 10                    / 

      0  2                    \
      0  4                     | -- "member since" TLV
     35 66 7e f2              /

      0  3                    \
      0  4                     | -- "on since" TLV
     35 a2 79 1a              /

      0  4                    \
      0  2                     | -- idle time TLV (not present for AOL members)
      0  0                    /

      0  2                    \
      0 52                     |
      5  1  0  1  1  1  1      |
      0 49                     |
      0  0  0  0               |
     3c 48 54                  |
     4d 4c 3e 3c 46 4f 4e 54   |
     20 43 4f 4c 4f 52 3d 22   | -- ICBM TLV
     23 30 30 30 30 30 30 22   |
     20 42 41 43 4b 3d 22 23   |
     66 66 66 66 66 66 22 20   |
     53 49 5a 45 3d 33 3e 68   |
     65 6c 6c 6f 3c 2f 46 4f   |
     4e 54 3e 3c 2f 48 54 4d   |
     4c 3e                    /

 */

int aim_parse_incoming_im(struct command_rx_struct *command)
{
  int i = 0;
  int g = 0;
  char *srcsn = NULL;
  char *msg = NULL;
  unsigned int msglen = 0;

  /* [26] is SN length */
  /* [27] - [27 + $26] is SN */
  /* [27 + $26 + 49] is msg */

  i = 20;
  if ((srcsn = (char *) malloc( (command->data[i]) + 1)) == NULL)
    {
      printf("ERROR ON malloc(srcsn)\n");
      return -1;
    }
  memcpy(srcsn, &(command->data[i+1]), command->data[i]);
  srcsn[command->data[i]] = '\0';

  printf("\nGot IM from %s: ", srcsn);

  i += (int) command->data[i];
  i += 15; /* arbitrary value */

  /* I really don't like this, but it's the simplest way */
  g = i;
  while (command->data[g] != 0x3c)  /* hopefully the first 0x3c will be in the msg */
    g++;
  
  /* g should hold the index of the first byte of the message */
  msglen = ( (( (unsigned int) command->data[g-6]) & 0xFF ) << 8);
  msglen += ( (unsigned int) command->data[g-5]) & 0xFF; /* mask off garbage */
  msglen -= 4; /* skip four 0x00s */

  if ((msg = (char *) malloc( (msglen + 1) )) == NULL)
    {
      printf("ERROR ON malloc(msg)\n");
      return -1;
    }
  memcpy(msg, &(command->data[g]), msglen);
  msg[msglen] = '\0'; 
  printf("%s\n\n", msg);

  /* send the message back to the sender */
  aim_send_im(srcsn, msg);

  /* free */
  free(srcsn);
  free(msg);
  
  return 0;
}
