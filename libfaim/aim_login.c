/*
  aim_login.c

  This contains all the functions needed to actually login.

 */

#include "aim.h"
#ifdef TIS_TELNET_PROXY
#include "tis_telnet_proxy.h"
#endif

int aim_login(char *sn, char *password)
{
  int ret;
  struct sockaddr_in sa;
  struct hostent *hp;
  struct sockaddr_in sa2;
  struct hostent *hp2;

  /* connection1 is local only -- for authentication block */
  struct connection_info_struct connection1;

  char loginserver[]={FAIM_LOGIN_SERVER};  /* login servers name */
  char loginresp[LOGIN_RESP_LEN];    /* destbuf for get_login_response() */
  struct login_phase1_struct loginresp_struct = {NULL,NULL,NULL};

  /* xxx.xxx.xxx.xxx = 15b + NULL */
  char conn2_name[15+1];

#ifdef TIS_TELNET_PROXY
  char *proxyserver=TIS_TELNET_PROXY;
  hp = gethostbyname(proxyserver);
  memset(&sa.sin_zero, 0, 8);
  sa.sin_port = htons(23);
  memcpy(&sa.sin_addr, hp->h_addr, hp->h_length);
  sa.sin_family = hp->h_addrtype;
#else
  hp = gethostbyname(loginserver); 
  memset(&sa.sin_zero, 0, 8);
  sa.sin_port = htons(FAIM_LOGIN_PORT);
  memcpy(&sa.sin_addr, hp->h_addr, hp->h_length);
  sa.sin_family = hp->h_addrtype;
#endif /* TIS_TELNET_PROXY */

#if debug > 0
  printf("Authenticating...");
#endif

#if debug > 0
  printf ("\nConnecting to server (%s)...", FAIM_LOGIN_SERVER);
#endif
  connection1.fd = socket(hp->h_addrtype, SOCK_STREAM, 0);
  ret = connect(connection1.fd, (struct sockaddr *)&sa, sizeof(struct sockaddr_in));
  if( ret < 0 ) 
  {
     perror("connect");
     return AIM_CONNECT_ERROR;
  }
#if debug > 0
  printf("done.\n");
#endif


#ifdef TIS_TELNET_PROXY
  tis_telnet_proxy_connect(connection1.fd, loginserver,FAIM_LOGIN_PORT);
#endif

  aim_send_login(connection1.fd, sn, password); /* 317b ok -- A */
  aim_get_login_response(connection1.fd, loginresp);  /* 10b + 324b ok -- B+C */
  
  close(connection1.fd);  /* we should be done with this one now */

  /* Do all sorts of bizarre parsing stuff (see the comments on code below) */
  ret = aim_parse_login_response(&loginresp_struct, loginresp);
  if( ret != 0 )
     return ret;

  /* Read out IP number into local string */
  memcpy(conn2_name, loginresp_struct.nextIP, 16);
#if debug > 0
  printf("The IP number for conn2 is: %s\n", conn2_name);
#endif

  /* Screen name */
#if debug > 0
  printf("Your SN is: %s\n", loginresp_struct.screen_name);
#endif

  aim_connection.sn = loginresp_struct.screen_name;

  {
    /* hopefully this fixes a really stupid corruption problem... */
    int z = 0;
    z = strlen(loginresp_struct.screen_name);
    aim_connection.sn = (char *) malloc(z+1);
    memcpy(aim_connection.sn, loginresp_struct.screen_name, z);
    aim_connection.sn[z] = '\0';
  }

#if debug > 0
  printf("done.\n");
#endif 

  /* these are hardcoded for now */
  aim_connection.local_seq_num_origin = 0xabe4;  /* picked at random, btw */

#if debug > 0
  printf("Starting main AIM service...");
#endif

#ifdef TIS_TELNET_PROXY
  hp2 = gethostbyname(proxyserver);
  memset(&sa2.sin_zero, 0, 8);
  sa2.sin_port = htons(23);
  memcpy(&sa2.sin_addr, hp2->h_addr, hp2->h_length);
  sa2.sin_family = hp->h_addrtype;
#else
  hp2 = gethostbyname(conn2_name); 
  memset(&sa2.sin_zero, 0, 8);
  sa2.sin_port = htons(FAIM_LOGIN_PORT);
  memcpy(&sa2.sin_addr, hp2->h_addr, hp2->h_length);
  sa2.sin_family = hp2->h_addrtype;
#endif /* TIS_TELNET_PROXY */

#if debug > 0
  printf ("Connecting to second server (%s)...", conn2_name);
#endif
  aim_connection.fd = socket(hp2->h_addrtype, SOCK_STREAM, 0);
  ret = connect(aim_connection.fd, (struct sockaddr *)&sa2, sizeof(struct sockaddr_in));
  if( ret < 0 ) 
  {
     perror("connect conn2");
     return AIM_CONNECT_ERROR;
  }
#if debug > 0
  printf("done.\n");
#endif

  /* Phase 2 */
#ifdef TIS_TELNET_PROXY
  tis_telnet_proxy_connect(aim_connection.fd, conn2_name, FAIM_LOGIN_PORT);
#endif
  aim_send_login_phase2(aim_connection.fd, &loginresp_struct);

  /* Free up */
  free(loginresp_struct.nextIP);
  free(loginresp_struct.packetData);
  free(loginresp_struct.email);

  return 0;
}

/*
  send_login(int socket, char *sn, char *password)
  
  This is the initial login request packet.

  This normally contains a screen name (sn), a password,
  and some client information.  The latter is left nearly
  blank (it's got four zeros in it).  AIM Java puts in a 
  bunch of junk like the client version, the amount of 
  free memory your machine has, etc...

  The password is encoded before transmition, as per
  encode_password().  See that function for their
  stupid method of doing it.

*/

int aim_send_login (int s, char *sn, char *password)
{
  /* this is for the client info field of this packet.  for now, just
     put a few zeros in there and hope they don't notice. */
  char info_field[] = {
    0x00, 0x00, 0x00, 0x00
  };
  int info_field_len = 4;

  fd_set fds; /* for select() */
  struct timeval tv; /* for select() */
  char *password_encoded = NULL;  /* to store encoded password */
  char *new_packet_login = NULL; /* ptr to packet buffer for construction */
  int new_packet_login_len = 0; /* packet buffer length */

  int n = 0; /* counter during packet construction */

  /* breakdown of new_packet_login_len */
  new_packet_login_len  = 4; /* FLAP: header */
  new_packet_login_len += 2; /* FLAP: SNAC len */
  new_packet_login_len += 6; /* SNAC: fixed bytes */
  new_packet_login_len += 2; /* SN len */
  new_packet_login_len += strlen(sn); /* SN text */
  new_packet_login_len += 1; /* SN null terminator */
  new_packet_login_len += 1; /* fixed byte */
  new_packet_login_len += 2; /* password len */
  new_packet_login_len += strlen(password); /* password text */
  new_packet_login_len += 1; /* password null term*/
  new_packet_login_len += 1; /* fixed byte */
  new_packet_login_len += 2; /* info field len */
  new_packet_login_len += info_field_len; /* info field text */
  new_packet_login_len += 1; /* info field null term */
  new_packet_login_len += 41; /* fixed bytes */

  /* allocate buffer to use for constructing packet_login */
  new_packet_login = (char *) malloc ( new_packet_login_len );
  
  /* FLAP header */
  new_packet_login[0] = 0x2a; /* command start */
  new_packet_login[1] = 0x01; /* family 0x01 */
  new_packet_login[2] = 0x6b; /* sequence number */
  new_packet_login[3] = 0x02; /* sequence number byte 2 */

  /* SNAC length word */
  new_packet_login[4] = (char) ( (new_packet_login_len - 6) >> 8);
  new_packet_login[5] = (char) ( (new_packet_login_len - 6) & 0xFF);

  /* SNAC */
  new_packet_login[6] = 0x00;
  new_packet_login[7] = 0x00;
  new_packet_login[8] = 0x00;
  new_packet_login[9] = 0x01;
  new_packet_login[10] = 0x00;
  new_packet_login[11] = 0x01;

  new_packet_login[12] = (char) ( (strlen(sn)) >> 8);
  new_packet_login[13] = (char) ( (strlen(sn)) & 0xFF);

  n = 14;
  memcpy(&(new_packet_login[n]), sn, strlen(sn));
  n += strlen(sn);
  new_packet_login[n] = 0x00;
  n++;

  new_packet_login[n] = 0x02;
  n++;

  /* store password length as word */
  new_packet_login[n] = (char) ( (strlen(password)) >> 8);
  new_packet_login[n+1] = (char) ( (strlen(password)) & 0xFF);
  n += 2;

  /* allocate buffer for encoded password */
  password_encoded = (char *) malloc(strlen(password));
  /* encode password */
  aim_encode_password(password, password_encoded);
  /* store encoded password */
  memcpy(&(new_packet_login[n]), password_encoded, strlen(password));
  n += strlen(password);
  /* free buffer */
  free(password_encoded);
  /* place null terminator after encoded password */
  new_packet_login[n] = 0x00;
  n++;

  new_packet_login[n] = 0x03;
  n++;

  new_packet_login[n] = (char) ( (info_field_len) >> 8);
  new_packet_login[n+1] = (char) ( (info_field_len) & 0xFF);
  n += 2;
  memcpy(&(new_packet_login[n]), info_field, info_field_len);
  n += info_field_len;
  new_packet_login[n] = 0x00;
  n++;

  new_packet_login[n] = 0x16;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x02;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x01;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x17;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x02;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x01;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x18;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x02;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x01;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x1a;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x02;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x13;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x0e;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x02;
  new_packet_login[n+3] = 0x75;
  n += 4;

  new_packet_login[n] = 0x73;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x0f;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x02;
  new_packet_login[n+1] = 0x65;
  new_packet_login[n+2] = 0x6e;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x09;
  new_packet_login[n+1] = 0x00;
  new_packet_login[n+2] = 0x02;
  new_packet_login[n+3] = 0x00;
  n += 4;

  new_packet_login[n] = 0x15;
  n += 1;

#if debug > 0
  printf("Sending new_packet_login...");
#endif

  FD_ZERO(&fds);
  FD_SET(s, &fds);
  tv.tv_sec = 0;
  tv.tv_usec = 5;

  FD_ZERO(&fds);
  FD_SET(s, &fds);
  tv.tv_sec = 0;
  tv.tv_usec = 5;
  
  if(select(s+1, NULL, &fds, NULL, &tv) == 1)
    if ( write(s, new_packet_login, new_packet_login_len) != new_packet_login_len)
	printf("\n****ERROR ON WRITE**** (send_login)\n");

#if debug > 0
  printf("done.\n");
#endif
  
  free(new_packet_login);
  return 0;
}

/*
  int encode_password(
                      const char *password,
		      char *encoded
		      );

  This takes a const pointer to a (null terminated) string
  containing the unencoded password.  It also gets passed
  an already allocated buffer to store the encoded password.
  This buffer should be the exact length of the password without
  the null.  The encoded password buffer IS NOT NULL TERMINATED.

  The encoding_table seems to be a fixed set of values.  We'll
  hope it doesn't change over time!  

 */

int aim_encode_password(const char *password, char *encoded)
{
  char encoding_table[] = {
    0xf3, 0xb3, 0x6c, 0x99,
    0x95, 0x3f, 0xac, 0xb6,
    0xc5, 0xfa, 0x6b, 0x63,
    0x69, 0x6c, 0xc3, 0x9f
  };

  int i;
  
  for (i = 0; i < strlen(password); i++)
      encoded[i] = (password[i] ^ encoding_table[i]);

  return 0;
}


/* 
   get_login_response(int socket, char *buf)

   So theoretically here, Oscar's suppose to send me 334bytes
   in a sign of confirmation of the last request (phase1 login).
   This will hopefully only be 334 bytes.  At least, that's what
   karpski says it should be...  Hmm...I'll just allow for a
   larger packet just in case...

   Anyway...I haven't the slightest idea what all this data is for.
   I know there's a little at beginning to send back the login name,
   but there's also an IP number encoded there as well. The rest is
   incomprehensible.  
   
   The big thing I do know is that there's actually two AIM command
   sequences here.  The first (0 <= i <= 9) is a confirmation of our
   intial login packet above.  The next (10 <= i <= 333) is a 
   who-knows-what packet (but not quite as bad as the one we get to
   send back next!).  Each command sequence is always initiated with
   an asterik (0x2a), then a status code (0x00, 0x01, or 0x04). See
   the spec for more info.

*/
int aim_get_login_response(int s, char *loginresp)
{
  int i;
  int j;
  int a = 0;
  fd_set fds;
  struct timeval tv;
  unsigned char first[15];
  unsigned char second[350];
  unsigned char tmphdr[6];
  
#if debug > 0
  printf("Reading login response...");
#endif
  
  FD_ZERO(&fds);
  FD_SET(s, &fds);
  tv.tv_sec = 1;
  tv.tv_usec = 0;

  i = Read(s, first, 10);
  if (first[0] != 0x2a)
    printf("\afirst:INVALID FLAP\n");
  j = Read(s, tmphdr, 6);

  if (tmphdr[0] != 0x2a)
    printf("\asecondhdr:INVALID FLAP\n");

  if (tmphdr[1] != 0x04)
    printf("\asecondhdr:WRONG CHANNEL\n");

  a = (tmphdr[4] << 8) & 0xFF00;
  a += (tmphdr[5]) & 0x00FF;
#if debug > 1
  printf("read header (should now read 0x%04x bytes (%02x+%02x))\n", a, tmphdr[4], tmphdr[5]);
#endif

  FD_ZERO(&fds);
  FD_SET(s, &fds);
  tv.tv_sec = 1;
  tv.tv_usec = 0;

  j = 0;
  /* PLEASE READ: there's major timing issues that happen here!  make sure
     you wait until you've read at least 300b before you leave, or
     you'll get unreliable logins when OSCAR is busy!  */
#if debug > 2
  printf("\t");
#endif
  while (  (select(s+1, &fds, NULL, NULL, &tv) == 1)  || (j < a) )
    {
      read(s, &(second[j]), 1);
#if debug > 2
      if (j % 8 == 0)
	printf("\n\t");
      if (second[j] < 127 && second[j] >= ' ')
	 printf("%c=%02x ", second[j], second[j]);
      else
	 printf("0x%02x ", second[j]);

#endif
      j++;
      FD_ZERO(&fds);
      FD_SET(s, &fds);
      tv.tv_sec=2;
      tv.tv_usec=2;
    }


#if debug > 0
  printf(" done. (%d+%db read)\n", i, j);
#endif

  {
    int z = 0;
    
    memcpy(&(loginresp[z]), first, 10);
    z+=10;
    memcpy(&(loginresp[z]), tmphdr, 6);
    z+=6;
    memcpy(&(loginresp[z]), second, a);
  }

  /* Sanity checking is done in parse_login_response() */

  return 0;
}

/* int parse_login_response (struct login_phase1_struct *, 
                             struct connection_info_struct *,
                             char *)

   This has got to be by far the worst loop I've ever written.  The point
   here is to parse and extract as much information we can out of this
   packet (which, btw, contains two seperate command sequences).  Both
   command sequences must be processed seperatly, which is the purpose
   of the dual-loop.  Any extraneous characters are reported.  I've also
   gone to great lengths here to test every byte of the protocol specification.
   If any non-conformant sequences are sent, we'll have to add to or 
   modify the running documentation.  Hopefully, we know enough by now
   that we don't have to do that.  One failure here is that we don't 
   verify that the string sizes are reasonable before we malloc and memcpy.
   Everything else is checked.  I doubt I'll win any efficiency awards
   with this one...

   The purpose of the two seperate counters is this.  i is always relative
   to the beginning of the packet.  It's a straight increment.  j on the
   other hand, jumps around in order to make it appear as though all the
   strings were fixed and is always relative to the beginning of the
   current command block (not from the beginning of the packet).  This is
   so I can keep the byte specifications nice and clean as offsets to j,
   then just modify j after each variable length string to make it look as
   though the string was only 1 byte long.  I hope I conformed to my own
   standard.  This got confusing real quick.

   commandnum keeps track of only the sequences parsed in this function.
   The items in the connection_info_struct are meant for keeping track
   of the source ids, sequence number origins, overall command count,
   etc.  The login_phase1_struct is for keeping track off all the
   information gathered in the first phase of the login process.  This
   may expand as needed.  After each command sequence is identified,
   the global login_phase1_struct->command_count is incremented.

*/

int aim_parse_login_response (struct login_phase1_struct *loginrespstruct, char *loginresp)
{
  int commandnum = 0; /* our current command (if there's more than one/resp */
  int i = 0;
  int j = 0; /* a temp used by some handlers -- set to 0 before use */

  while (i < LOGIN_RESP_LEN)
    {
      while ( (loginresp[i] != 0x2a) && (i < LOGIN_RESP_LEN))
	i++;
      
      /* Just in case */
      if ( (commandnum == 0) && (i != 0))
	printf("parse_login_response: ignoring %d of preceding garabage\n", i);
      
      commandnum++;
      /* We'll assume that this is the right packet -- should be the 10ber */
      if (commandnum == 1)
	{
#if debug > 1
	  printf("parse_login_response: reading packet of family 0x%x...\n", loginresp[i+1]);
#endif
	  if (loginresp[i+4] != 0x00)
	    printf("parse_login_response: unexpected value on byte i+4: 0x%x != 0x%x\n", loginresp[i+4], 0x00);
	  if (loginresp[i+5] != 0x04) /* if proper packet this = 0x04 */
	    printf("parse_login_response: info length mismatch: probably interpreting as wrong packet type/family!\n");
	  if ( (loginresp[i+6] != 0x00) ||
	       (loginresp[i+7] != 0x00) ||
	       (loginresp[i+8] != 0x00) ||
	       (loginresp[i+9] != 0x01) )
	    printf("parse_login_response: info field wrong for this packet type\n");
	  i++;
	}
      if (commandnum == 2) /* This should be the 324b packet */
	{
	  /* make sure we really should be here */
	  if (loginresp[i] != 0x2a)
	    printf("parse_login_response: ahh! reached login FLAP handler with an impropper packet!\n");

#if debug > 1
	  printf("parse_login_response: reading packet on chan 0x%x...\n", loginresp[i+1]);
#endif

	  /* i'm still wondering why i put this here -- it does no good*/
	  if (loginresp[i+1] == AIM_SERVICE_FULL)
          {
	      printf("parse_login_response: got a \"service full\" error, giving up\n");
	      return AIM_SERVICE_FULL;
          }
	 
	   
	  /* check for reservation error */
#ifndef NORESERVATION
	  if (loginresp[i+4] != 0x01)
	    {
	      if (((u_char)loginresp[i+4]) < 0x87)
		{
		  printf("parse_login_response: probably (reservation?) error in login: (i+4) -- exiting to find salvation\n");
		  return -1;
		}
	      else
		printf("parse_login_response: unexpected value on byte i+4: 0x%x != 0x%x\n", loginresp[i+4], 0x01);
	    }
#else
#if debug > 0
	  printf("aim_login.c: warning: compiled with NORESERVATION -- reservation errors may not get detected properly!\n");
#endif
#endif
	  /* ignore info field length at loginresp[i+5] */

	  /* a little more spec-checking */
	  if ( (loginresp[i+6] != 0x00) ||
	       (loginresp[i+7] != 0x01) ||
	       (loginresp[i+8] != 0x00) )
	    printf("parse_login_response: info field wrong for this packet type\n"); 
	  
	  /* byte +9 is the screen name length w/o terminator */
	  /*    SN length must be (4 <= x <= 10) */
	  if ( (loginresp[i+9] >= 4) &&
	       (loginresp[i+9] <= 10) )
	    {
	      loginrespstruct->screen_name = (char *) malloc(sizeof(char) * (loginresp[i+9] + 1));
	      memcpy(loginrespstruct->screen_name, &(loginresp[i+10]), (loginresp[i+9] + 1));
	    }
	  else
	    {
	      printf("parse_login_response: snlength field invalid! (0x%x)\n", loginresp[i+9]);
	      return -1;
	    }
	  /* setup j relative to i for use past variable length 
	     strings (i will not be correct) */
	  j = i + (loginresp[i+9] + 1);

	  if (loginresp[j+10] != 0x05)
	    {
	      if (loginresp[j+10] == 0x04)
		{
		  /* Right now, we can identify an error, but not specifically
		     But, we can read out the URL for message and printf it.
		     This should really just pass the rest of the packet
		     onto to a specific error handler.
		  */
		  printf("PRESUMED ERROR CONDITION FROM OSCAR (j+10 = 0x04)\n");
		  printf("Please refer to %s.\n", &(loginresp[j+13]));
		  return -1;
		}
	    }
	  if (loginresp[j+11] != 0x00)
	    printf("parse_login_response: j+11(%x) wrong for this packet type/family\n", loginresp[j+11]); 
	  
	  /* byte j+13 should be the length of the string with nextIP */
	  loginrespstruct->nextIP = (char *) malloc(sizeof(char) * (loginresp[j+12] + 1));
	  memcpy(loginrespstruct->nextIP, &(loginresp[j+13]), (loginresp[j+12] + 1));

	  j += (loginresp[j+12] + 1);
	  
	  if ( (loginresp[j+12] != 0x00) || 
	       (loginresp[j+13] != 0x06) ||
	       (loginresp[j+14] != 0x01) )
	  {
	    /* Check for "signon too soon" error */
	    if( ( loginresp[j+12] == 0x00 ) &&
	        ( loginresp[j+13] == 0x04 ) &&
		( loginresp[j+14] == 0x00 ) )
               return AIM_SIGNON_TOO_SOON;

	    printf("parse_login_response: j+12 (%x) or j+13 (%x) or j+14(%x) wrong for this packet type\n", loginresp[j+12], loginresp[j+13], loginresp[j+14]); 
	  }

	  /* we now know what the gibberish is for: to be sent to conn2.
	     for the scope of this function, load 259b into
	     loginrespstruct->packetData and continue.
	  */
	  /* for now, we'll just assume it's a fixed 259b. */
	  loginrespstruct->packetData = (char *) malloc(sizeof(char) * 259);
	  memcpy(loginrespstruct->packetData, &(loginresp[j+13]), 259);

	  /* byte j+275 should be the length of the string with email address */
	  loginrespstruct->email = (char *) malloc(sizeof(char) * (loginresp[j+275] + 1));
	  memcpy(loginrespstruct->email, &(loginresp[j+276]), (loginresp[j+275] + 1));

	  j += (loginresp[j+275] + 1);

	  /* Verify the command termination bytes (see spec) */
	  if ( (loginresp[j+276] != 0x13) ||
	       (loginresp[j+277] != 0x00) ||
	       (loginresp[j+278] != 0x02) || 
	       (loginresp[j+279] != 0x00) || 
	       (loginresp[j+280] != 0x03))
	    printf("parse_login_response: j+276/277/278/279/280 wrong for this packet type\n"); 

	  i++;
	}
      /* this oddity is temporary -- i have to remember how elseif is in C */
      if ( (commandnum != 1) && (commandnum != 2) )
	i++;
    }

  return 0;
}

/* 
   send_login_phase2(int socket)   

   DO NOT ASK ME WHAT THIS PACKET DOES.  All I know is that it's really
   long and it took 30min to type in...
   
   AHA!  Well, my cute packetmatcher has enlightened me a little.
   It turns out the gibberish we spoke of above in the first packet
   back from Oscar is the exact same gibberish we have here.  Aparently,
   we just send the stuff it sent us back to the new connection
   with the appropriate protocol conformancies.

*/
int aim_send_login_phase2(int s, struct login_phase1_struct *phase1resp)
{

  /*
    The first 11b are generated by us.  The last 259b are
    gotten from the login_response packet above.

    For now, we'll leave the 259b here as reference, and 
    copy the dynamic data over it at run-time.
   */

  int packet_login_phase2_len = 6+5+259;/* 6b FLAP, 5b SNAC, 259 from phase1 */

  struct command_tx_struct newpacket;

  newpacket.lock = 1;
  newpacket.type = 0x01;
  newpacket.commandlen = packet_login_phase2_len - 6;
  newpacket.data = (char *) malloc (packet_login_phase2_len - 6);

  /* SNAC header */
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x00;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x01;
  newpacket.data[4] = 0x00;

#if debug > 1
  printf("Generating packet_login_phase2...");
#endif
  /* copy in authentication data from phase1 */
  memcpy(&(newpacket.data[5]), phase1resp->packetData, 259);
  if( (newpacket.data[4] != 0x00) || 
      (newpacket.data[5] != 0x06) ||
      (newpacket.data[6] != 0x01) )
    printf("send_login_phase2(): ERROR ON PACKETGEN! (%x/%x/%x)\n", newpacket.data[4], newpacket.data[5], newpacket.data[6]);
  else
#if debug > 1
    printf("done.\n");
#endif

  aim_tx_enqueue(&newpacket);
  
  return 0;
}

/* 
   send_login_phase3(int socket)   

   DO NOT ASK ME WHAT THIS PACKET DOES.  I don't know wtf this
   packet does either...someday...

   16b

*/

int aim_send_login_phase3(void)
{
#if 0
  char packet_login_phase3[] = {
    0x2a, 0x02, 0xab, 0xe6, 0x00, 0x0a, 0x00, 0x01,
    0x00, 0x06, 0x00, 0x00, 0x7a, 0x8c, 0x0b, 0x2c
  };
  int packet_login_phase3_len = 16;
#endif
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 0x0a;
  
  newpacket.data = (char *) malloc(newpacket.commandlen);
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x01;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x06;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0b;
  newpacket.data[9] = 0x2c;

  aim_tx_enqueue(&newpacket);

  return 0;
}

/* 
   send_login_phase3b(int socket)   

   Unknown.

   24b

*/
int aim_send_login_phase3b(void)
{
#if 0
  char packet_login_phase3b[] = {
    0x2a, 0x02, 0xab, 0xe7, 0x00, 0x12,
    0x00, 0x01,
    0x00, 0x08, 0x00, 0x00, 0x7a, 0x8c, 0x0d, 0xd7,
    0x00, 0x01, 0x00, 0x02, 0x00, 0x03, 0x00, 0x04
  };
  int packet_login_phase3b_len = 24;
#endif
  struct command_tx_struct newpacket;

  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 18;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x01;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x08;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0d;
  newpacket.data[9] = 0xd7;
  newpacket.data[10] = 0x00;
  newpacket.data[11] = 0x01;
  newpacket.data[12] = 0x00;
  newpacket.data[13] = 0x02;
  newpacket.data[14] = 0x00;
  newpacket.data[15] = 0x03;
  newpacket.data[16] = 0x00;
  newpacket.data[17] = 0x04;

  aim_tx_enqueue(&newpacket);

  return 0;
}

/* 
   send_login_phase3c(int socket)   

   Unknown.

   118b

*/
int aim_send_login_phase3c_1(void)
{
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 14;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x01;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x14;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0d;
  newpacket.data[9] = 0xd9;
  newpacket.data[10] = 0x00;
  newpacket.data[11] = 0x00;
  newpacket.data[12] = 0x00;
  newpacket.data[13] = 0x03;

  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c_2(void)
{
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 12;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x0a;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x01;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x0e;
  newpacket.data[6] = 0x00;
  newpacket.data[7] = 0x00;
  newpacket.data[8] = 0x7a;
  newpacket.data[9] = 0x8c;
  newpacket.data[10] = 0x0d;
  newpacket.data[11] = 0xda;

  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c_3(void)
{
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 12;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x01;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x04;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0d;
  newpacket.data[9] = 0xf5;
  newpacket.data[10] = 0x00;
  newpacket.data[11] = 0x05;
  
  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c_4(void)
{
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 10;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x09;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x02;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0d;
  newpacket.data[9] = 0xf7;

  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c_5(void)
{
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 10;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x03;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x02;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0d;
  newpacket.data[9] = 0xf8;
  
  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c_6(void)
{
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 10;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x02;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x02;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0d;
  newpacket.data[9] = 0xfa;
  
  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c_7(void)
{
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = 10;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  
  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x04;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x04;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x0d;
  newpacket.data[9] = 0xfb;

  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c(void)
{
#if 0
  char packet_login_phase3c[] = {
    0x2a, 0x02, 0xab, 0xe8, 0x00, 0x0e,
    0x00, 0x01,
    0x00, 0x14, 0x00, 0x00, 0x7a, 0x8c, 0x0d, 0xd9,
    0x00, 0x00, 0x00, 0x03,
    0x2a, 0x02, 0xab, 0xe9,
    0x00, 0x0a, 0x00, 0x01, 0x00, 0x0e, 0x00, 0x00,
    0x7a, 0x8c, 0x0d, 0xda,
    0x2a, 0x02, 0xab, 0xea,
    0x00, 0x0c, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00,
    0x7a, 0x8c, 0x0d, 0xf5, 0x00, 0x05,

    0x2a, 0x02, 0xab, 0xeb, 0x00, 0x0a,
    0x00, 0x09, 0x00, 0x02, 0x00, 0x00, 0x7a, 0x8c,
    0x0d, 0xf7,
    
    0x2a, 0x02, 0xab, 0xec, 0x00, 0x0a,
    0x00, 0x03, 0x00, 0x02,
    0x00, 0x00, 0x7a, 0x8c, 0x0d, 0xf8, 
    
    0x2a, 0x02,
    0xab, 0xed, 0x00, 0x0a, 0x00, 0x02, 0x00, 0x02,
    0x00, 0x00, 0x7a, 0x8c, 0x0d, 0xfa,
    
    0x2a, 0x02,
    0xab, 0xee, 0x00, 0x0a, 0x00, 0x04, 0x00, 0x04,
    0x00, 0x00, 0x7a, 0x8c, 0x0d, 0xfb
  };
  int packet_login_phase3c_len = 118;
#endif

  /* send each command seperatly */
  aim_send_login_phase3c_1();
  aim_send_login_phase3c_2();
  aim_send_login_phase3c_3();
  aim_send_login_phase3c_4();
  aim_send_login_phase3c_5();
  aim_send_login_phase3c_6();
  aim_send_login_phase3c_7();

  return 0;
}

/* 
   send_login_phase3c_g(int socket)   

   Unknown.

   20b

*/
int aim_send_login_phase3c_g(void)
{

  char packet_login_phase3c_g[] = {
    0x2a, 0x02, 0xab, 0xef, 0x00, 0x0e, 0x00, 0x09,
    0x00, 0x04, 0x00, 0x00, 0x7a, 0x8c, 0x11, 0x80,
    0x00, 0x00, 0x00, 0x1f
  };
  int packet_login_phase3c_g_len = 20;

  struct command_tx_struct newpacket;

  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = packet_login_phase3c_g_len - 6;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  memcpy(newpacket.data, &(packet_login_phase3c_g[6]),newpacket.commandlen);
  
  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase3c_hi_a(void)
{
  /* AFAIK, this data will never change. */
  char packet_login_phase3c_hi_a[] = {
    /* command one */
    0x2a, 0x02,  /* FLAP: init */
    0xab, 0xf0,  /* FLAP: seqnum */
    
    0x00, 0x0a,  /* FLAP: SNAC len */
    
    0x00, 0x09,  /* SNAC: SNAC Layer 2 len + 1 */
    
    0x00, 0x07,  /* SNAC L2: SNAC L3 len + 1*/
    
    0x00, 0x00,  /* SNAC L3: data */
    0x7a, 0x8c,
    0x11, 0x81,
  };
  int packet_login_phase3c_hi_a_len = 16;
  
  struct command_tx_struct newpacket;

  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = packet_login_phase3c_hi_a_len - 6;

  newpacket.data = (char *) malloc(newpacket.commandlen);
  memcpy(newpacket.data, &(packet_login_phase3c_hi_a[6]),newpacket.commandlen);
  
  aim_tx_enqueue(&newpacket);

  return 0;

}

/*
  aim_send_login_phase3c_hi_b(char **)

  This just builds the "set buddy list" command then queues it.

  The passed (char **) is a an array of strings.  The array must
  be terminated with a NULL.  eg,

    char *buddy_list[] = {
      "buddy1",
      "buddy2",
      NULL
    }

    buddy_list = "AFritzler&TGaArdvark&ME Fruity";

 */
int aim_send_login_phase3c_hi_b(char *buddy_list)
{
  int i, j;

  struct command_tx_struct newpacket;

  int packet_login_phase3c_hi_b_len = 0;

  char *localcpy = NULL;
  char *tmpptr = NULL;

  packet_login_phase3c_hi_b_len = 16; /* 16b for FLAP and SNAC headers */

  /* bail out if we can't make the packet */
  if (buddy_list == NULL)
    {
      printf("\nNO BUDDIES!  ARE YOU THAT LONELY???\n");
      return 0;
    }
#if debug > 0
  printf("****buddy list: %s\n", buddy_list);
  printf("****buddy list len: %d (%x)\n", strlen(buddy_list), strlen(buddy_list));
#endif

  localcpy = (char *) malloc(strlen(buddy_list)+1);
  memcpy(localcpy, buddy_list, strlen(buddy_list)+1);

  i = 0;
  tmpptr = strtok(localcpy, "&");
  while ((tmpptr != NULL) && (i < 100))
    {
#if debug > 0
      printf("---adding %s (%d)\n", tmpptr, strlen(tmpptr));
#endif
      packet_login_phase3c_hi_b_len += strlen(tmpptr)+1;
      i++;
      tmpptr = strtok(NULL, "&");
    }
#if debug > 0
  printf("*** send buddy list len: %d (%x)\n", packet_login_phase3c_hi_b_len, packet_login_phase3c_hi_b_len);
#endif
  free(localcpy);

  newpacket.type = 0x02;
  newpacket.commandlen = packet_login_phase3c_hi_b_len - 6;
  newpacket.lock = 1;
  
  newpacket.data = (char *) malloc(newpacket.commandlen);

  newpacket.data[0] = 0x00;
  newpacket.data[1] = 0x03;
  newpacket.data[2] = 0x00;
  newpacket.data[3] = 0x04;
  newpacket.data[4] = 0x00;
  newpacket.data[5] = 0x00;
  newpacket.data[6] = 0x7a;
  newpacket.data[7] = 0x8c;
  newpacket.data[8] = 0x11;
  newpacket.data[9] = 0x8d;

  j = 10;  /* the next byte */

  i = 0;
  tmpptr = strtok(buddy_list, "&");
  while ((tmpptr != NULL) & (i < 100))
    {
#if debug > 0
      printf("---adding %s (%d)\n", tmpptr, strlen(tmpptr));
#endif
      newpacket.data[j] = strlen(tmpptr);
      memcpy(&(newpacket.data[j+1]), tmpptr, strlen(tmpptr));
      j += strlen(tmpptr)+1;
      i++;
      tmpptr = strtok(NULL, "&");
    }

  newpacket.lock = 0;

  aim_tx_enqueue(&newpacket);

  return 0;
}

/* 
   send_login_phase3c_hi(int socket)   

   Buddy lists, profile, etc.

   variable (mines about 1171b)

*/
int aim_send_login_phase3c_hi(char *newbuddy_list, char *profile)
{
  int packet_profile_len = 0;
  struct command_tx_struct newpacket;
  int i = 0;

  /* len: SNAC */
  packet_profile_len = 10;
  /* len: T+L (where t(0001)) */
  packet_profile_len += 2 + 2;
  /* len: V (where t(0001)) */
  packet_profile_len += strlen("text/x-aolrtf");
  /* len: T+L (where t(0002)) */
  packet_profile_len += 2 + 2;
  /* len: V (where t(0002)) */
  packet_profile_len += strlen(profile);

  newpacket.type = 0x02;
  newpacket.commandlen = packet_profile_len;
  newpacket.data = (char *) malloc(packet_profile_len);

  i = 0;
  /* SNAC: family */
  newpacket.data[i++] = 0x00;
  newpacket.data[i++] = 0x02;
  /* SNAC: subtype */
  newpacket.data[i++] = 0x00;
  newpacket.data[i++] = 0x04;
  /* SNAC: flags */
  newpacket.data[i++] = 0x00;
  newpacket.data[i++] = 0x00;
  /* SNAC: id */
  newpacket.data[i++] = 0x7a;
  newpacket.data[i++] = 0x8c;
  newpacket.data[i++] = 0x11;
  newpacket.data[i++] = 0x95;
  
  /* TLV t(0001) */
  newpacket.data[i++] = 0x00;
  newpacket.data[i++] = 0x01;
  /* TLV l(000d) */
  newpacket.data[i++] = 0x00;
  newpacket.data[i++] = 0x0d;
  /* TLV v(text/x-aolrtf) */
  memcpy(&(newpacket.data[i]), "text/x-aolrtf", 0x000d);
  i += 0x000d;
  
  /* TLV t(0002) */
  newpacket.data[i++] = 0x00;
  newpacket.data[i++] = 0x02;
  /* TLV l() */
  newpacket.data[i++] = (strlen(profile) >> 8) & 0xFF;
  newpacket.data[i++] = (strlen(profile) & 0xFF);
  /* TLV v(profile) */
  memcpy(&(newpacket.data[i]), profile, strlen(profile));
  
  aim_send_login_phase3c_hi_a();
  aim_send_login_phase3c_hi_b(newbuddy_list);

  aim_tx_enqueue(&newpacket);
  
  return 0;
}


/* 
   send_login_phase4_a(int socket)   

   Unknown.

   120b

*/
int aim_send_login_phase4_a_1(void)
{
  char command_1[] = {
    0x00, 0x04, 0x00, 0x02, 0x00, 0x00, 0x7a, 0x8c,
    0x11, 0x9c, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03,
    0x1f, 0x3f, 0x03, 0xe7, 0x03, 0xe7, 0x00, 0x00, 
    0x00, 0x64
  };
  int command_1_len = 26;
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = command_1_len;
  newpacket.data = (char *) malloc (newpacket.commandlen);
  memcpy(newpacket.data, command_1, newpacket.commandlen);
  
  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase4_a_2(void)
{
  char command_2[] = {
     0x00, 0x01, 0x00, 0x02, 0x00, 0x00, 0x7a, 0x8c,
     0x11, 0xab, 0x00, 0x01, 0x00, 0x02, 0x00, 0x01,
     0x00, 0x13, 0x00, 0x09, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01, 0x00, 0x03, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01, 0x00, 0x02, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01, 0x00, 0x08, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01, 0x00, 0x06, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01, 0x00, 0x0a, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01, 0x00, 0x0b, 0x00, 0x01, 0x00, 0x01,
     0x00, 0x01
  };
  int command_2_len = 0x52;
  struct command_tx_struct newpacket;
  
  newpacket.lock = 1;
  newpacket.type = 0x02;
  newpacket.commandlen = command_2_len;
  newpacket.data = (char *) malloc (newpacket.commandlen);
  memcpy(newpacket.data, command_2, newpacket.commandlen);
  
  aim_tx_enqueue(&newpacket);

  return 0;
}

int aim_send_login_phase4_a(void)
{
  /* send each component command */
  aim_send_login_phase4_a_1();
  aim_send_login_phase4_a_2();

  return 0;
}
