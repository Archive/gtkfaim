/*

  SNAC-related dodads...

*/

#include <aim.h>
#include <assert.h>

struct aim_snac_t	*aim_snac_list = NULL;
u_long	aim_snac_nextid = 0x00000001;

u_long	aim_newsnac(struct aim_snac_t *newsnac) {
	struct aim_snac_t	*snac = NULL, *cur = aim_snac_list;
  
	assert(newsnac != NULL);
	snac = calloc(1, sizeof(struct aim_snac_t));
	assert(snac != NULL);
	memcpy(snac, newsnac, sizeof(struct aim_snac_t));
	snac->next = NULL;
	snac->issuetime = time(&snac->issuetime);

	if (cur == NULL) {
		aim_snac_list = snac;
		return(snac->id);
	}
	while (cur->next != NULL)
		cur = cur->next;
	cur->next = snac;
	return(snac->id);
}

struct aim_snac_t	*aim_remsnac(u_long id) {
	struct aim_snac_t	*cur = aim_snac_list;

        g_print("Removing snac with id:  0x%04x\n", id);
	if (cur == NULL)
         {
            g_print("SNAC ERROR: cur == NULL\n");
            return(NULL);
         }
	if (cur->id == id) {
            g_print("SNAC ERROR: returning a valid SNAC\n");
		aim_snac_list = cur->next;
		return(cur);
	}
        if (cur->next)
          {
	    while (cur->next != NULL) {
		if (cur->next->id == id) {
			struct aim_snac_t	*tmp = NULL;

			tmp = cur->next;
			cur->next = cur->next->next;
                   if (tmp == NULL)
                     g_print("SNAC ERROR:  tmp is NULL\n");
                   else
                     g_print("SNAC ERROR:  tmp is a valid snac\n");
			return(tmp);
		}
		cur = cur->next;
                g_print("SNAC ERROR:  Loop iterated id: 0x%04x\n",cur->next->id);
	    }
          }
        else
          {
            aim_snac_list = NULL;
            g_print("SNAC ERROR:  list empty returning the current snac\n");
            return cur;
          }
        g_print("SNAC ERROR:  reached the end, returning NULL\n");
	return(NULL);
}

/*
  This is for cleaning up old SNACs that either don't get replies or
  a reply was never received for.  Garabage collection. Plain and simple.

  maxage is the _minimum_ age in seconds to keep SNACs

 */
int aim_cleansnacs(int maxage)
{
  struct aim_snac_t *cur = aim_snac_list;
  time_t curtime;

  curtime = time(&curtime);

  while (cur)
    {
#if 1
      if ( (cur) && (((cur->issuetime) + maxage) < curtime))
        {
          printf("aimsnac: WARNING purged obsolete snac %ul\n", cur->id);
#if 1
          aim_remsnac(cur->id);
#endif
        }
#endif
      cur = cur->next;
    }

  return 0;
}
