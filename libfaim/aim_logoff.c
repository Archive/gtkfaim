/*
  aim_logoff.c

  All the routines needed to logoff, if there are any.

 */

#include "aim.h"

/* 
   aimlogoff(int socket)
   
   This tells AIM to take use off the usage lists and off the BuddyLists.
   It appears there's no response or acknowledgment of this command from
   the server.

   I think this is pointless.  You get the same result by just closing
   the socket to the server.
   
*/
int aim_logoff(void)
{
  char packet_logoff[] = {
    0x2a, 0x04, 0x6a, 0x52, 0x00, 0x00
  };
  char packet_logoff_len = 6;
  
  if ( write(aim_connection.fd, packet_logoff, packet_logoff_len) != packet_logoff_len)
    {
      close(aim_connection.fd);
      return -1;
    }
  else
    {
      close(aim_connection.fd);
      return 0;
    }
}
