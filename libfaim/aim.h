#ifndef __AIM_H__
#define __AIM_H__

#include <faimconfig.h>

/* some global includes */
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>

#define CONNECT_SIG_LEN 10 /* not used anymore, hopefully */
#define LOGIN_RESP_LEN 512 /* should only be 334b but no segfault for us! */


/*
 * Error codes
 */
#define AIM_CONNECT_ERROR	-0x1
#define AIM_SIGNON_TOO_SOON	-0x4
#define AIM_SERVICE_FULL	-0x6f


struct login_phase1_struct {
  char *screen_name;
  char *nextIP;
  char *packetData;
  char *email;
};

struct connection_info_struct {
  unsigned int local_seq_num_origin; /* our first seq num */
  int local_command_count;

  unsigned int remote_seq_num_origin; /* oscar's first seqnum */
  int remote_command_count; /* command_count + seq_num_origin = cur_seq_num */

  char *sn; /* our screen name */

  int fd;                   /* socket descriptor */
};

/* struct for incoming commands */
struct command_rx_struct {
                            /* byte 1 assumed to always be 0x2a */
  char type;                /* type code (byte 2) */
  unsigned int seqnum;      /* sequence number (bytes 3 and 4) */
  unsigned int commandlen;  /* total packet len - 6 (bytes 5 and 6) */
  char *data;               /* packet data (from 7 byte on) */
  unsigned int lock;        /* 1 = locked, 0 = open */
  unsigned int handled;     /* 1 = been handled, 0 = new */
  struct command_rx_struct *next; /* ptr to next struct in list */
};

/* struct for outgoing commands */
struct command_tx_struct {
                            /* byte 1 assumed to be 0x2a */
  char type;                /* type/family code */
  unsigned int seqnum;      /* seqnum dynamically assigned on tx */
  unsigned int commandlen;  /* SNAC length */
  char *data;               /* packet data */
  unsigned int lock;        /* 1 = locked, 0 = open */
  unsigned int sent;        /* 1 = has been sent, 0 = new */
  struct command_tx_struct *next; /* ptr to next struct in list */
};

/*#define debug 10*/  /* debug level, 0: nodebug, 1: some debug, etc */

/* TLV-related tidbits */
struct aim_tlv_t {
  u_short type;
  u_short length;
  u_char *value;
};

struct aim_tlv_t *aim_grabtlv(u_char *src);
struct aim_tlv_t *aim_grabtlvstr(u_char *src);
int aim_puttlv (u_char *dest, struct aim_tlv_t *newtlv);
struct aim_tlv_t *aim_createtlv(void);
int aim_freetlv(struct aim_tlv_t **oldtlv);


/* some prototypes... */

/* externally called functions */
/*   explictly called */
int aim_login(char *, char *);
/*   implicitly or explicitly called */
int aim_get_command(void);
int aim_rxdispatch(void);
int aim_logoff(void);
typedef int (*rxcallback_t)(struct command_rx_struct *);
int aim_register_callbacks(rxcallback_t *);

/* interally called functions */
int aim_send_login (int, char *, char *);
int aim_encode_password(const char *, char *);
int aim_get_login_response(int s, char *loginresp);
int aim_parse_login_response (struct login_phase1_struct *loginrespstruct, char *loginresp);
int aim_send_login (int, char *, char *);
int aim_send_login_response_ack(int s);
int aim_send_login_phase2(int s, struct login_phase1_struct *phase1resp);
int aim_send_login_phase3(void);
int aim_send_login_phase3b(void);
int aim_send_login_phase3c(void);
int aim_send_login_phase3c_g(void);
int aim_send_login_phase3c_hi(char *, char *);
int aim_send_login_phase4_a(void);
struct command_rx_struct *aim_purge_rxqueue(struct command_rx_struct *queue);
int aim_parse_incoming_im(struct command_rx_struct *command);
int aim_parse_oncoming_buddy(struct command_rx_struct *);
int aim_parse_offgoing_buddy(struct command_rx_struct *command);
int aim_parse_login_phase2(struct command_rx_struct *command);
int aim_parse_login_phase3_b(struct command_rx_struct *command);
int aim_parse_login_phase3d_e(struct command_rx_struct *command);
int aim_parse_login_phase3c_f(struct command_rx_struct *command);
int aim_parse_unknown(struct command_rx_struct *command);
int aim_parse_missed_im(struct command_rx_struct *);
int aim_parse_last_bad(struct command_rx_struct *);
int aim_parse_login_phase3d_a(struct command_rx_struct *);
int aim_parse_login_phase3d_b(struct command_rx_struct *);
int aim_parse_login_phase3d_c(struct command_rx_struct *);
int aim_parse_login_phase3d_d(struct command_rx_struct *);
int aim_parse_login_phase4_c1(struct command_rx_struct *);
int aim_parse_login_phase4_c2(struct command_rx_struct *);
int aim_parse_login_phase2_1(struct command_rx_struct *);
int aim_parse_login_phase2_2(struct command_rx_struct *);
int aim_parse_userinfo(struct command_rx_struct *);
u_long aim_getinfo(const char *);

int aim_send_im(char *, char *);
int aim_send_im_away(char *, char *);
int aim_tx_enqueue(struct command_tx_struct *);
unsigned int aim_get_next_txseqnum(void);
int aim_tx_flushqueue(void);
int aim_tx_printqueue(void);
int aim_tx_purgequeue(void);

/* the dreaded global variable... */

/* connection information (fds, seqnums, etc) */
/*   this is only for the main connection -- auth is local to aim_login() */
extern struct connection_info_struct aim_connection;

/* queue (linked list) pointers */
extern struct command_tx_struct *aim_queue_outgoing; /* incoming commands */
extern struct command_rx_struct *aim_queue_incoming; /* outgoing commands */

/* The default callback handler array */
extern rxcallback_t aim_callbacks[];

extern struct aim_snac_t *aim_outstanding_snacs;
extern u_long aim_snac_nextid;

#define AIM_CB_INCOMING_IM 0
#define AIM_CB_ONCOMING_BUDDY 1
#define AIM_CB_OFFGOING_BUDDY 2
#define AIM_CB_MISSED_IM1 3
#define AIM_CB_MISSED_IM2 4
#define AIM_CB_LOGIN_P4_C1 5
#define AIM_CB_LOGIN_P4_C2 6
#define AIM_CB_LOGIN_P2_1 7
#define AIM_CB_LOGIN_P2_2 8
#define AIM_CB_LOGIN_P3_B 9
#define AIM_CB_LOGIN_P3D_A 10
#define AIM_CB_LOGIN_P3D_B 11
#define AIM_CB_LOGIN_P3D_C 12
#define AIM_CB_LOGIN_P3D_D 13
#define AIM_CB_LOGIN_P3D_E 14
#define AIM_CB_LOGIN_P3D_F 15
#define AIM_CB_LAST_BAD 16
#define AIM_CB_MISSED_IM3 17
#define AIM_CB_UNKNOWN 18
#define AIM_CB_USERINFO 19
#define AIM_CB_SEARCH_ADDRESS 20
#define AIM_CB_SEARCH_NAME 21
#define AIM_CB_SEARCH_FAIL 22

int Read(int, u_char *, int);


struct aim_snac_t {
  u_long id;
  u_short family;
  u_short type;
  u_short flags;
  void *data;
  time_t issuetime;
  struct aim_snac_t *next;
};
u_long aim_newsnac(struct aim_snac_t *newsnac);
struct aim_snac_t *aim_remsnac(u_long id);
int aim_cleansnacs(int maxage);

u_long aim_add_buddy(char *);
u_long aim_remove_buddy(char *);
u_long aim_usersearch_address(char *);
u_long aim_usersearch_name(char *);

#endif /* __AIM_H__ */
