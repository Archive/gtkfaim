
/*
  aim_rxhandlers.c

  This file contains most all of the incoming packet handlers, along
  with aim_rxdispatch(), the Rx dispatcher.  Queue/list management is
  actually done in aim_rxqueue.c.
  
 */


#include "aim.h" /* for most everything */

/* these are the default handlers only */
rxcallback_t aim_callbacks[] = {
  aim_parse_incoming_im, /* incoming IM */
  aim_parse_oncoming_buddy, /* oncoming buddy */
  aim_parse_offgoing_buddy, /* offgoing buddy */
  aim_parse_missed_im, /* last IM was missed */
  aim_parse_missed_im, /* last IM was missed 2 */
  aim_parse_login_phase4_c1, /* login phase 4 packet C command 1*/
  aim_parse_login_phase4_c2, /* login phase 4 packet C command 2 */
  aim_parse_login_phase2_1, /* login phase 2, first resp */
  aim_parse_login_phase2_2, /* login phase 2, second resp */
  aim_parse_login_phase3_b, /* login phase 3 packet B */
  aim_parse_login_phase3d_a, /* login phase 3D packet A */
  aim_parse_login_phase3d_b, /* login phase 3D packet B */
  aim_parse_login_phase3d_c, /* login phase 3D packet C */
  aim_parse_login_phase3d_d, /* login phase 3D packet D */
  aim_parse_login_phase3d_e, /* login phase 3D packet E */
  aim_parse_login_phase3c_f, /* login phase 3C packet F */
  aim_parse_last_bad, /* last command bad */
  aim_parse_missed_im, /* missed some messages */
  aim_parse_unknown, /* completely unknown command */
  aim_parse_userinfo, /* User Info Response */
  aim_parse_unknown, /* User Search by Address response */
  aim_parse_unknown, /* User Search by Name response */
  aim_parse_unknown, /* user search fail */
  0x00
};


int aim_register_callbacks(rxcallback_t *newcallbacks)
{
  int i = 0;
  
  for (i = 0; aim_callbacks[i] != 0x00; i++)
    {
      if ( (newcallbacks[i] != NULL) &&
	   (newcallbacks[i] != 0x00) )
	{
#if debug > 3
	  printf("aim_register_callbacks: changed handler %d\n", i);
#endif
	  aim_callbacks[i] = newcallbacks[i];
	}
    }
  
  return 0;
}

/*
  aim_rxdispatch()

  Basically, heres what this should do:
    1) Determine correct packet handler for this packet
    2) Mark the packet handled (so it can be dequeued in purge_queue())
    3) Send the packet to the packet handler
    4) Go to next packet in the queue and start over
    5) When done, run purge_queue() to purge handled commands

  Note that any unhandlable packets should probably be left in the
  queue.  This is the best way to prevent data loss.  This means
  that a single packet may get looked at by this function multiple
  times.  This is more good than bad!  This behavior may change.

  Aren't queue's fun? 
  
 */
int aim_rxdispatch(void)
{
  int i = 0;
  struct command_rx_struct *workingPtr = NULL;
  
  if (aim_queue_incoming == NULL)
    /* this shouldn't really happen, unless the main loop's select is broke  */
    printf("parse_generic: incoming packet queue empty.\n");
  else
    {
      workingPtr = aim_queue_incoming;
      for (i = 0; workingPtr != NULL; i++)
	{
	  /* Identify and handle packet */

	  /* standard incoming IM */
	  if ( (workingPtr->data[0] == 0x00) && 
	       (workingPtr->data[1] == 0x04) &&
	       (workingPtr->data[2] == 0x00) &&
	       (workingPtr->data[3] == 0x07) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_INCOMING_IM])(workingPtr);
	    }
	  /* oncoming buddy */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x03) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x0b) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_ONCOMING_BUDDY])(workingPtr);
	    }
	  /* offgoing buddy */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x03) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x0c) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_OFFGOING_BUDDY])(workingPtr);
	    }
	  /* unknown command -- something to do with bad IMs */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x01) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x0a) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_MISSED_IM1])(workingPtr);
	    }
	  /* unknown command -- something to do with bad IMs */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x04) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x01) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_MISSED_IM2])(workingPtr);
	    }
	  /* command one, phase 4, packet C, login residuals */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x0b) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x02) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P4_C1])(workingPtr);
	    }
	  /* command two, phase 4, packet C, login residuals */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x01) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x13) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P4_C2])(workingPtr);
	    }
	  /* Phase 2 */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x00) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x01) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P2_1])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x01) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x03) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P2_2])(workingPtr);
	    }
	  /* Phase 3, packet B (662b) */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x01) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x07) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P3_B])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x01) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x0f) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P3D_A])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x09) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x03) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P3D_B])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x03) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x03) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P3D_C])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x02) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x03) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P3D_D])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x04) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x05) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P3D_E])(workingPtr);
	    }
	  /* phase3c_f */
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x01) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x05) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LOGIN_P3D_F])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x02) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x01) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_LAST_BAD])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x04) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x0a) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_MISSED_IM3])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x02) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x06) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_USERINFO])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x0a) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x03) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_SEARCH_ADDRESS])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x0a) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x00) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_SEARCH_NAME])(workingPtr);
	    }
	  else if ( (workingPtr->data[0] == 0x00) &&
		    (workingPtr->data[1] == 0x0a) &&
		    (workingPtr->data[2] == 0x00) &&
		    (workingPtr->data[3] == 0x01) )
	    {
	      workingPtr->handled = 1;
	      (aim_callbacks[AIM_CB_SEARCH_FAIL])(workingPtr);
	    }	  
	  else
	    {
	      /* Completely unknown packet */
	      workingPtr->handled = 1;  /* go ahead and purge it anyway */
	      (aim_callbacks[AIM_CB_UNKNOWN])(workingPtr);
	    }
	  /* move to next command */
	  workingPtr = workingPtr->next;
	}
#if 0
      printf("parse_generic: incoming packet queue had %d entries.\n", i);
#endif
    }

  aim_queue_incoming = aim_purge_rxqueue(aim_queue_incoming);
  
  return 0;
}

int aim_parse_oncoming_buddy(struct command_rx_struct *command)
{

  printf("\n%s is now online\n", &(command->data[11]));

  return 0;
}

int aim_parse_offgoing_buddy(struct command_rx_struct *command)
{

  printf("\n%s has left\n", &(command->data[11]));

  return 0;
}

int aim_parse_login_phase2_1(struct command_rx_struct *command)
{
#if debug > 1
  printf("got phase 2, first response\n");
#endif
  return 0;
}

int aim_parse_login_phase2_2(struct command_rx_struct *command)
{
#if debug > 1
  printf("\nGot login Phase 2, second response...\n");
#endif
  aim_send_login_phase3(); /* 16b ok A */

  return 0;
}

int aim_parse_login_phase3_b(struct command_rx_struct *command)
{
#if debug > 1
  printf("\nGot login Phase 3, Packet B...\n");
#endif

  /* Phase 3B */
  aim_send_login_phase3b(); /* 24b ok A e7*/
  /* Phase 3C */
  aim_send_login_phase3c(); /* 118b ok A-G */

  return 0;
}

int aim_parse_login_phase3d_e(struct command_rx_struct *command)
{
#if debug > 1
  printf("\nGot login Phase 3D, Packet E...\n");
#endif
  aim_send_login_phase3c_g(); /* conn2: send 20b G */
  return 0;
}

int aim_parse_login_phase3c_f(struct command_rx_struct *command)
{
#if debug > 1
  printf("\nGot login Phase 3C, Packet F...\n");
#endif
  aim_send_login_phase3c_hi(NULL, NULL); /* conn2: send ~1181b H-I*/
  aim_send_login_phase4_a(); /* conn2: send 120b A*/  
  printf("\nYou are now officially online.\n");
  return 0;
}

int aim_parse_unknown(struct command_rx_struct *command)
{
  int i = 0;

  printf("\nRecieved unknown packet:");

  for (i = 0; i < command->commandlen; i++)
    {
      if ((i % 8) == 0)
	printf("\n\t");

      printf("0x%2x ", command->data[i]);
    }
  
  printf("\n\n");

  return 0;
}

int aim_parse_missed_im(struct command_rx_struct *command)
{
  if ( (command->data[0] == 0x00) &&
       (command->data[1] == 0x01) &&
       (command->data[2] == 0x00) &&
       (command->data[3] == 0x0a) )
    {
      printf("\n****STOP SENDING/RECIEVING MESSAGES SO FAST!****\n\n");
    }
  else if ( (command->data[0] == 0x00) &&
	    (command->data[1] == 0x04) &&
	    (command->data[2] == 0x00) &&
	    (command->data[3] == 0x01) )
    {
      printf("\n***LAST IM DIDN\'T MAKE IT BECAUSE THE BUDDY IS NOT ONLINE***\n\n");
    }
  else if ( (command->data[0] == 0x00) &&
	    (command->data[1] == 0x04) &&
	    (command->data[2] == 0x00) &&
	    (command->data[3] == 0x0a) )
    {
      printf("You missed some messages from %s because they were sent too fast\n", &(command->data[13]));
    }

  return 0;
}

int aim_parse_last_bad(struct command_rx_struct *command)
{
  u_long snacid = 0x00000000;

  snacid = ((command->data[6] << 24) & 0xFF000000);
  snacid += ((command->data[7] << 16)& 0x00FF0000);
  snacid += ((command->data[8] <<  8)& 0x0000FF00);
  snacid += ((command->data[9] )     & 0x000000FF);
  
#if debug > 1
  printf("Received unknown error in SNAC family 0x0002 (snacid = %08lx)\n", snacid);
#endif

  return 0; 
}

int aim_parse_login_phase3d_a(struct command_rx_struct *command)
{
#if debug > 1
  printf("got login phase 3d, packet a\n");
#endif
  return 0;
}

int aim_parse_login_phase3d_b(struct command_rx_struct *command)
{
#if debug > 1
  printf("got login phase 3d, packet b\n");
#endif
  return 0;
}

int aim_parse_login_phase3d_c(struct command_rx_struct *command)
{
#if debug > 1
  printf("got login phase 3d, packet c\n");
#endif
  return 0;
}

int aim_parse_login_phase3d_d(struct command_rx_struct *command)
{
#if debug > 1
  printf("got login phase 3d, packet d\n");
#endif
 return 0;
}

int aim_parse_login_phase4_c1(struct command_rx_struct *command)
{
#if debug > 1
  printf("got first command of phase4_c login packet\n");
#endif
  return 0;
}

int aim_parse_login_phase4_c2(struct command_rx_struct *command)
{
#if debug > 1
  printf("got second command of phase4_c login packet\n");
#endif
  return 0;
}
