AC_DEFUN([GTK_CHECK],
[
	AC_SUBST(GTK_LIBS)
	AC_SUBST(GTK_INCLUDEDIR)
	AC_PATH_PROG(GTK_CONFIG,gtk-config,no)
	
	if test "$GTK_CONFIG" = "no"; then
	  no_gtk_config="yes"
	  AC_MSG_ERROR([*** GTK not found on this system, unable to build ***])
	else
	  AC_MSG_CHECKING(if $GTK_CONFIG works)
	  if $GTK_CONFIG --libs >/dev/null 2>&1; then
	    AC_MSG_RESULT(yes)
	    GTK_LIBS="`$GTK_CONFIG --libs`"
	    GTK_INCLUDEDIR="`$GTK_CONFIG --cflags`"
            $1
	  else
	    AC_MSG_RESULT(no)
	    no_gtk_config="yes"
	    AC_MSG_ERROR([*** GTK not found on this system, unable to build ***])
          fi
        fi
])

AC_DEFUN([GNOME_CHECK],
[
	AC_SUBST(GNOME_LIBS)
	AC_SUBST(GNOMEUI_LIBS)
	AC_SUBST(GNOME_LIBDIR)
	AC_SUBST(GNOME_INCLUDEDIR)
	AC_PATH_PROG(GNOME_CONFIG,gnome-config,no)

	if test "$GNOME_CONFIG" = "no"; then
	  no_gnome_config="yes"
	  AC_MSG_WARN([*** GNOME not found on this system \
those features will not be used. ***])
	else
	  AC_MSG_CHECKING(if $GNOME_CONFIG works)
	  if $GNOME_CONFIG --libs-only-l gnome >/dev/null 2>&1; then
	    AC_MSG_RESULT(yes)
            AC_MSG_CHECKING([if gnome is from cvs])
            AC_EGREP_HEADER(popt, `$GNOME_CONFIG --includedir`/libgnomeui/gnome-init.h,gnome_cvs=yes, gnome_cvs=no)
            if test $gnome_cvs = yes; then
              AC_MSG_RESULT(yes)
              GNOME_LIBS="-DGNOME `$GNOME_CONFIG --libs-only-l gnome`"
	      GNOMEUI_LIBS="`$GNOME_CONFIG --libs-only-l gnomeui`"
	      GNOME_LIBDIR="`$GNOME_CONFIG --libs-only-L gnomeui`"
	      GNOME_INCLUDEDIR="-DGNOME `$GNOME_CONFIG --cflags gnomeui`"
              $1
            else
              AC_MSG_RESULT(no)
              AC_MSG_WARN([*** GNOME must be from CVS for gnome support ***])
            fi
	  else
	    AC_MSG_RESULT(no)
	    no_gnome_config="yes"
	    AC_MSG_WARN([*** GNOME not found on this system \
those features will not be used. ***])
          fi
        fi
	if test x$exec_prefix = xNONE; then
	    if test x$prefix = xNONE; then
		gnome_prefix=$ac_default_prefix/lib
	    else
 		gnome_prefix=$prefix/lib
	    fi
	else
	    gnome_prefix=`eval echo \`echo $libdir\``
	fi
	
	AC_ARG_WITH(gnome-includes,
	[  --with-gnome-includes   Specify location of GNOME headers],[
	CFLAGS="$CFLAGS -I$withval"
	])
	
	AC_ARG_WITH(gnome-libs,
	[  --with-gnome-libs       Specify location of GNOME libs],[
	LDFLAGS="$LDFLAGS -L$withval"
	gnome_prefix=$withval
	])

	AC_ARG_WITH(gnome,
	[  --with-gnome            Specify prefix for GNOME files],[
	if test x$withval = xyes; then
	    dnl Note that an empty true branch is not valid sh syntax.
	    ifelse([$1], [], :, [$1])
        else
	    LDFLAGS="$LDFLAGS -L$withval/lib"
	    CFLAGS="$CFLAGS -I$withval/include"
	    gnome_prefix=$withval/lib
  	fi
	])

])

