/************************************************************************\
 * gtkFAIM
 * -------
 *
 * Description: gtkFAIM main header file
 *
\************************************************************************/

#ifndef __GTKFAIM_H__
#define __GTKFAIM_H__ 1

/*
 * Include headers
 */
#include <config.h>
#include <gtk/gtk.h>
#if GTK_MAJOR_VERSION > 1 || GTK_MINOR_VERSION > 0
# define GTK_ACCELGROUP
#endif
#ifdef GNOME
  #include <gnome.h>
#endif

#include <aim.h>



/*
 * Public macros
 */
#define GTKFAIM_FE "gtk"

/* it turns out that using my real numbering system, C wants to
   use octal (for leading zero).  So, just mentally prepend a zero
   to these */
#define GTKFAIM_MAJOR 0x00
#define GTKFAIM_MINOR 0x0c
#define GTKFAIM_PATCHLEVEL ""

/* Configuration macros */
#define NO_DEFAULT NULL

/* Configuration entry flags */
#define GTKFAIM_GLOBAL    0x00
#define GTKFAIM_LOCAL     0x01
#define GTKFAIM_READ_ONLY 0x02

/* macros for printing errors/warnings to tty */
#define GTKFAIM_DEBUG(module, error, cr) \
 fprintf(stderr, "gtkfaim: %s(%s:%d): %s%c", module, __FILE__, __LINE__, error, cr ? '\n' : ' ')

#define GTKFAIM_DEBUG2(module, error, error2, cr) \
 fprintf(stderr, "gtkfaim: %s(%s:%d): %s%s%c", module, __FILE__, __LINE__, error, error2, cr ? '\n' : ' ')



/*
 * Public types
 */
typedef char*                                          comment_t;
typedef struct tuple_struct                            tuple_t;
typedef enum   faim_conf_type_enum                     faim_conf_type_t;
typedef struct faim_conf_struct                        faim_conf_t;
typedef struct gtkfaim_config_struct                   gtkfaim_config_t;
typedef enum   gtkfaim_manage_node_type_enum           gtkfaim_manage_node_type_t;
typedef struct gtkfaim_manage_node_struct              gtkfaim_manage_node_t;
typedef struct gtkfaim_imwindow_struct                 gtkfaim_imwindow_t;
typedef struct gtkfaim_buddygroup_struct               gtkfaim_group_t;
typedef struct gtkfaim_buddy_struct                    gtkfaim_buddy_t;
typedef struct gtkfaim_buddylist_node_struct           gtkfaim_buddylist_node_t;
typedef struct gtkfaim_window_nonbuddy_getsn_struct    gtkfaim_window_nonbuddy_getsn_t;
typedef struct gtkfaim_window_nonbuddy_getinfo_struct  gtkfaim_window_nonbuddy_getinfo_t;
typedef struct gtkfaim_window_nonbuddy_addbuddy_struct gtkfaim_window_nonbuddy_addbuddy_t;
typedef struct gtkfaim_window_nonbuddy_addgroup_struct gtkfaim_window_nonbuddy_addgroup_t;
typedef struct gtkfaim_window_buddies_struct           gtkfaim_window_buddies_t;
typedef struct gtkfaim_window_prefs_struct             gtkfaim_window_prefs_t;
typedef struct gtkfaim_infowindow_struct               gtkfaim_window_info_t;



/*
 * Public structures
 */
enum gtkfaim_manage_node_type_enum
{
   BUDDYLIST_TYPE_GROUP,
   BUDDYLIST_TYPE_BUDDY
};

enum faim_conf_type_enum
{
   FAIM_CONF_TYPE_STRING,
   FAIM_CONF_TYPE_BOOLEAN,
   FAIM_CONF_TYPE_TUPLE
};


struct tuple_struct
{
   gint                        red;
   gint                        green;
   gint                        blue;
};

struct faim_conf_struct
{
   const char                 *name;
   int                         offset;
   faim_conf_type_t            type;
   int                         len;
   int                         flags;
   void                       *deflt;
   comment_t                  *comments;
};

struct gtkfaim_config_struct
{
   char                       *screenname;
   char                       *password;
   u_int                       autologin;
   tuple_t                     imsg_bgcolor;
   tuple_t                     imsg_fgcolor;
   tuple_t                     imsg_src_bgcolor;
   tuple_t                     imsg_src_fgcolor;
   tuple_t                     imsg_dest_bgcolor;
   tuple_t                     imsg_dest_fgcolor;
   char                       *onlogin;
   char                       *onlogoff;
   char                       *oninmessage;
   char                       *onoutmessage;
   char                       *onjoin;
   char                       *onleave;
   char                       *profile;
   char                       *profile_string;
   char                       *awaymsg;
   u_int                       sendonenter;
   char                       *gtkrc;
   u_int                       timestamps;
   gboolean                    readonly;
};

struct gtkfaim_manage_node_struct
{
   gtkfaim_manage_node_type_t  type;
   gpointer                    data;
};

struct gtkfaim_imwindow_struct
{
   gtkfaim_buddy_t            *buddy;
   GtkWidget                  *widgetptr;  /* GtkWidget ptr to window */
   GtkWidget                  *vbox;
   GtkWidget                  *paned;
   GtkWidget                  *hbox_text1;
   GtkWidget                  *hbox_text2;
   GtkWidget                  *hbox_buttons;
   GtkWidget                  *textbox_conversation;
   GtkWidget                  *textbox_msg;
   GtkWidget                  *textbox_conversation_scroll;
   GtkWidget                  *textbox_msg_scroll;
   GtkWidget                  *button_send;
   GtkWidget                  *button_close;
   GtkWidget                  *button_info;
#ifndef NOZAP
   GtkWidget                  *button_zap;
#endif
#ifdef GTK_ACCELGROUP
   GtkAccelGroup              *accelgroup;
#else
   GtkAcceleratorTable        *acceltable;
#endif
};

struct gtkfaim_buddygroup_struct
{
   char                       *name;
   GList                      *buddies;
   int                         numonline;
   GtkWidget                  *tree;
   GtkWidget                  *treenode;
   GtkCTreeNode               *ctreenode;
};

struct gtkfaim_buddy_struct
{
   char                       *sn;
   char                       *alias;
   gtkfaim_imwindow_t         *imwindow;
   unsigned int                references;
   gboolean                    online;
   unsigned int                warninglevel;
   unsigned int                class;
   unsigned int                idletime;
   time_t                      membersince;
   time_t                      onsince;
   unsigned int                flags;
};

struct gtkfaim_buddylist_node_struct
{
   gtkfaim_buddy_t            *buddy;
   gtkfaim_group_t            *group;
   GtkWidget                  *treenode;
   GtkWidget                  *signon_pixmap;
   GtkWidget                  *label;
   guint                       signon_timer;
};

struct gtkfaim_window_nonbuddy_getsn_struct
{
   GtkWidget                  *window;
#ifndef GNOME
   GtkWidget                  *vbox;
   GtkWidget                  *textbox;
   GtkWidget                  *imopen;
   GtkWidget                  *cancel;
#endif
};

struct gtkfaim_window_nonbuddy_getinfo_struct
{
   GtkWidget                  *window;
#ifndef GNOME
   GtkWidget                  *vbox;
   GtkWidget                  *snlabel;
   GtkWidget                  *textbox;
   GtkWidget                  *inopen;
   GtkWidget                  *cancel;
#endif
};

struct gtkfaim_window_nonbuddy_addbuddy_struct
{
   GtkWidget                  *window;
   GtkWidget                  *vbox;
   GtkWidget                  *snlabel;
   GtkWidget                  *aliaslabel;
   GtkWidget                  *grouplabel;
   GtkWidget                  *combo;
   GtkWidget                  *textbox;
   GtkWidget                  *alias_textbox;
   GtkWidget                  *accept;
   GtkWidget                  *cancel;
};

struct gtkfaim_window_nonbuddy_addgroup_struct
{
   GtkWidget                  *window;
   GtkWidget                  *vbox;
   GtkWidget                  *grouplabel;
   GtkWidget                  *textbox;
   GtkWidget                  *accept;
   GtkWidget                  *cancel;
};

struct gtkfaim_window_buddies_struct
{
   GtkWidget                  *window;
#ifndef GNOME
   GtkWidget                  *menu_bar;
   GtkWidget                  *file_menu;
   GtkWidget                  *file_item;
   GtkWidget                  *file_item_options;
   GtkWidget                  *file_options_menu;
   GtkWidget                  *file_options_item_preferences;
   GtkWidget                  *file_options_item_chngpwd;
   GtkWidget                  *file_item_signoff;
   GtkWidget                  *file_item_exit;
   GtkWidget                  *people_menu;
   GtkWidget                  *people_item;
   GtkWidget                  *people_item_find;
   GtkWidget                  *people_find_menu;
   GtkWidget                  *people_find_item_byname;
   GtkWidget                  *people_find_item_byemail;
   GtkWidget                  *people_item_away;
#endif
   GtkWidget                  *manage_hbox;
   GtkWidget                  *manage_label;
   GtkWidget                  *manage_tree;
#if (GTK_MAJOR_VERSION == 1) && (GTK_MINOR_VERSION == 1) && (GTK_MICRO_VERSION >= 5)
   GtkWidget                  *manage_scrolled_window;
#endif /* GTK Version >= 1.1.5 */
   GtkWidget                  *manage_vbox;
   GtkWidget                  *manage_add_buddy_button;
   GtkWidget                  *manage_add_group_button;
   GtkWidget                  *manage_remove_button;
   GtkWidget                  *monitor_hbox;
   GtkWidget                  *monitor_label;
   GtkWidget                  *monitor_scrolled_window;
   GtkWidget                  *monitor_tree;
   GtkWidget                  *monitor_vbox;
   GtkWidget                  *monitor_sendim_button;
   GtkWidget                  *monitor_info_button;
   GtkWidget                  *notebook;
   GtkWidget                  *vbox;
};

struct gtkfaim_prefs_window
{
   GtkWidget                  *window;
   GtkWidget                  *txt_preview;
   GtkWidget                  *e_onlogin;
   GtkWidget                  *e_onleave;
   GtkWidget                  *e_onjoin;
   GtkWidget                  *e_onlogoff;
   GtkWidget                  *e_onoutmsg;
   GtkWidget                  *e_oninmsg;
   GtkWidget                  *e_profile;
   GtkWidget                  *e_gtkrc;
   GtkWidget                  *chk_autologin;
   GtkWidget                  *chk_sendonenter;
   GtkWidget                  *chk_timestamps;
};

struct gtkfaim_infowindow_struct
{
   GtkWidget                  *window;
   GtkWidget                  *vbox1; /* outer */
   GtkWidget                  *vbox2; /* top half */
   GtkWidget                  *vbox3; /* bottom half */
   GtkWidget                  *hbox1;
   GtkWidget                  *hbox2; /* top half */
   GtkWidget                  *vbox4; /* top half left */
   GtkWidget                  *vbox5; /* top half right */
   GtkWidget                  *profile; /* profile box (bottom half) */
   GtkWidget                  *profile_vscroll;
   GtkWidget                  *sn_caption;
   GtkWidget                  *sn;
   GtkWidget                  *sn_hbox;
   GtkWidget                  *warnlevel_caption;
   GtkWidget                  *warnlevel;
   GtkWidget                  *warnlevel_hbox;
   GtkWidget                  *idletime_caption;
   GtkWidget                  *idletime;
   GtkWidget                  *idletime_hbox;
   GtkWidget                  *class_caption;
   GtkWidget                  *class;
   GtkWidget                  *class_hbox;
   GtkWidget                  *membersince_caption;
   GtkWidget                  *membersince;
   GtkWidget                  *membersince_hbox;
   GtkWidget                  *onsince_caption;
   GtkWidget                  *onsince;
   GtkWidget                  *onsince_hbox;
};



/*
 * Publicly Exported globals
 */
extern gtkfaim_config_t                    gtkfaim_config;
extern int                                 gtkfaim_away;
extern char                               *loaded_conffile;
extern u_long                              snac_outstanding;
extern u_long                              snac_last;
extern GList                              *gtkfaim_imwindowlist;
extern gtkfaim_window_buddies_t            gtkfaim_window_buddies;
extern GList                              *gtkfaim_groups;
extern GList                              *gtkfaim_buddies;
extern gtkfaim_group_t                    *gtkfaim_temporary_group;
extern gtkfaim_window_nonbuddy_getsn_t    *gtkfaim_nonbuddy_getsn;
extern gtkfaim_window_nonbuddy_getinfo_t  *gtkfaim_nonbuddy_getinfo;
extern gtkfaim_window_nonbuddy_addbuddy_t *gtkfaim_nonbuddy_addbuddy;
extern gtkfaim_window_nonbuddy_addgroup_t *gtkfaim_nonbuddy_addgroup;
extern gtkfaim_window_info_t              *gtkfaim_infowindow;



/*
 * Public functions
 */

/* away.c */
void             gtkfaim_away_cb                ( GtkWidget *widget,
                                                  gpointer data );


/* config.c */
void             gtkfaim_loadgtkrc              ( void );
int              gtkfaim_read_config            ( void );
int              gtkfaim_write_config           ( void );
int              gtkfaim_parse_config           ( char *, char ** );


/* buddylist.c */
gtkfaim_buddy_t *gtkfaim_buddy_find             ( char *sn );
gtkfaim_buddy_t *gtkfaim_buddy_new              ( char *sn, char *alias );
char            *gtkfaim_buddylist_create_array ( void );
GtkWidget       *gtkfaim_buddylist_manage_create( void );
void             gtkfaim_buddylist_refreshonline( void );
gboolean         gtkfaim_group_add_buddy        ( gtkfaim_group_t *group,
                                                  gtkfaim_buddy_t *buddy );
gboolean         gtkfaim_group_add_buddy_by_name( gtkfaim_group_t *group,
                                                  char *sn );
gboolean         gtkfaim_group_delete_buddy     ( gtkfaim_group_t *group,
                                                  gtkfaim_buddy_t *buddy );
gboolean         gtkfaim_group_delete_buddy_by_name( gtkfaim_group_t *group,
                                                     char *sn  );
gtkfaim_group_t *gtkfaim_group_find             ( char *name );
gtkfaim_group_t *gtkfaim_group_new              ( char *sn );
int              gtkfaim_parse_oncoming_buddy   ( struct command_rx_struct *
                                                  command );
int              gtkfaim_parse_offgoing_buddy   ( struct command_rx_struct *
                                                  command );
int              gtkfaim_window_buddies_create  ( void );


/* gtkfaim.c */
void             datawaiting_callback           ( gpointer data, gint source,
                                                  GdkInputCondition condition );
int              gtkfaim_clear_textbox          ( GtkWidget *widget );
gint             gtkfaim_delete_event           ( GtkWidget *widget,
                                                  GdkEvent *event,
                                                  gpointer data );
void             gtkfaim_destroy_event          ( GtkWidget *widget,
                                                  gpointer data );
void             gtkfaim_genericok              ( char *msg );
void             gtkfaim_genericok_cb           ( GtkWidget *widget,
                                                  gpointer data );
void             gtkfaim_genericok_signal       ( char *msg,
                                                  GtkSignalFunc func );
void             gtkfaim_logoff                 ( void );
int              gtkfaim_parse_missed_im        ( struct command_rx_struct *
                                                  command );
int              gtkfaim_html_insert            ( GtkWidget *destwidget,
                                                  char *text );


/* gtklogin.c */
int              gtkfaim_autologin              ( void );
void             gtkfaim_create_loginwindow     ( void );
int              gtkfaim_parse_login_phase3d_f  ( struct command_rx_struct *
                                                  command );


/* imwindow.c */
void             gtkfaim_add_imwindow           ( char *destsn );
void             gtkfaim_buddylist_sendim_click ( GtkWidget *widget,
                                                  gpointer data );
void             gtkfaim_imwindow_close         ( GtkWidget *widget,
                                                  gpointer data );
int              gtkfaim_parse_incoming_im      ( struct command_rx_struct *
                                                  command );
gtkfaim_imwindow_t *gtkfaim_search_imwindow(char *);


/* password.c */
void             gtkfaim_password_change_cb     ( GtkWidget *widget,
                                                  gpointer data );


/* prefs.c */
void             gtkfaim_create_prefs_window    ( void );


/* snutil.c -- utility functions for dealing with screennames */
int              sncmp                          ( const char *sn1,
                                                  const char *sn2 );
int              snlen                          ( const char *sn );


/* search_address.c */
int              gtkfaim_parse_search_address   ( struct command_rx_struct *
                                                  command );
int              gtkfaim_parse_search_fail      ( struct command_rx_struct *
                                                  command );
void             gtkfaim_usersearch_address_cb  ( GtkWidget *widget,
                                                  gpointer data );


/* search_name.c */
int              gtkfaim_parse_search_name      ( struct command_rx_struct *
                                                  command );


/* userinfo.c */
int              gtkfaim_parse_snacerror        ( struct command_rx_struct *
                                                  command );
int              gtkfaim_parse_userinfo         ( struct command_rx_struct *
                                                  command );



/*
 * Functions implemented as macros
 */
#define gtkfaim_read_config() \
   gtkfaim_parse_config( GTKFAIM_FE, &loaded_conffile )

#define gtkfaim_write_config() \
   if( loaded_conffile ) gtkfaim_parse_config( GTKFAIM_FE, &loaded_conffile )


#endif /* __GTKFAIM_H__ */ 

