#ifndef __CONFIG_H__
#define __CONFIG_H__

/* ---------------------------------- */
/* compile-time configuration options */
/* ---------------------------------- */

/* *** Be sure to check faim/faimconfig.h for other options before
   *** compiling for the first time! */


#ifdef debug 
#undef debug
/* 
   set debug to be > 0 if you want debugging information spewing
   on the attached tty.  set to 0 for daily use.  this value
   is _not_ inherited by the backend (set in backend faim/faimconfig.h
   explicitly). 

   Default: 0  
*/
#define debug 0

#endif

/*
  define NOZAP if you want to shutoff and remove the client-side 
  "neighborhood control" features.  note that people will still 
  be able to warn and block you (you just can't do it back with
  this defined)
  
  Default: defined
 */
#define NOZAP

/*
  define CLOSEBROKE if you get a crash shortly after logging in (ie, when
  the login window would normally disappear).  this is a possible bug
  in older gtk libraries (pre 1.0.4).  Default: undefined.
 */
/* #define CLOSEBROKE */

/*
  define ALLOWBLANKS to disable the check for blank messages before sending
  them.  really, really trivial.  Default: undefined.
 */
/* #define ALLOWBLANKS */

/*
  define SIGNON_ICON_DELAY to the number of milliseconds to display the
  open door icon next to buddies who have just signed on.
  Default: undefined (2000 ms)
 */
/* #define SIGNON_ICON_DELAY */

/* ----------------------------------- */
/* nothing user-configurable past here */
/* ----------------------------------- */


#endif /* __CONFIG_H__ */
