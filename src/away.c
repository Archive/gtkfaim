#include <gtkfaim.h>

#ifdef GNOME
void gtkfaim_awaybox_cb(GnomeDialog *data, gint button_number, gpointer xtradata)
#else
void gtkfaim_awaybox_cb(GtkWidget *widget, gpointer data)
#endif
{
  if (data)
    {
      gtkfaim_away = !gtkfaim_away;
#ifndef GNOME
      gtk_widget_hide(GTK_WIDGET(data));
      gtk_widget_destroy(GTK_WIDGET(data));
#endif
    }
  return;
}

void gtkfaim_awaybox(void)
{
  GtkWidget *window;
#ifndef GNOME
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *vbox;
#endif
  char msg[] = "You are now marked as away.  All messages will be auto-responded to, until the button is pressed.";

#ifdef GNOME
  window = gnome_ok_dialog(msg);
  gtk_signal_connect(GTK_OBJECT(window), "clicked", GTK_SIGNAL_FUNC(gtkfaim_awaybox_cb),NULL);
#else
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_window_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  
  vbox = gtk_vbox_new(FALSE, 2);
  label = gtk_label_new(msg);
  button = gtk_button_new_with_label("I'm Back");
  
  gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(gtkfaim_awaybox_cb), window);

  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, TRUE, 2);
  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 2);

  gtk_container_add(GTK_CONTAINER(window), vbox);

  gtk_widget_show(label);
  gtk_widget_show(button);
  gtk_widget_show(vbox);
#endif
  gtk_widget_show(window);
  
  return;
}


void gtkfaim_away_cb(GtkWidget *widget, gpointer data)
{
  /* invert state */
  gtkfaim_away = !gtkfaim_away;
  gtkfaim_awaybox();

  return;
}
