/************************************************************************\
 * gtkFAIM
 * -------
 *
 * Description: Buddy list management dialogs and routines
 *
\************************************************************************/

#include <aim.h>
#include <signal.h>
#include <gtkfaim.h>
#include <gtk/gtkctree.h>
#include <gdk/gdkprivate.h>

#include "signon_pixmap.h"
/* #include "tree_minus.xpm"
#include "tree_plus.xpm" */


/*
 * Public macros
 */
#ifndef SIGNON_ICON_DELAY
#define SIGNON_ICON_DELAY 2000
#endif


/*
 * Forward declarations
 */
gtkfaim_buddy_t          *gtkfaim_buddy_find             ( char *sn );
gtkfaim_group_t          *gtkfaim_group_find             ( char *name );
gtkfaim_buddylist_node_t *gtkfaim_group_find_buddy       ( gtkfaim_group_t *group, char *sn );
gtkfaim_buddy_t          *gtkfaim_buddy_new              ( char *sn, char *alias );
gboolean                  gtkfaim_buddy_destroy          ( gtkfaim_buddy_t *buddy );
gtkfaim_group_t          *gtkfaim_group_new              ( char *sn );
gboolean                  gtkfaim_group_destroy          ( gtkfaim_group_t *group );
gboolean                  gtkfaim_group_add_buddy        ( gtkfaim_group_t *group,
                                                           gtkfaim_buddy_t *buddy );
gboolean                  gtkfaim_group_add_buddy_by_name( gtkfaim_group_t *group,
                                                           char *sn );
void                      gtkfaim_buddylist_refreshonline( void );
char                     *gtkfaim_buddylist_create_array ( void );
int                       gtkfaim_parse_oncoming_buddy   ( struct command_rx_struct *command );
int                       gtkfaim_parse_offgoing_buddy   ( struct command_rx_struct *command );
GtkWidget                *gtkfaim_buddylist_manage_create( void );
int                       gtkfaim_window_buddies_create  ( void );

#ifdef GNOME
void about_cb                   ( GtkWidget *widget, void *data );
static void gtkfaim_menu_signoff( GtkWidget *widget, gpointer data );
static void gtkfaim_menu_exit   ( GtkWidget *widget, gpointer data );
#endif


/*
 * Public globals
 */
GList           *gtkfaim_buddies         = NULL; /* List of buddies */
GList           *gtkfaim_groups          = NULL; /* List of groups  */
gtkfaim_group_t *gtkfaim_temporary_group = NULL; /* Temporary group */
gboolean         gtkfaim_defer_refresh   = FALSE;


/*
 * Private globals
 */
#ifdef GNOME
  /* Gnome menu stuff */
  static GnomeUIInfo file_options_menu[]= {
    {
      GNOME_APP_UI_ITEM,
      N_("Preferences"), N_("Edit Preferences..."),
      gtkfaim_create_prefs_window, NULL, NULL,
      GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PREFERENCES,
      0, 0, NULL
    },
    {
      GNOME_APP_UI_ITEM,
      N_("Change Password"), N_("Change your password"),
      gtkfaim_password_change_cb, NULL, NULL,
      GNOME_APP_PIXMAP_NONE, NULL,
     0, 0, NULL
    },
    GNOMEUIINFO_END
  };

  static GnomeUIInfo file_menu[]= {
    {
      GNOME_APP_UI_SUBTREE,
      N_("Options"), N_("Change your options"),
      file_options_menu, NULL, NULL,
      GNOME_APP_PIXMAP_NONE, NULL,
      0, 0, NULL
    },
    {
      GNOME_APP_UI_ITEM,
      N_("Signoff"), N_("Close your current connection"),
      gtkfaim_menu_signoff, NULL, NULL,
      GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CLOSE,
      0, 0, NULL
    },
    GNOMEUIINFO_SEPARATOR,
    {
      GNOME_APP_UI_ITEM,
      N_("Exit"), N_("Exit gtkFAIM"),
      gtkfaim_menu_exit, NULL, NULL,
      GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
      'Q', GDK_CONTROL_MASK, NULL
    },
    GNOMEUIINFO_END
  };
  static GnomeUIInfo people_find_menu[]=
  {
    { 
      GNOME_APP_UI_ITEM,
      N_("By Name..."), N_("Find a person by their screenname"),
      gtkfaim_usersearch_address_cb, (gpointer)1, NULL, 
      GNOME_APP_PIXMAP_NONE, NULL, 
      0, 0, NULL
    },
    {
      GNOME_APP_UI_ITEM,
      N_("By EMail..."),N_("Find a person by their email"),
      gtkfaim_usersearch_address_cb, 0, NULL, 
      GNOME_APP_PIXMAP_NONE, NULL, 
      0, 0, NULL
    },
    GNOMEUIINFO_END
  };
  static GnomeUIInfo people_menu[]=
  {
    {
      GNOME_APP_UI_SUBTREE,
      N_("Find a Buddy"),NULL,people_find_menu,
      GNOME_APP_PIXMAP_NONE, NULL, 
      0, 0, NULL
    },
    {
      GNOME_APP_UI_ITEM, N_("Away"), N_("Tell others your away"),
      gtkfaim_away_cb, NULL, NULL, 
      GNOME_APP_PIXMAP_NONE, NULL, 
      0, 0, NULL
    },
    GNOMEUIINFO_END
  };
  static GnomeUIInfo help_menu[]=
  {
    {
      GNOME_APP_UI_ITEM,
      N_("About..."), N_("Info about gtkFAIM"),
      about_cb, NULL, NULL,
      GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
      0, (GdkModifierType)0, NULL
    },
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_HELP("gtkFAIM"),
    GNOMEUIINFO_END
  };

  static GnomeUIInfo main_menu[]=
  {
    GNOMEUIINFO_SUBTREE(N_("File"), file_menu),
    GNOMEUIINFO_SUBTREE(N_("People"), people_menu),
    GNOMEUIINFO_SUBTREE(N_("Help"), help_menu),
    GNOMEUIINFO_END
  };
  
#endif


/*
 * Private functions
 */
static gint
gtkfaim_buddy_find_compare( gconstpointer a, gconstpointer b )
{
   const char      *sn    = (const char *)b;
   gtkfaim_buddy_t *buddy = (gtkfaim_buddy_t *)a;

   return sncmp( buddy->sn, sn );
}

static gint
gtkfaim_group_find_compare( gconstpointer a, gconstpointer b )
{
   const char      *name  = (const char *)b;
   gtkfaim_group_t *group = (gtkfaim_group_t *)a;

   return strcmp( group->name, name );
}

static gint
gtkfaim_group_find_buddy_compare( gconstpointer a, gconstpointer b )
{
   const char               *sn   = (const char *)b;
   gtkfaim_buddylist_node_t *node = (gtkfaim_buddylist_node_t *)a;

   return sncmp( node->buddy->sn, sn );
}

static gint
gtkfaim_manage_find_buddylist_node_compare( gconstpointer a, gconstpointer b )
{
   gtkfaim_buddylist_node_t *node        = (gtkfaim_buddylist_node_t *)b;
   gtkfaim_manage_node_t    *manage_node = (gtkfaim_manage_node_t *)a;

   if( ( manage_node->type == BUDDYLIST_TYPE_BUDDY ) && ( manage_node->data == node ) )
      return 0;

   return 1;
}

static void
gtkfaim_free_buddylist_node( gpointer data, gpointer user )
{
   gtkfaim_buddylist_node_t *node  = (gtkfaim_buddylist_node_t *)data;
   gtkfaim_buddy_t          *buddy = node->buddy;

   if( node )
   {
      /* Dereference buddy */
      buddy->references--;

      if( node->treenode )
      {
         /*
          * Remove disconnected buddy's node from tree
          */
         GList *clear_list = g_list_prepend( NULL, node->treenode );
         gtk_tree_remove_items( GTK_TREE( node->group->tree ), clear_list );
      }

      if( gtkfaim_window_buddies.manage_tree )
      {
         GtkCTree     *ctree;
         GtkCTreeNode *ctree_node;
    
#if debug > 2
         GTKFAIM_DEBUG2( "free_buddylist_node", "Searching ctree for buddy: ", buddy->sn, TRUE );
#endif
         ctree = GTK_CTREE( gtkfaim_window_buddies.manage_tree );
         ctree_node = gtk_ctree_find_by_row_data_custom( ctree, NULL, node, (GCompareFunc)gtkfaim_manage_find_buddylist_node_compare );

         if( ctree_node )
         {
            gpointer data = gtk_ctree_node_get_row_data( ctree, ctree_node );
            gtk_ctree_remove_node( ctree, ctree_node );
            g_free( data );
         }
      }

      if( gtkfaim_window_buddies.window != NULL )
         gtkfaim_buddylist_refreshonline();

      g_free( node );

      if( !buddy->references )
         gtkfaim_buddy_destroy( buddy );
   }
}

static void
gtkfaim_free_group( gpointer data, gpointer user )
{
   gtkfaim_group_t *group = (gtkfaim_group_t *)data;

   if( group )
   {
      gtkfaim_buddylist_node_t *node;
      
      /*
       * Defer refresh until we're done
       */
      gtkfaim_defer_refresh = TRUE;

      /*
       * Remove all buddies from group
       */
      while( ( node = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, 0 ) ) )
         gtkfaim_group_delete_buddy( group, node->buddy );

      /*
       * Okay we're done nuking buddies, we'll refresh next
       */
      gtkfaim_defer_refresh = FALSE;

      if( group->treenode )
      {
         /*
          * Remove group's node from tree
          */
         GList *clear_list = g_list_prepend( NULL, group->treenode );
         gtk_tree_remove_items( GTK_TREE( gtkfaim_window_buddies.monitor_tree ), clear_list );
      }

      if( gtkfaim_window_buddies.manage_tree && group->ctreenode )
      {
         GtkCTree *ctree = GTK_CTREE( gtkfaim_window_buddies.manage_tree );
         gpointer data = gtk_ctree_node_get_row_data( ctree, group->ctreenode );
         gtk_ctree_remove_node( ctree, group->ctreenode );
         g_free( data );
      }

      if( gtkfaim_window_buddies.window != NULL )
         gtkfaim_buddylist_refreshonline();

      g_free( node );

      gtkfaim_group_destroy( group );
   }
}

static void
gtkfaim_buddylist_node_compute_online_count( gpointer data, gpointer user )
{
   gtkfaim_buddylist_node_t *node  = (gtkfaim_buddylist_node_t *)data;;

   if( node && node->buddy->online )
      node->group->numonline++;
}

static void
gtkfaim_group_compute_online_count( gpointer data, gpointer user )
{
   gtkfaim_group_t *group = (gtkfaim_group_t *)data;

   group->numonline = 0;
   g_list_foreach( group->buddies,
                   (GFunc)gtkfaim_buddylist_node_compute_online_count, NULL );
}

static char *
gtkfaim_group_create_label( gtkfaim_group_t *group )
{
   char *newlabel;
   int digits1, digits2;
   int i;
   int numbuddies = g_list_length( group->buddies );

   for( i = group->numonline / 10, digits1 = 1; i; i /= 10, digits1++ );
   for( i = numbuddies / 10, digits2 = 1; i; i /= 10, digits2++ );
   newlabel = g_new( char, strlen( group->name ) + 5 + digits1 + digits2 + 1 );
   sprintf( newlabel, "%s (%d/%d)", group->name, group->numonline, numbuddies );

   return newlabel;
}

static void
gtkfaim_nonbuddy_info( gchar *destsn )
{
   char newdestsn[11];

   if( ( destsn == NULL ) || ( strlen( destsn ) < 4 ) )
   {
      GTKFAIM_DEBUG( "nonbuddy_openinfo", "SN MISSING! or is less than four characters", TRUE );
      gtkfaim_genericok( "Destination screen name invalid." );
      return;
   }
  
   if( strlen( destsn ) > 10 )
   {
      GTKFAIM_DEBUG( "nonbuddy_openinfo", "truncating to 10 bytes length", TRUE );
      memcpy( newdestsn, destsn, 10 );
      newdestsn[10] = '\0';
   }
   else
   {
      memcpy( newdestsn, destsn, strlen( destsn ) );
      newdestsn[strlen(destsn)] = '\0';
   }

   aim_getinfo( destsn );
}

static gint
gtkfaim_signon_hide_pixmap( gpointer data )
{
   gtkfaim_buddylist_node_t *node = (gtkfaim_buddylist_node_t *)data;

   gtk_widget_hide( GTK_TREE_ITEM( node->treenode )->pixmaps_box );

   node->signon_timer = 0;

   return 0;
}


/*
 * Gtk Event Handlers
 */
static gint
gtkfaim_buddylist_group_click( GtkWidget *widget, GdkEventButton *event, gpointer func_data )
{
   if( ( GTK_IS_TREE_ITEM( widget ) ) && ( event->type == GDK_2BUTTON_PRESS ) )
   {
      gtkfaim_group_t *group = (gtkfaim_group_t *)func_data;

      GTKFAIM_DEBUG2( "buddylist", "unimplemented feature: dblclicked the group ", group->name, TRUE );
   }
   return 0;
}

static gint
gtkfaim_buddylist_buddy_click( GtkWidget *widget, GdkEventButton *event, gpointer func_data )
{
   if( ( GTK_IS_TREE_ITEM( widget ) ) && ( event->type == GDK_2BUTTON_PRESS ) )
   {
      gtkfaim_buddy_t *buddy = (gtkfaim_buddy_t *)func_data;

      /* open up a blank IM window for this buddy */
      gtkfaim_add_imwindow( buddy->sn );
   }
   return 0;
}

static gboolean
gtkfaim_buddylist_manage_compare_drag( GtkCTree *ctree, GtkCTreeNode *source, GtkCTreeNode *parent, GtkCTreeNode *sibling )
{
   gtkfaim_manage_node_t *node;
   gtkfaim_manage_node_t *parent_node = NULL;

   node = (gtkfaim_manage_node_t *)gtk_ctree_node_get_row_data( ctree, source );
   if( parent )
      parent_node = (gtkfaim_manage_node_t *)gtk_ctree_node_get_row_data( ctree, parent );

   if( ( node->type == BUDDYLIST_TYPE_GROUP ) && ( parent_node == NULL ) )
      return TRUE;

   if( ( node->type == BUDDYLIST_TYPE_BUDDY ) && parent_node &&
       ( parent_node->type == BUDDYLIST_TYPE_GROUP ) &&
       ( (gtkfaim_group_t *)parent_node->data == ((gtkfaim_buddylist_node_t *)node->data)->group ) )
      return TRUE;

   return FALSE;
}

static void
gtkfaim_buddylist_manage_after_move( GtkCTree *ctree, GtkCTreeNode *child, GtkCTreeNode *parent, GtkCTreeNode *sibling, gpointer data )
{
   gtkfaim_manage_node_t *node;
   gtkfaim_manage_node_t *sibling_node = NULL;

   node = (gtkfaim_manage_node_t *)gtk_ctree_node_get_row_data( ctree, child );
   if( sibling )
      sibling_node = (gtkfaim_manage_node_t *)gtk_ctree_node_get_row_data( ctree, sibling );

   switch( node->type )
   {
      case BUDDYLIST_TYPE_GROUP:
      {
         gtkfaim_group_t          *group      = (gtkfaim_group_t *)node->data;
         GList                    *clear_list = g_list_prepend( NULL, group->treenode );
         gtkfaim_buddylist_node_t *buddynode;
         gint                      i;

#if debug > 2
         GTKFAIM_DEBUG( "buddylist", "Reordering groups", TRUE );
#endif

         /*
          * Clear buddy nodes (we don't remove them from the tree, as
          * just removing the group node will automatically remove the
          * buddy nodes from the tree)
          */
         for( i = 0; ( buddynode = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, i ) ); i++ )
            buddynode->treenode = NULL;

#if debug > 2
         GTKFAIM_DEBUG( "buddylist", "Reordering groups", TRUE );
#endif
         /*
          * Remove group node from monitor tree
          */
         gtk_tree_remove_items( GTK_TREE( gtkfaim_window_buddies.monitor_tree ), clear_list );

#if debug > 2
         GTKFAIM_DEBUG( "buddylist", "Moving group in list", TRUE );
#endif
         /*
          * Move group to proper place in list
          */
         gtkfaim_groups = g_list_remove( gtkfaim_groups, group );
         if( sibling_node )
         {
            gint index = g_list_index( gtkfaim_groups, sibling_node->data );
            gtkfaim_groups = g_list_insert( gtkfaim_groups, group, index );
         }
         else
            gtkfaim_groups = g_list_append( gtkfaim_groups, group );

         group->treenode = NULL;

         break;
      }
      case BUDDYLIST_TYPE_BUDDY:
      {
         gtkfaim_buddylist_node_t *buddynode = (gtkfaim_buddylist_node_t *)node->data;
         GList                    *clear_list = g_list_prepend( NULL, buddynode->treenode );

#if debug > 2
         GTKFAIM_DEBUG( "buddylist", "Reordering buddy in group", TRUE );
#endif
         /*
          * Remove group node from monitor tree
          */
         gtk_tree_remove_items( GTK_TREE( buddynode->group->tree ), clear_list );

         /*
          * Move group to proper place in list
          */
         buddynode->group->buddies = g_list_remove( buddynode->group->buddies, buddynode );
         if( sibling_node )
         {
            gint index = g_list_index( buddynode->group->buddies, sibling_node->data );
            buddynode->group->buddies = g_list_insert( buddynode->group->buddies, buddynode, index );
         }
         else
            buddynode->group->buddies = g_list_append( buddynode->group->buddies, buddynode );

         buddynode->treenode = NULL;

         break;
      }
   }

#if debug > 2
   GTKFAIM_DEBUG( "buddylist", "Calling refreshonline", TRUE );
#endif
   /*
    * That's all we need to do, gtkfaim_buddylist_refreshonline()
    * will do the rest.
    */
   gtkfaim_buddylist_refreshonline();
}

static void
gtkfaim_nonbuddy_addbuddycancel( GtkWidget *widget, gpointer data )
{
   if( gtkfaim_nonbuddy_addbuddy )
   {
      gtk_widget_hide( gtkfaim_nonbuddy_addbuddy->window );
      gtk_widget_destroy( gtkfaim_nonbuddy_addbuddy->window);
      g_free( gtkfaim_nonbuddy_addbuddy );
      gtkfaim_nonbuddy_addbuddy = NULL;
   }
}

static void
gtkfaim_nonbuddy_addgroupcancel( GtkWidget *widget, gpointer data )
{
   if( gtkfaim_nonbuddy_addgroup )
   {
      gtk_widget_hide( gtkfaim_nonbuddy_addgroup->window );
      gtk_widget_destroy( gtkfaim_nonbuddy_addgroup->window);
      g_free( gtkfaim_nonbuddy_addgroup );
      gtkfaim_nonbuddy_addgroup = NULL;
   }
}

static void
gtkfaim_nonbuddy_infocancel( GtkWidget *widget, gpointer data )
{
   if( gtkfaim_nonbuddy_getinfo != NULL )
   {
      gtk_widget_hide( gtkfaim_nonbuddy_getinfo->window );
      gtk_widget_destroy( gtkfaim_nonbuddy_getinfo->window);
      g_free( gtkfaim_nonbuddy_getinfo );
      gtkfaim_nonbuddy_getinfo = NULL;
   }
}

#ifdef GNOME
static void
gtkfaim_nonbuddy_info_click( gchar *string, gpointer data )
#else
static void
gtkfaim_nonbuddy_info_click( GtkWidget *widget, gpointer data )
#endif
{
   char *destsn;
  
  /* grab only the first 10 chars (the maximum AIM SN length) */
#ifdef GNOME
   destsn = string;
   if( destsn == NULL )
      return;
#else
   destsn = gtk_editable_get_chars( GTK_EDITABLE( gtkfaim_nonbuddy_getinfo->textbox ), 0, -1 );
#endif

   gtkfaim_nonbuddy_info( destsn );

   g_free( destsn );

   /* close the window */
   gtkfaim_nonbuddy_infocancel( NULL, NULL );
}

static void
gtkfaim_nonbuddy_addbuddy_click( GtkWidget *widget, gpointer data )
{
   gtkfaim_group_t *group;
   char            *destsn;
   char            *destalias;
   char            *destgrp;
  
   /* grab only the first 10 chars (the maximum AIM SN length) */
   destsn = gtk_editable_get_chars( GTK_EDITABLE( gtkfaim_nonbuddy_addbuddy->textbox ), 0, -1 );
   destalias = gtk_editable_get_chars( GTK_EDITABLE( gtkfaim_nonbuddy_addbuddy->alias_textbox ), 0, -1 );
   if( *destalias == '\0' )
   {
      g_free( destalias );
      destalias = NULL;
   }
   destgrp = gtk_editable_get_chars( GTK_EDITABLE( GTK_COMBO( gtkfaim_nonbuddy_addbuddy->combo )->entry ), 0, -1 );
   if( ( group = gtkfaim_group_find( destgrp ) ) == NULL )
   {
      GTKFAIM_DEBUG( "addbuddy", "Mismatched group name in selection.", TRUE );
   }
   else
   {
      if( gtkfaim_group_find_buddy( group, destsn ) )
      {
         GTKFAIM_DEBUG( "addbuddy", "Attempted to add existing buddy to group.", TRUE );
      }
      else
      {
         gtkfaim_buddy_t *buddy = gtkfaim_buddy_new( destsn, destalias );
         gtkfaim_group_add_buddy( group, buddy );
      }
   }

   g_free( destsn );
   if( destalias )
      g_free( destalias );
   g_free( destgrp);

   /* close the window */
   gtkfaim_nonbuddy_addbuddycancel( NULL, NULL );
}

static void
gtkfaim_nonbuddy_addgroup_click( GtkWidget *widget, gpointer data )
{
   char *destname;
  
   /* grab only the first 10 chars (the maximum AIM SN length) */
   destname = gtk_editable_get_chars( GTK_EDITABLE( gtkfaim_nonbuddy_addgroup->textbox ), 0, -1 );
   if( gtkfaim_group_find( destname ) )
   {
      GTKFAIM_DEBUG( "addbuddy", "Attempted to add existing group.", TRUE );
   }
   else
   {
      gtkfaim_group_new( destname );
   }

   g_free( destname );

   /* close the window */
   gtkfaim_nonbuddy_addgroupcancel( NULL, NULL );
}

static void
gtkfaim_buddylist_add_buddy_click( GtkWidget *widget, gpointer data )
{
   GtkWidget       *abwindow;
   GtkWidget       *abwindow_vbox;
   GtkWidget       *abwindow_snlabel;
   GtkWidget       *abwindow_aliaslabel;
   GtkWidget       *abwindow_grouplabel;
   GtkWidget       *abwindow_combo;
   GtkWidget       *abwindow_accept;
   GtkWidget       *abwindow_cancel;
   GtkWidget       *abwindow_entry;
   GtkWidget       *abwindow_alias_entry;
   GList           *popdown_list = NULL;
   gtkfaim_group_t *group;
   gint             curnode;

   /* we can only have one of these in existance at a time */
   if( gtkfaim_nonbuddy_addbuddy )
      return;

   abwindow = gtk_window_new( GTK_WINDOW_TOPLEVEL );
   gtk_window_set_title( (GtkWindow *)abwindow, "Add Buddy" );

   gtk_signal_connect( GTK_OBJECT( abwindow ), "delete_event",
                       GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_addbuddycancel ),
                       NULL );

   gtk_window_position( GTK_WINDOW( abwindow ), GTK_WIN_POS_CENTER );
  
   abwindow_vbox = gtk_vbox_new( FALSE, 0 );
   abwindow_snlabel = gtk_label_new( "Screenname:" );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox ), abwindow_snlabel, FALSE, FALSE, 0 );
  
   abwindow_entry = gtk_entry_new_with_max_length( 10 );
   gtk_widget_set_usize( GTK_WIDGET( abwindow_entry ), 1, 20 );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox ), abwindow_entry, FALSE, FALSE, 2 );

   abwindow_aliaslabel = gtk_label_new( "Alias:" );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox ), abwindow_aliaslabel, FALSE, FALSE, 0 );

   abwindow_alias_entry = gtk_entry_new_with_max_length( 50 );
   gtk_widget_set_usize( GTK_WIDGET( abwindow_alias_entry ), 1, 20 );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox ), abwindow_alias_entry, FALSE, FALSE, 2 );

   abwindow_grouplabel = gtk_label_new( "Group:" );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox ), abwindow_grouplabel, FALSE, FALSE, 0 );

   for( curnode = 0;
        ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
        curnode++ )
      if( group != gtkfaim_temporary_group )
         popdown_list = g_list_append( popdown_list, group->name );

   abwindow_combo = gtk_combo_new();
   gtk_combo_set_popdown_strings( GTK_COMBO( abwindow_combo ), popdown_list );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox ), abwindow_combo, FALSE, FALSE, 0 );

   abwindow_accept = gtk_button_new_with_label( "Accept" );
   gtk_signal_connect( GTK_OBJECT( abwindow_accept ), "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_addbuddy_click ), NULL );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox ), abwindow_accept, FALSE, FALSE, 0 );

   abwindow_cancel = gtk_button_new_with_label( "Cancel" );
   gtk_signal_connect( GTK_OBJECT( abwindow_cancel ), "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_addbuddycancel ), NULL );
   gtk_box_pack_start( GTK_BOX( abwindow_vbox), abwindow_cancel, FALSE, FALSE, 0 );

   gtk_container_add( GTK_CONTAINER( abwindow ), abwindow_vbox );

   /* go global */
   gtkfaim_nonbuddy_addbuddy = g_new( gtkfaim_window_nonbuddy_addbuddy_t, 1 );

   gtkfaim_nonbuddy_addbuddy->window = abwindow;
   gtkfaim_nonbuddy_addbuddy->vbox = abwindow_vbox;
   gtkfaim_nonbuddy_addbuddy->snlabel = abwindow_snlabel;
   gtkfaim_nonbuddy_addbuddy->aliaslabel = abwindow_aliaslabel;
   gtkfaim_nonbuddy_addbuddy->grouplabel = abwindow_grouplabel;
   gtkfaim_nonbuddy_addbuddy->combo = abwindow_combo;
   gtkfaim_nonbuddy_addbuddy->accept = abwindow_accept;
   gtkfaim_nonbuddy_addbuddy->cancel = abwindow_cancel;
   gtkfaim_nonbuddy_addbuddy->textbox = abwindow_entry;
   gtkfaim_nonbuddy_addbuddy->alias_textbox = abwindow_alias_entry;

   /* show it */
   gtk_widget_show( abwindow_alias_entry );
   gtk_widget_show( abwindow_entry );
   gtk_widget_show( abwindow_cancel );
   gtk_widget_show( abwindow_accept );
   gtk_widget_show( abwindow_combo );
   gtk_widget_show( abwindow_grouplabel );
   gtk_widget_show( abwindow_aliaslabel );
   gtk_widget_show( abwindow_snlabel );
   gtk_widget_show( abwindow_vbox );
   gtk_widget_show( abwindow );
}

static void
gtkfaim_buddylist_add_group_click( GtkWidget *widget, gpointer data )
{
   GtkWidget       *agwindow;
   GtkWidget       *agwindow_vbox;
   GtkWidget       *agwindow_grouplabel;
   GtkWidget       *agwindow_accept;
   GtkWidget       *agwindow_cancel;
   GtkWidget       *agwindow_entry;

   /* we can only have one of these in existance at a time */
   if( gtkfaim_nonbuddy_addgroup )
      return;

   agwindow = gtk_window_new( GTK_WINDOW_TOPLEVEL );
   gtk_window_set_title( (GtkWindow *)agwindow, "Add Group" );

   gtk_signal_connect( GTK_OBJECT( agwindow ), "delete_event",
                       GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_addgroupcancel ),
                       NULL );

   gtk_window_position( GTK_WINDOW( agwindow ), GTK_WIN_POS_CENTER );
  
   agwindow_vbox = gtk_vbox_new( FALSE, 0 );
   agwindow_grouplabel = gtk_label_new( "Group Name:" );
   gtk_box_pack_start( GTK_BOX( agwindow_vbox ), agwindow_grouplabel, FALSE, FALSE, 0 );

   agwindow_entry = gtk_entry_new_with_max_length( 50 );
   gtk_widget_set_usize( GTK_WIDGET( agwindow_entry ), 1, 20 );
   gtk_box_pack_start( GTK_BOX( agwindow_vbox ), agwindow_entry, FALSE, FALSE, 2 );

   agwindow_accept = gtk_button_new_with_label( "Accept" );
   gtk_signal_connect( GTK_OBJECT( agwindow_accept ), "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_addgroup_click ), NULL );
   gtk_box_pack_start( GTK_BOX( agwindow_vbox ), agwindow_accept, FALSE, FALSE, 0 );

   agwindow_cancel = gtk_button_new_with_label( "Cancel" );
   gtk_signal_connect( GTK_OBJECT( agwindow_cancel ), "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_addgroupcancel ), NULL );
   gtk_box_pack_start( GTK_BOX( agwindow_vbox), agwindow_cancel, FALSE, FALSE, 0 );

   gtk_container_add( GTK_CONTAINER( agwindow ), agwindow_vbox );

   /* go global */
   gtkfaim_nonbuddy_addgroup = g_new( gtkfaim_window_nonbuddy_addgroup_t, 1 );

   gtkfaim_nonbuddy_addgroup->window = agwindow;
   gtkfaim_nonbuddy_addgroup->vbox = agwindow_vbox;
   gtkfaim_nonbuddy_addgroup->grouplabel = agwindow_grouplabel;
   gtkfaim_nonbuddy_addgroup->accept = agwindow_accept;
   gtkfaim_nonbuddy_addgroup->cancel = agwindow_cancel;
   gtkfaim_nonbuddy_addgroup->textbox = agwindow_entry;

   /* show it */
   gtk_widget_show( agwindow_entry );
   gtk_widget_show( agwindow_cancel );
   gtk_widget_show( agwindow_accept );
   gtk_widget_show( agwindow_grouplabel );
   gtk_widget_show( agwindow_vbox );
   gtk_widget_show( agwindow );
}

static void
gtkfaim_buddylist_remove_click( GtkWidget *widget, gpointer data )
{
   GtkCTree *ctree = GTK_CTREE( gtkfaim_window_buddies.manage_tree );
   GtkCList *clist = GTK_CLIST( ctree );
   if( clist->selection )
   {
      GtkCTreeNode          *ctree_node;
      gtkfaim_manage_node_t *node;
      
      ctree_node = GTK_CTREE_NODE( clist->selection->data );
      node = (gtkfaim_manage_node_t *)gtk_ctree_node_get_row_data( ctree, ctree_node );

      switch( node->type )
      {
         case BUDDYLIST_TYPE_GROUP:
         {
            gtkfaim_group_t *group = (gtkfaim_group_t *)node->data;

            gtkfaim_group_destroy( group );
            break;
         }
         case BUDDYLIST_TYPE_BUDDY:
         {
            gtkfaim_buddylist_node_t *buddynode = (gtkfaim_buddylist_node_t *)node->data;

            gtkfaim_group_delete_buddy( buddynode->group, buddynode->buddy );
            break;
         }
      }
   }
}

static void
gtkfaim_buddylist_info_click( GtkWidget *widget, gpointer data )
{
   GtkWidget *inwindow;
#ifndef GNOME
   GtkWidget *inwindow_vbox;
   GtkWidget *inwindow_snlabel;
   GtkWidget *inwindow_inopen;
   GtkWidget *inwindow_cancel;
   GtkWidget *inwindow_entry;
#endif
   GList *selection;

   /* we can only have one of these in existance at a time */
   if( gtkfaim_nonbuddy_getinfo )
      return;

   selection = GTK_TREE( gtkfaim_window_buddies.monitor_tree )->selection;
   if( selection )
   {
      gtkfaim_group_t *group;
      gint             curnode;

      for( curnode = 0;
           ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
           curnode++ )
      {
         gtkfaim_buddylist_node_t *node;
         gint                      i;

         /*
          * If selected node is a group, return
          */
         if( group->treenode == selection->data )
            return;

         for( i = 0;
              ( node = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, i ) );
              i++ )
         {
            if( node->treenode == selection->data )
            {
               gtkfaim_nonbuddy_info( GTK_LABEL( GTK_BIN( node->treenode )->child )->label );
               return;
            }
         }
      }
   }
  
#ifdef GNOME
   inwindow = gnome_request_string_dialog( _("Screenname to get info on"), GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_info_click ), NULL );
   gtk_signal_connect( GTK_OBJECT( inwindow ), "close", GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_infocancel ), NULL );
/*  inwindow = gnome_app_new( _("gtkFAIM"), _("Get Info") );
   gtk_widget_realize( inwindow ); */
#else
   inwindow = gtk_window_new( GTK_WINDOW_TOPLEVEL );
   gtk_window_set_title( (GtkWindow *)inwindow, "Get Info" );
#endif

   gtk_signal_connect( GTK_OBJECT( inwindow ), "delete_event",
                       GTK_SIGNAL_FUNC( gtkfaim_nonbuddy_infocancel ),
                       NULL );

   gtk_window_position( GTK_WINDOW( inwindow ), GTK_WIN_POS_CENTER );
  
#ifndef GNOME
   inwindow_vbox = gtk_vbox_new( FALSE, 0 );
   inwindow_snlabel = gtk_label_new( "Screenname:" );
   gtk_box_pack_start( GTK_BOX( inwindow_vbox ), inwindow_snlabel, FALSE, FALSE, 0 );
  
   inwindow_entry = gtk_entry_new_with_max_length( 10 );
   gtk_widget_set_usize( GTK_WIDGET( inwindow_entry ), 1, 20 );
   gtk_box_pack_start( GTK_BOX( inwindow_vbox ), inwindow_entry, FALSE, FALSE, 2 );

   inwindow_inopen = gtk_button_new_with_label( "Get Information" );
   gtk_signal_connect( GTK_OBJECT( inwindow_inopen ), "clicked", GTK_SIGNAL_FUNC(gtkfaim_nonbuddy_info_click), NULL );
   gtk_box_pack_start( GTK_BOX( inwindow_vbox ), inwindow_inopen, FALSE, FALSE, 0 );

   inwindow_cancel = gtk_button_new_with_label( "Cancel" );
   gtk_signal_connect( GTK_OBJECT( inwindow_cancel ), "clicked", GTK_SIGNAL_FUNC(gtkfaim_nonbuddy_infocancel), NULL );
   gtk_box_pack_start( GTK_BOX( inwindow_vbox), inwindow_cancel, FALSE, FALSE, 0 );

   gtk_container_add( GTK_CONTAINER( inwindow ), inwindow_vbox );
#endif

   /* go global */
   gtkfaim_nonbuddy_getinfo = g_new( gtkfaim_window_nonbuddy_getinfo_t, 1 );

   gtkfaim_nonbuddy_getinfo->window = inwindow;
#ifndef GNOME
   gtkfaim_nonbuddy_getinfo->vbox = inwindow_vbox;
   gtkfaim_nonbuddy_getinfo->snlabel = inwindow_snlabel;
   gtkfaim_nonbuddy_getinfo->inopen = inwindow_inopen;
   gtkfaim_nonbuddy_getinfo->cancel = inwindow_cancel;
   gtkfaim_nonbuddy_getinfo->textbox = inwindow_entry;

   /* show it */
   gtk_widget_show( inwindow_entry );
   gtk_widget_show( inwindow_cancel );
   gtk_widget_show( inwindow_inopen );
   gtk_widget_show( inwindow_snlabel );
   gtk_widget_show( inwindow_vbox );
#endif
   gtk_widget_show( inwindow );
}

static void
gtkfaim_menu_signoff( GtkWidget *widget, gpointer data )
{
   extern sig_t     gtkfaim_old_sigint_handler;
   GList           *clear_list = NULL;
   gtkfaim_group_t *group;
   gint             curnode;

   gtkfaim_logoff(); /* logsoff and does onlogoff event */

   /*
    * Clear out monitor tree
    */
   for( curnode = 0;
        ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
        curnode++ )
   {
      if( group->treenode )
      {
         gtkfaim_buddylist_node_t *buddynode;
         gint                      i;

         /*
          * Clear buddy nodes (we don't remove them from the tree, as
          * just removing the group node will automatically remove the
          * buddy nodes from the tree)
          */
         for( i = 0;
              ( buddynode = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, i ) );
              i++ )
            buddynode->treenode = NULL;

         clear_list = g_list_prepend( clear_list, group->treenode );
         group->treenode = NULL;
      }
   }

   if( clear_list )
      gtk_tree_remove_items( GTK_TREE( gtkfaim_window_buddies.monitor_tree ), clear_list );

   gtk_widget_hide( gtkfaim_window_buddies.window );
   gtk_widget_destroy( gtkfaim_window_buddies.window );
   bzero( &gtkfaim_window_buddies, sizeof( gtkfaim_window_buddies ) );

   gtk_main_quit();

   /*
    * Restore gtk signal handler
    */
   signal( SIGINT, gtkfaim_old_sigint_handler );

   gtkfaim_create_loginwindow();

   gtk_main();
}

static void
gtkfaim_menu_exit( GtkWidget *widget, gpointer data )
{
   gtkfaim_logoff();
   gtkfaim_destroy_event( widget, data );
}


/*
 * Public functions
 */
gtkfaim_buddy_t *
gtkfaim_buddy_find( char *sn )
{
   GList *node = g_list_find_custom( gtkfaim_buddies, (gpointer)sn,
                                     (GCompareFunc)gtkfaim_buddy_find_compare );

   if( node == NULL )
      return NULL;

   return (gtkfaim_buddy_t *)node->data;
}

gtkfaim_group_t *
gtkfaim_group_find( char *name )
{
   GList *node = g_list_find_custom( gtkfaim_groups, (gpointer)name,
                                     (GCompareFunc)gtkfaim_group_find_compare );

   if( node == NULL )
      return NULL;

   return (gtkfaim_group_t *)node->data;
}

gtkfaim_buddylist_node_t *
gtkfaim_group_find_buddy( gtkfaim_group_t *group, char *sn )
{
   GList *node;

   if( group == NULL )
      return NULL;

   node = g_list_find_custom( group->buddies, (gpointer)sn,
                              (GCompareFunc)gtkfaim_group_find_buddy_compare );

   if( node == NULL )
      return NULL;

   return (gtkfaim_buddylist_node_t *)node->data;
}

gtkfaim_buddy_t *
gtkfaim_buddy_new( char *sn, char *alias )
{
   gtkfaim_buddy_t *buddy = gtkfaim_buddy_find( sn );

   if( buddy != NULL )
   {
#if debug > 2
      GTKFAIM_DEBUG( "buddy", "Found buddy match, not adding", TRUE );
#endif
      return buddy;
   }

   buddy = g_new( gtkfaim_buddy_t, 1 );
   bzero( buddy, sizeof(*buddy) );

   buddy->sn = g_new( char, strlen( sn ) + 1 );
   memcpy( buddy->sn, sn, strlen( sn ) );
   buddy->sn[strlen(sn)] = '\0';

   if( alias )
   {
      buddy->alias = g_new( char, strlen( alias ) + 1 );
      memcpy( buddy->alias, alias, strlen( alias ) );
      buddy->alias[strlen(alias)] = '\0';
   }

   gtkfaim_buddies = g_list_append( gtkfaim_buddies, buddy );

   if( gtkfaim_window_buddies.window )
   {
      /* Update our buddylist on AIM server */
      aim_add_buddy( buddy->sn );
#if debug > 2
      GTKFAIM_DEBUG( "buddy", "Refreshing buddylist on AIM server.", TRUE );
#endif
   }

   return buddy;
}

gtkfaim_group_t *
gtkfaim_group_new( char *name )
{
   gtkfaim_group_t *group = gtkfaim_group_find( name );

   if( group )
   {
#if debug > 2
      GTKFAIM_DEBUG( "group", "Found group match, not adding", TRUE );
#endif
      return group;
   }

   group = g_new( gtkfaim_group_t, 1 );
   bzero( group, sizeof(*group) );

   group->name = g_new( char, strlen( name ) + 1 );
   memcpy( group->name, name, strlen( name ) );
   group->name[strlen(name)] = '\0';

   if( !strcmp( group->name, "Temporary" ) )
      gtkfaim_temporary_group = group;

#if debug > 2
   GTKFAIM_DEBUG2( "group", "Appending group: ", name, TRUE );
#endif
   gtkfaim_groups = g_list_append( gtkfaim_groups, group );

   if( group != gtkfaim_temporary_group )
   {
      if( gtkfaim_window_buddies.manage_tree )
      {
         GtkCTree              *newroot     = GTK_CTREE( gtkfaim_window_buddies.manage_tree );
         GtkCTreeNode          *sibling;
         gtkfaim_manage_node_t *manage_node;
         char                  *text[1];

         manage_node = g_new( gtkfaim_manage_node_t, 1 );
         manage_node->type = BUDDYLIST_TYPE_GROUP;
         manage_node->data = group;

         text[0] = (gchar *)group->name;
         sibling = gtk_ctree_insert_node( GTK_CTREE( newroot ), NULL, NULL, text, 5, NULL, NULL, NULL,
                                          NULL, FALSE, TRUE );
         gtk_ctree_node_set_row_data( GTK_CTREE( newroot ), sibling, (gpointer *)manage_node );
         group->ctreenode = sibling;
      }

      if( gtkfaim_window_buddies.window != NULL )
         gtkfaim_buddylist_refreshonline();
   }

   return group;
}

gboolean
gtkfaim_buddy_destroy( gtkfaim_buddy_t *buddy )
{
   static gboolean  entered = FALSE;
   gtkfaim_group_t *group;
   gint             curnode;

   /* Prevent possible recursion deadlock */
   if( entered ) return FALSE;

   entered = TRUE;

   for( curnode = 0;
        ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
        curnode++ )
   {
      gtkfaim_group_delete_buddy( group, buddy );
   }

   /* Close the IM window, if it exists */
   if( buddy->imwindow )
      gtkfaim_imwindow_close( buddy->imwindow->button_close, NULL );

   /* Remove buddy from list */
   gtkfaim_buddies = g_list_remove( gtkfaim_buddies, buddy );

   if( gtkfaim_window_buddies.window )
   {
      /* Update our buddylist on AIM server */
      aim_remove_buddy( buddy->sn );
#if debug > 2
      GTKFAIM_DEBUG( "buddy", "Refreshing buddylist on AIM server.", TRUE );
#endif
   }

   /* Free resources used by buddy structure */
   g_free( buddy->sn );
   if( buddy->alias )
      g_free( buddy->alias );
   g_free( buddy );

   /* Safe to enter again */
   entered = FALSE;

   return TRUE;
}

gboolean
gtkfaim_group_destroy( gtkfaim_group_t *group )
{
   static gboolean  entered = FALSE;

   /* Prevent possible recursion deadlock */
   if( entered ) return FALSE;

   entered = TRUE;

   /* Remove group from list */
   gtkfaim_groups = g_list_remove( gtkfaim_groups, group );

   /* Remove group from monitor and manage trees */
   gtkfaim_free_group( group, NULL );

   if( group == gtkfaim_temporary_group )
      gtkfaim_temporary_group = NULL;

   /* Free resources used by group structure */
   g_free( group->name );
   g_free( group );

   /* Safe to enter again */
   entered = FALSE;

   return TRUE;
}

gboolean
gtkfaim_group_add_buddy( gtkfaim_group_t *group, gtkfaim_buddy_t *buddy )
{
   gtkfaim_buddylist_node_t *node;

   if( group == NULL )
   {
#if debug > 2
      GTKFAIM_DEBUG( "group", "Null group passed to gtkfaim_group_add_buddy", TRUE );
#endif
      return FALSE;
   }

   if( buddy == NULL )
   {
#if debug > 2
      GTKFAIM_DEBUG( "group", "Null buddy passed to gtkfaim_group_add_buddy", TRUE );
#endif
      return FALSE;
   }

   if( gtkfaim_group_find_buddy( group, buddy->sn ) != NULL )
   {
#if debug > 2
      GTKFAIM_DEBUG( "group", "Buddy already part of group", TRUE );
#endif
      return FALSE;
   }

   node = g_new( gtkfaim_buddylist_node_t, 1 );
   bzero( node, sizeof( *node ) );

   node->buddy = buddy;
   node->group = group;
   buddy->references++;
   group->buddies = g_list_append( group->buddies, node );

   if( group != gtkfaim_temporary_group )
   {
      if( group->tree )
      {
         GtkCTree              *newroot     = GTK_CTREE( gtkfaim_window_buddies.manage_tree );
         GtkCTreeNode          *sibling;
         gtkfaim_manage_node_t *manage_node;
         char                  *label;
         char                  *text[1];

         if( node->buddy->alias )
         {
            label = g_new( char, strlen( node->buddy->sn ) + strlen( node->buddy->alias ) + 4 );
            sprintf( label, "%s (%s)", node->buddy->sn, node->buddy->alias );
         }
         else
            label = node->buddy->sn;

         manage_node = g_new( gtkfaim_manage_node_t, 1 );
         manage_node->type = BUDDYLIST_TYPE_BUDDY;
         manage_node->data = node;

         text[0] = (gchar *)label;
         sibling = gtk_ctree_insert_node( GTK_CTREE( newroot ), GTK_CTREE_NODE( group->ctreenode ),
                                          NULL, text, 5, NULL, NULL, NULL, NULL, TRUE, FALSE );
         gtk_ctree_node_set_row_data( GTK_CTREE( newroot ), sibling, (gpointer *)manage_node );
         if( node->buddy->alias )
            g_free( label );
      }

      if( gtkfaim_window_buddies.window )
         gtkfaim_buddylist_refreshonline();
   }

   return TRUE;
}

gboolean
gtkfaim_group_add_buddy_by_name( gtkfaim_group_t *group, char *sn )
{
   return gtkfaim_group_add_buddy( group, gtkfaim_buddy_find( sn ) );
}

gboolean
gtkfaim_group_delete_buddy( gtkfaim_group_t *group, gtkfaim_buddy_t *buddy )
{
   gtkfaim_buddylist_node_t *node;

   if( group == NULL )
   {
#if debug > 2
      GTKFAIM_DEBUG( "group", "Null group passed to gtkfaim_group_remove_buddy", TRUE );
#endif
      return FALSE;
   }

   if( buddy == NULL )
   {
#if debug > 2
      GTKFAIM_DEBUG( "group", "Null buddy passed to gtkfaim_group_remove_buddy", TRUE );
#endif
      return FALSE;
   }

   if( ( node = gtkfaim_group_find_buddy( group, buddy->sn ) ) == NULL )
   {
#if debug > 2
      GTKFAIM_DEBUG( "group", "Buddy already not part of group", TRUE );
#endif
      return FALSE;
   }

   group->buddies = g_list_remove( group->buddies, node );
   gtkfaim_free_buddylist_node( node, NULL );

   return TRUE;
}

gboolean
gtkfaim_group_delete_buddy_by_name( gtkfaim_group_t *group, char *sn )
{
   return gtkfaim_group_delete_buddy( group, gtkfaim_buddy_find( sn ) );
}

void
gtkfaim_buddylist_refreshonline( void )
{
   gtkfaim_buddylist_node_t *node;
   gtkfaim_group_t          *group;
   gint                      curnode = 0;

   /*
    * If deferred refesh is flagged, just return
    */
   if( gtkfaim_defer_refresh )
      return;

#if debug > 2
   GTKFAIM_DEBUG( "buddylist", "Computing number of buddies", TRUE );
#endif
   /*
    * Compute number of buddies online for each group
    */
   g_list_foreach( gtkfaim_groups,
                   (GFunc)gtkfaim_group_compute_online_count, NULL );

   /*
    * Update monitor tree
    */
   for( curnode = 0;
        ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
        curnode++ )
   {
      gboolean constructed = FALSE;
      gint     buddynode;
      gint     lastonline;

      if( group == gtkfaim_temporary_group )
         continue;

      if( group->treenode == NULL )
      {
         GdkBitmap *mask_minus;
         GdkBitmap *mask_plus;
#if debug > 2
         GTKFAIM_DEBUG2( "buddylist", "Adding group to tree: ", group->name, TRUE );
#endif

         /*
          * Update group list order
          */
         group->treenode = gtk_tree_item_new_with_label( gtkfaim_group_create_label( group ) );

/*         gtk_pixmap_set( GTK_PIXMAP( GTK_TREE_ITEM( group->treenode )->minus_pix_widget ), gdk_pixmap_create_from_xpm_d( GTK_WIDGET( gtkfaim_window_buddies.window )->window, &mask_minus, NULL, tree_minus ), mask_minus );
         gtk_pixmap_set( GTK_PIXMAP( GTK_TREE_ITEM( group->treenode )->plus_pix_widget ), gdk_pixmap_create_from_xpm_d( GTK_WIDGET( gtkfaim_window_buddies.window )->window, &mask_plus, NULL, tree_plus ), mask_plus );
*/

         gtk_signal_connect( GTK_OBJECT( group->treenode ), "button_press_event", GTK_SIGNAL_FUNC( gtkfaim_buddylist_group_click ), group );
         gtk_tree_insert( GTK_TREE( gtkfaim_window_buddies.monitor_tree ), group->treenode, curnode );

         constructed = TRUE;
      }

      if( ( GTK_TREE_ITEM( group->treenode )->subtree == NULL ) || ( group->tree == NULL ) )
      {
         /* bind the group nodes to the main tree */
         group->tree = gtk_tree_new();
         gtk_tree_item_set_subtree( GTK_TREE_ITEM( group->treenode ), group->tree );
         gtk_widget_show( group->tree );
         gtk_widget_show( group->treenode );
         gtk_tree_item_expand( GTK_TREE_ITEM( group->treenode ) );
      }

      if( !constructed )
      {
#if debug > 2
         GTKFAIM_DEBUG2( "buddylist", "Updating group label: ", group->name, TRUE );
#endif
         if( GTK_BIN( group->treenode ) != NULL )
            gtk_label_set( GTK_LABEL( GTK_BIN( group->treenode )->child ),
                           gtkfaim_group_create_label( group ) );
      }

      for( buddynode = 0, lastonline = 0;
           ( node = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, buddynode ) );
           buddynode++ )
      {
         if( node->group != gtkfaim_temporary_group )
         {
            if( node->buddy->online )
            {
               if( node->treenode == NULL )
               {
                  GdkBitmap *mask = NULL;
                  GtkWidget *pixmap = gtk_pixmap_new( gdk_pixmap_create_from_xpm_d( GTK_WIDGET( gtkfaim_window_buddies.window )->window, &mask, NULL, signon_pixmap ), mask );
#if debug > 2
                  GTKFAIM_DEBUG2( "buddylist", "Adding buddy: ", node->buddy->sn, TRUE );
#endif
                  /*
                   * Add a new node to the tree, preserving buddylist order
                   */
                  node->treenode = gtk_tree_item_new_with_label( node->buddy->sn );
                  node->signon_pixmap = GTK_BIN( GTK_TREE_ITEM( node->treenode )->pixmaps_box )->child;
                  gtk_container_remove( GTK_CONTAINER( GTK_TREE_ITEM( node->treenode )->pixmaps_box ), GTK_BIN( GTK_TREE_ITEM( node->treenode )->pixmaps_box )->child );
                  gtk_container_add( GTK_CONTAINER( GTK_TREE_ITEM( node->treenode )->pixmaps_box ), pixmap );
                  gtk_widget_show( pixmap );

                  if( !constructed )
                  {
                     gtk_widget_show( GTK_TREE_ITEM( node->treenode )->pixmaps_box );
                     node->signon_timer = gtk_timeout_add( SIGNON_ICON_DELAY, gtkfaim_signon_hide_pixmap, node );
                  }

                  gtk_signal_connect( GTK_OBJECT( node->treenode ), "button_press_event", GTK_SIGNAL_FUNC( gtkfaim_buddylist_buddy_click ), node->buddy );
                  gtk_tree_insert( GTK_TREE( node->group->tree ), node->treenode, lastonline );
                  gtk_widget_show( node->treenode );
               }

               lastonline++;
            }
            else if( node->treenode )
            {
               /*
                * Remove disconnected buddy's node from tree
                */
               GList *clear_list = g_list_prepend( NULL, node->treenode );
#if debug > 2
               GTKFAIM_DEBUG2( "buddylist", "Removing buddy: ", node->buddy->sn, TRUE );
#endif
               gtk_tree_remove_items( GTK_TREE( group->tree ), clear_list );
               node->treenode = NULL;

               if( node->signon_timer )
               {
                  gtk_timeout_remove( node->signon_timer );
                  node->signon_timer = 0;
               }

               /*
                * Handle the disappearing expand/collapse button
                */
               if( ( GTK_TREE_ITEM( node->group->treenode )->subtree == NULL ) || ( node->group->tree == NULL ) )
               {
                  /* bind the group nodes to the main tree */
                  node->group->tree = gtk_tree_new();
                  gtk_tree_item_set_subtree( GTK_TREE_ITEM( node->group->treenode ), group->tree );
                  gtk_widget_show( node->group->tree );
                  gtk_widget_show( node->group->treenode );
                  gtk_tree_item_expand( GTK_TREE_ITEM( node->group->treenode ) );
               }
            }
         }
      }
   }
#if debug > 2
   GTKFAIM_DEBUG( "buddylist", "refreshonline done.", TRUE );
#endif
}

/* see the FAIM backend code to understand what this does */
char *
gtkfaim_buddylist_create_array(void)
{
   gtkfaim_buddy_t *buddy;
   char            *buddies;
   int              buddies_len = 0;
   int              offset      = 0;
   int              i;

   for( i = 0;
        ( buddy = (gtkfaim_buddy_t *)g_list_nth_data( gtkfaim_buddies, i ) );
        i++ )
      buddies_len += strlen( buddy->sn ) + 1;

   buddies = g_new( char, buddies_len + 1 );

   for( i = 0;
        ( buddy = (gtkfaim_buddy_t *)g_list_nth_data( gtkfaim_buddies, i ) );
        i++ )
   {
      memcpy( buddies + offset, buddy->sn, strlen( buddy->sn ) );
      offset += strlen( buddy->sn );
      buddies[offset++] = '&';
   }

   buddies[buddies_len] = '\0';

   return buddies;
}

int
gtkfaim_parse_oncoming_buddy( struct command_rx_struct *command )
{
   gtkfaim_buddy_t *buddy;
   char            *newname;

   /* since the first byte of the warning level may not always cushion us... */
   newname = (char *) malloc( command->data[10] + 1 );
   memcpy( newname, &command->data[11], command->data[10] );
   newname[command->data[10]] = '\0';

#if debug > 0
   GTKFAIM_DEBUG2("oncomingbuddy", "oncoming buddy: ", newname, TRUE);
#endif

   if( ( buddy = gtkfaim_buddy_find( newname ) ) == NULL )
   {
      /* should never happen */
      GTKFAIM_DEBUG( "oncomingbuddy", "got a signon msg for an unknown buddy", TRUE );
   }
   else
   {
      gboolean isnew    = FALSE;
      gboolean foundone = FALSE;
      int      i        = 11 + command->data[10];
      int      tlvcnt   = 0;
  
      /* warning level */
      buddy->warninglevel = (command->data[i] << 8);
      buddy->warninglevel += (command->data[i+1]);
      i += 2;
      
      tlvcnt = ((command->data[i++]) << 8) & 0xFF00;
      tlvcnt += (command->data[i++]) & 0x00FF;

      /* a mini TLV parser (near verbatim from parse_incoming_im() */
      {
         int curtlv = 0;
         int tlv1 = 0;

         while( curtlv < tlvcnt )
         {
            if( ( command->data[i] == 0x00 ) && ( command->data[i+1] == 0x01 ) )
            {
               if( tlv1 ) break;
               /* t(0001) = class */
               if( command->data[i+3] != 0x02 )
                  printf("faim: userinfo: **warning: strange v(%x) for t(1)\n", command->data[i+3]);
               buddy->class = ((command->data[i+4]) << 8) & 0xFF00;
               buddy->class += (command->data[i+5]) & 0x00FF;
               i += (2 + 2 + command->data[i+3]);
               tlv1++;
            }
            else if( ( command->data[i] == 0x00 ) && ( command->data[i+1] == 0x02 ) )
            {
               /* t(0002) = member since date  */
               if( command->data[i+3] != 0x04 )
                  printf("faim: userinfo: **warning: strange v(%x) for t(2)\n", command->data[i+3]);

               buddy->membersince = ((command->data[i+4]) << 24) &  0xFF000000;
               buddy->membersince += ((command->data[i+5]) << 16) & 0x00FF0000;
               buddy->membersince += ((command->data[i+6]) << 8) &  0x0000FF00;
               buddy->membersince += ((command->data[i+7]) ) &      0x000000FF;
               i += (2 + 2 + command->data[i+3]);
            }
            else if( ( command->data[i] == 0x00 ) && ( command->data[i+1] == 0x03 ) )
            {
               /* t(0003) = on since date  */
               if( command->data[i+3] != 0x04 )
                  printf("faim: userinfo: **warning: strange v(%x) for t(3)\n", command->data[i+3]);		

               buddy->onsince = ((command->data[i+4]) << 24) &  0xFF000000;
               buddy->onsince += ((command->data[i+5]) << 16) & 0x00FF0000;
               buddy->onsince += ((command->data[i+6]) << 8) &  0x0000FF00;
               buddy->onsince += ((command->data[i+7]) ) &      0x000000FF;
               i += (2 + 2 + command->data[i+3]);
            }
            else if( ( command->data[i] == 0x00 ) && ( command->data[i+1] == 0x04 ) )
            {
               /* t(0004) = idle time */
               if( command->data[i+3] != 0x02 )
                  printf("faim: userinfo: **warning: strange v(%x) for t(4)\n", command->data[i+3]);

               buddy->idletime = ((command->data[i+4]) << 8) & 0xFF00;
               buddy->idletime += (command->data[i+5]) & 0x00FF;
               i += (2 + 2 + command->data[i+3]);
            }  
            else
            {
               printf("faim: userinfo: **warning: unexpected TLV t(%02x%02x) l(%02x%02x)\n", command->data[i], command->data[i+1], command->data[i+2], command->data[i+3]);
               i += (2 + 2 + command->data[i+3]);
            }
            curtlv++;
         }
      }		

#if debug > 1
      printf( "faim: blist: sn = \"%s\" (len=%d)\n", buddy->sn, command->data[10] );
      printf( "faim: blist: warnlevel = 0x%04x\n", buddy->warninglevel );
      printf( "faim: blist: tlvcnt = 0x%04x\n", tlvcnt );
      printf( "faim: blist: class = 0x%04x ", buddy->class );
      if( buddy->class == 0x0010 )
         printf( "(FREE)\n" );
      else if( buddy->class == 0x0011 )
         printf( "(TRIAL)\n" );
      else if( buddy->class == 0x0004 )
         printf( "(AOL)\n" );
      else
         printf( "(UNKNOWN)\n" );
      printf( "faim: blist: membersince = %lu\n", buddy->membersince );
      printf( "faim: blist: onlinesince = %lu\n", buddy->onsince );
      printf( "faim: blist: idletime = 0x%04x\n", buddy->idletime );
#endif

      /* this should prevent executing the onjoin event on mere updates */
      if( !buddy->online )
      {
         isnew = TRUE;
         buddy->online = TRUE;
      }
      
      /* this should correct any spacing/capitalization differences
         between the buddy SN the client knows and the one OSCAR
         has registered */
      if( strcmp( newname, buddy->sn ) )
      {
         gtkfaim_group_t *group;
         gint             i;

         if( buddy->sn )
            free( buddy->sn );

         buddy->sn = g_new( char, strlen( newname ) + 1 );
         memcpy( buddy->sn, newname, strlen( newname ) );
         buddy->sn[strlen(newname)] = '\0';

         for( i = 0;
              ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, i ) );
              i++ )
         {
            gtkfaim_buddylist_node_t *node = gtkfaim_group_find_buddy( group, buddy->sn );

            if( node && node->treenode )
               gtk_label_set( GTK_LABEL( GTK_BIN( node->treenode )->child ), buddy->sn );
         }
      }

      if( !foundone && isnew && gtkfaim_config.onjoin && !fork() )
      {
         /* Child */
         execlp( gtkfaim_config.onjoin, 
                 /* FIXME: argv[0] should give pathname. */ 
                 gtkfaim_config.onjoin, buddy->sn, NULL );
         /* if we still exist, there must of been an error */
         GTKFAIM_DEBUG( "event", "error while executing onjoin:", FALSE );
         perror( NULL );
         exit( 1 );
      }
      else
         foundone = TRUE;

      gtkfaim_buddylist_refreshonline();
   }

#if debug > 0
   GTKFAIM_DEBUG( "oncomingbuddy", "done with oncoming processing", TRUE );
#endif
   if( newname )
      free( newname );

   return 0;
}

int
gtkfaim_parse_offgoing_buddy( struct command_rx_struct *command )
{
   gtkfaim_buddy_t *buddy;
   char            *newname;
  
   newname = (char *) malloc( ( ( command->data[10] ) ) + 1 );
   memcpy( newname, &(command->data[11]), command->data[10] );
   newname[command->data[10]] = '\0';

#if debug > 0
   GTKFAIM_DEBUG2( "offgoingbuddy", "buddy leaving: ", newname, TRUE );
#endif

   if( ( buddy = gtkfaim_buddy_find( newname ) ) == NULL )
   {
      /* should never happen */
      GTKFAIM_DEBUG( "offgoingbuddy", "got a signoff for an unknown buddy!", TRUE );
   }
   else
   {
      gboolean foundone = FALSE;

      buddy->online = FALSE;
#if debug > 2
      GTKFAIM_DEBUG( "offgoingbuddy", "online = FALSE", TRUE );
#endif
      if( !foundone && gtkfaim_config.onleave && !fork() )
      {
#if debug > 2
         GTKFAIM_DEBUG( "offgoingbuddy", "executing onleave event", TRUE );
#endif
         /* Child */
         execlp( gtkfaim_config.onleave, 
                 /* FIXME: argv[0] should give pathname. */ 
                 gtkfaim_config.onleave, buddy->sn, NULL );
         /* if we still exist, there must of been an error */
         GTKFAIM_DEBUG( "event", "error while executing onleave:", FALSE );
         perror( NULL );
         exit( 1 );
      }
      else
         foundone = TRUE;

      gtkfaim_buddylist_refreshonline();
   }

#if debug > 0
   GTKFAIM_DEBUG( "offgoingbuddy", "done with offgoing processing", TRUE );
#endif

   if( newname )
      free( newname );

   return 0;
}


/*
 * Dialog Construction Functions
 */
GtkWidget *
gtkfaim_buddylist_manage_create( void )
{
   gtkfaim_group_t *group;
   GtkWidget       *newroot;
   GtkCTreeNode    *parent  = NULL;
   gint             curnode;

   newroot = gtk_ctree_new( 1, 0 );
   gtk_ctree_set_drag_compare_func( GTK_CTREE( newroot ), gtkfaim_buddylist_manage_compare_drag );
   gtk_signal_connect_after( GTK_OBJECT ( newroot ), "tree_move", GTK_SIGNAL_FUNC( gtkfaim_buddylist_manage_after_move ), NULL );

   gtk_clist_freeze( GTK_CLIST( newroot ) );
   gtk_clist_clear( GTK_CLIST( newroot ) );

   for( curnode = 0;
        ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
        curnode++ )
   {
      if( group != gtkfaim_temporary_group )
      {
         gtkfaim_buddylist_node_t  *node;
         gint                       buddynode;
         GtkCTreeNode              *sibling;
         gtkfaim_manage_node_t     *manage_node;
         gchar                     *text[1];

         manage_node = g_new( gtkfaim_manage_node_t, 1 );
         manage_node->type = BUDDYLIST_TYPE_GROUP;
         manage_node->data = group;

         text[0] = (gchar *)group->name;
         parent = gtk_ctree_insert_node( GTK_CTREE( newroot ), NULL, NULL, text, 5, NULL, NULL, NULL, NULL, FALSE, TRUE );
         gtk_ctree_node_set_row_data( GTK_CTREE( newroot ), parent, (gpointer *)manage_node );
         group->ctreenode = parent;

         for( buddynode = 0;
              ( node = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, buddynode ) );
              buddynode++ )
         {
            char *label;
            if( node->buddy->alias )
            {
               label = g_new( char, strlen( node->buddy->sn ) + strlen( node->buddy->alias ) + 4 );
               sprintf( label, "%s (%s)", node->buddy->sn, node->buddy->alias );
            }
            else
               label = node->buddy->sn;

            manage_node = g_new( gtkfaim_manage_node_t, 1 );
            manage_node->type = BUDDYLIST_TYPE_BUDDY;
            manage_node->data = node;

            text[0] = (gchar *)label;
            sibling = gtk_ctree_insert_node( GTK_CTREE( newroot ), parent, NULL, text, 5, NULL, NULL, NULL, NULL, TRUE, FALSE );
            gtk_ctree_node_set_row_data( GTK_CTREE( newroot ), sibling, (gpointer *)manage_node );

            if( node->buddy->alias )
               g_free( label );
         }
      }
   }

   gtk_clist_thaw( GTK_CLIST( newroot ) );
  
   return newroot;
}


GtkWidget *
gtkfaim_window_buddies_manage_create( void )
{
   GtkWidget *iconbox;

   gtkfaim_window_buddies.manage_vbox = gtk_vbox_new( FALSE, 0 );
   gtkfaim_window_buddies.manage_hbox = gtk_hbox_new( FALSE, 2 );
   gtkfaim_window_buddies.manage_tree = gtkfaim_buddylist_manage_create();
#if (GTK_MAJOR_VERSION == 1) && (GTK_MINOR_VERSION == 1) && (GTK_MICRO_VERSION >= 5)
   gtkfaim_window_buddies.manage_scrolled_window = gtk_scrolled_window_new( NULL, NULL );
   gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( gtkfaim_window_buddies.manage_scrolled_window ),
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC );

#else /* GTK Version < 1.1.5 */
   gtk_clist_set_policy( GTK_CLIST( gtkfaim_window_buddies.manage_tree ),
                         GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC );
#endif

   /* Manage page buttons */
   gtkfaim_window_buddies.manage_add_buddy_button = gtk_button_new();
   iconbox = gtk_event_box_new();
   gtk_widget_set_name( GTK_WIDGET( iconbox ), "gtkfaim_addbuddy" );
   if( GTK_WIDGET( iconbox )->style->bg_pixmap[0] )
   {
      int height =(((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->height);
      int width = (((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->width);
      gtk_widget_set_usize( iconbox, width, height );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.manage_add_buddy_button ),
                         iconbox );
      gtk_widget_show( iconbox );
   }
   else
   {
      GtkWidget *label = gtk_label_new( "Add Buddy" );
      gtk_widget_destroy( iconbox );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.manage_add_buddy_button ),
                         label );
      gtk_widget_show( label );
   }
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.manage_add_buddy_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_buddylist_add_buddy_click ),
                       NULL );
   {
      GtkTooltips *tooltips = gtk_tooltips_new();
      gtk_tooltips_set_tip( tooltips, gtkfaim_window_buddies.manage_add_buddy_button,
                            "Add A Buddy To Your Buddy List", NULL );
   }
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.manage_hbox ),
                       gtkfaim_window_buddies.manage_add_buddy_button, FALSE, FALSE, 0 );


   gtkfaim_window_buddies.manage_add_group_button = gtk_button_new();
   iconbox = gtk_event_box_new();
   gtk_widget_set_name( GTK_WIDGET( iconbox ), "gtkfaim_addgroup" );
   if( GTK_WIDGET( iconbox )->style->bg_pixmap[0] )
   {
      int height =(((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->height);
      int width = (((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->width);
      gtk_widget_set_usize( iconbox, width, height );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.manage_add_group_button ),
                         iconbox );
      gtk_widget_show( iconbox );
   }
   else
   {
      GtkWidget *label = gtk_label_new( "Add Group" );
      gtk_widget_destroy( iconbox );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.manage_add_group_button ),
                         label );
      gtk_widget_show( label );
   }
   gtk_widget_show( gtkfaim_window_buddies.manage_add_group_button );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.manage_add_group_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_buddylist_add_group_click ),
                       NULL );
   {
      GtkTooltips *tooltips = gtk_tooltips_new();
      gtk_tooltips_set_tip( tooltips, gtkfaim_window_buddies.manage_add_group_button,
                            "Add A Group To Your Buddy List", NULL );
   }
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.manage_hbox ),
                       gtkfaim_window_buddies.manage_add_group_button, FALSE, FALSE, 0 );


   gtkfaim_window_buddies.manage_remove_button = gtk_button_new();
   iconbox = gtk_event_box_new();
   gtk_widget_set_name( GTK_WIDGET( iconbox ), "gtkfaim_remove" );
   if( GTK_WIDGET( iconbox )->style->bg_pixmap[0] )
   {
      int height =(((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->height);
      int width = (((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->width);
      gtk_widget_set_usize( iconbox, width, height );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.manage_remove_button ),
                         iconbox );
      gtk_widget_show( iconbox );
   }
   else
   {
      GtkWidget *label = gtk_label_new( "Remove" );
      gtk_widget_destroy( iconbox );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.manage_remove_button ),
                         label );
      gtk_widget_show( label );
   }
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.manage_remove_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_buddylist_remove_click ),
                       NULL );
   {
      GtkTooltips *tooltips = gtk_tooltips_new();
      gtk_tooltips_set_tip( tooltips, gtkfaim_window_buddies.manage_remove_button,
                            "Remove Selected Buddy/Group From Your Buddy List",
                            NULL );
   }
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.manage_hbox ),
                       gtkfaim_window_buddies.manage_remove_button, FALSE,
                       FALSE, 0 );

   /* Add components */
   gtk_container_border_width( GTK_CONTAINER( gtkfaim_window_buddies.manage_vbox ), 2 );
#if (GTK_MAJOR_VERSION == 1) && (GTK_MINOR_VERSION == 1) && (GTK_MICRO_VERSION < 5)
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.manage_vbox ),
                       gtkfaim_window_buddies.manage_tree, TRUE, TRUE, 2 );
#else /* GTK Version >= 1.1.5 */
   gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.manage_scrolled_window ),
                      gtkfaim_window_buddies.manage_tree );
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.manage_vbox ),
                       gtkfaim_window_buddies.manage_scrolled_window, TRUE, TRUE, 2 );
#endif
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.manage_vbox ),
                       gtkfaim_window_buddies.manage_hbox, FALSE, FALSE, 0 );

   /* show all the widgets we created */
   gtk_widget_show( gtkfaim_window_buddies.manage_hbox );
   gtk_widget_show( gtkfaim_window_buddies.manage_vbox );
#if (GTK_MAJOR_VERSION == 1) && (GTK_MINOR_VERSION == 1) && (GTK_MICRO_VERSION >= 5)
   gtk_widget_show( gtkfaim_window_buddies.manage_scrolled_window );
#endif /* GTK Version >= 1.1.5 */
   gtk_widget_show( gtkfaim_window_buddies.manage_tree );
   gtk_widget_show( gtkfaim_window_buddies.manage_add_buddy_button );
   gtk_widget_show( gtkfaim_window_buddies.manage_add_group_button );
   gtk_widget_show( gtkfaim_window_buddies.manage_remove_button );

   return gtkfaim_window_buddies.manage_vbox;
}


GtkWidget *
gtkfaim_window_buddies_monitor_create( void )
{
   GtkWidget *iconbox;

   gtkfaim_window_buddies.monitor_vbox = gtk_vbox_new( FALSE, 0 );
   gtkfaim_window_buddies.monitor_hbox = gtk_hbox_new( FALSE, 2 );
   gtkfaim_window_buddies.monitor_tree = gtk_tree_new();
   gtkfaim_window_buddies.monitor_scrolled_window = gtk_scrolled_window_new( NULL, NULL );

   /* this should shut off the connecting lines */
   gtk_tree_set_view_mode( GTK_TREE( gtkfaim_window_buddies.monitor_tree ),
                           GTK_TREE_VIEW_ITEM );
   gtk_tree_set_view_lines( GTK_TREE( gtkfaim_window_buddies.monitor_tree ),
                            0 );
  
   gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( gtkfaim_window_buddies.monitor_scrolled_window ),
                                   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC );
#if (GTK_MAJOR_VERSION == 1) && (GTK_MINOR_VERSION == 1) && (GTK_MICRO_VERSION < 5)
   gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.monitor_scrolled_window ),
                      gtkfaim_window_buddies.monitor_tree );
#else /* GTK Version >= 1.1.5 */
   gtk_scrolled_window_add_with_viewport( GTK_SCROLLED_WINDOW( gtkfaim_window_buddies.monitor_scrolled_window ),
                                          gtkfaim_window_buddies.monitor_tree );
#endif 

   /* Monitor page buttons */
   gtkfaim_window_buddies.monitor_info_button = gtk_button_new();
   iconbox = gtk_event_box_new();
   gtk_widget_set_name( GTK_WIDGET( iconbox ), "gtkfaim_info" );
   if( GTK_WIDGET( iconbox )->style->bg_pixmap[0] )
   {
      int height =(((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->height);
      int width = (((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->width);
      gtk_widget_set_usize( iconbox, width, height );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.monitor_info_button ),
                         iconbox );
      gtk_widget_show( iconbox );
   }
   else
   {
      GtkWidget *label = gtk_label_new( "Info" );
      gtk_widget_destroy( iconbox );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.monitor_info_button ),
                         label );
      gtk_widget_show( label );
   }
   gtk_widget_show( gtkfaim_window_buddies.monitor_info_button );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.monitor_info_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_buddylist_info_click ),
                       NULL );
   {
      GtkTooltips *tooltips = gtk_tooltips_new();
      gtk_tooltips_set_tip( tooltips,
                            gtkfaim_window_buddies.monitor_info_button,
                            "Request Information", NULL );
   }
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.monitor_hbox ),
                       gtkfaim_window_buddies.monitor_info_button, FALSE,
		       FALSE, 0 );

   gtkfaim_window_buddies.monitor_sendim_button = gtk_button_new();
   iconbox = gtk_event_box_new();
   gtk_widget_set_name( GTK_WIDGET( iconbox ), "gtkfaim_sendim" );
   if( GTK_WIDGET( iconbox )->style->bg_pixmap[0] )
   {
      int height =(((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->height);
      int width = (((GdkWindowPrivate *)(GTK_WIDGET( iconbox )->style->bg_pixmap[0]))->width);
      gtk_widget_set_usize( iconbox, width, height );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.monitor_sendim_button ),
                         iconbox );
      gtk_widget_show( iconbox );
   }
   else
   {
      GtkWidget *label = gtk_label_new( "IM" );
      gtk_widget_destroy( iconbox );
      gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.monitor_sendim_button ),
                         label );
      gtk_widget_show( label );
   }
   gtk_widget_show( gtkfaim_window_buddies.monitor_sendim_button );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.monitor_sendim_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( gtkfaim_buddylist_sendim_click ),
                       NULL );
   {
      GtkTooltips *tooltips = gtk_tooltips_new();
      gtk_tooltips_set_tip( tooltips,
                            gtkfaim_window_buddies.monitor_sendim_button,
                            "Send Instant Message", NULL );
   }
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.monitor_hbox ),
                       gtkfaim_window_buddies.monitor_sendim_button, FALSE,
                       FALSE, 0 );

   /* Add components */
   gtk_container_border_width( GTK_CONTAINER( gtkfaim_window_buddies.monitor_vbox ), 2 );
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.monitor_vbox ),
                       gtkfaim_window_buddies.monitor_scrolled_window, TRUE, TRUE, 2 );
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.monitor_vbox ),
                       gtkfaim_window_buddies.monitor_hbox, FALSE, FALSE, 0 );


   /* show all the widgets we created */
   gtk_widget_show( gtkfaim_window_buddies.monitor_hbox );
   gtk_widget_show( gtkfaim_window_buddies.monitor_vbox );
   gtk_widget_show( gtkfaim_window_buddies.monitor_tree );
   gtk_widget_show( gtkfaim_window_buddies.monitor_scrolled_window );
   gtk_widget_show( gtkfaim_window_buddies.monitor_info_button );
   gtk_widget_show( gtkfaim_window_buddies.monitor_sendim_button );

   return gtkfaim_window_buddies.monitor_vbox;
}

int
gtkfaim_window_buddies_create( void )
{
#ifdef GNOME
   gtkfaim_window_buddies.window = gnome_app_new (_("gtkFAIM"), _("gtkFAIM Buddy List") );
   gtk_widget_realize (gtkfaim_window_buddies.window);
#else 
   gtkfaim_window_buddies.window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
#endif

   gtk_widget_set_usize( gtkfaim_window_buddies.window, 170, 180 );

   gtkfaim_window_buddies.vbox = gtk_vbox_new(FALSE, 0);

   gtkfaim_window_buddies.notebook = gtk_notebook_new();
   gtk_notebook_set_tab_pos( GTK_NOTEBOOK( gtkfaim_window_buddies.notebook ), GTK_POS_TOP );
   gtk_notebook_set_scrollable( GTK_NOTEBOOK( gtkfaim_window_buddies.notebook ), TRUE );
   gtk_notebook_set_show_tabs( GTK_NOTEBOOK( gtkfaim_window_buddies.notebook ), TRUE );
   gtk_notebook_set_show_border( GTK_NOTEBOOK( gtkfaim_window_buddies.notebook ), TRUE );
   gtk_widget_show( gtkfaim_window_buddies.notebook );

   gtkfaim_window_buddies.monitor_label = gtk_label_new( "Monitor" );
   gtkfaim_window_buddies.manage_label = gtk_label_new( "Edit List" );
  
#if debug > 2
   GTKFAIM_DEBUG( "buddies_create", "Constructing monitor tab.", TRUE );
#endif
   gtk_notebook_append_page( GTK_NOTEBOOK( gtkfaim_window_buddies.notebook ),
                             gtkfaim_window_buddies_monitor_create(),
                             gtkfaim_window_buddies.monitor_label );
#if debug > 2
   GTKFAIM_DEBUG( "buddies_create", "Constructing manage tab.", TRUE );
#endif
   gtk_notebook_append_page( GTK_NOTEBOOK( gtkfaim_window_buddies.notebook ),
                             gtkfaim_window_buddies_manage_create(),
                             gtkfaim_window_buddies.manage_label );

   gtk_widget_show( gtkfaim_window_buddies.manage_label );
   gtk_widget_show( gtkfaim_window_buddies.monitor_label );

   /* file menu items */
#ifdef GNOME
   gnome_app_create_menus( GNOME_APP( gtkfaim_window_buddies.window ),
                           main_menu );
#else
   gtkfaim_window_buddies.file_menu = gtk_menu_new();
   gtkfaim_window_buddies.file_options_menu = gtk_menu_new();

   gtkfaim_window_buddies.file_item_options = gtk_menu_item_new_with_label( "Options" );

   gtkfaim_window_buddies.file_item_signoff = gtk_menu_item_new_with_label( "Signoff" );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.file_item_signoff ),
                       "activate", GTK_SIGNAL_FUNC( gtkfaim_menu_signoff ),
                       NULL );

   gtkfaim_window_buddies.file_item_exit = gtk_menu_item_new_with_label( "Exit" );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.file_item_exit ),
                       "activate", GTK_SIGNAL_FUNC( gtkfaim_menu_exit ), NULL );

   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.file_menu ),
                    gtkfaim_window_buddies.file_item_options );
   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.file_menu ),
                    gtkfaim_window_buddies.file_item_signoff );
   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.file_menu ),
                    gtkfaim_window_buddies.file_item_exit );

   gtk_widget_show( gtkfaim_window_buddies.file_item_options );
   gtk_widget_show( gtkfaim_window_buddies.file_item_signoff );
   gtk_widget_show( gtkfaim_window_buddies.file_item_exit );

   /* people menu */
   gtkfaim_window_buddies.people_menu = gtk_menu_new();
   gtkfaim_window_buddies.people_item_find = gtk_menu_item_new_with_label( "Find a buddy" );
   gtkfaim_window_buddies.people_item_away = gtk_menu_item_new_with_label( "Away" );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.people_item_away ),
                       "activate", GTK_SIGNAL_FUNC( gtkfaim_away_cb ), NULL );

   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.people_menu ),
                    gtkfaim_window_buddies.people_item_find );
   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.people_menu ),
                    gtkfaim_window_buddies.people_item_away );
   gtk_widget_show( gtkfaim_window_buddies.people_item_find );
   gtk_widget_show( gtkfaim_window_buddies.people_item_away );

   /* main menu bar */
   gtkfaim_window_buddies.menu_bar = gtk_menu_bar_new();
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.vbox ),
                       gtkfaim_window_buddies.menu_bar, FALSE, FALSE, 0 );
   gtk_widget_show( gtkfaim_window_buddies.menu_bar );

   /* file menu item */
   gtkfaim_window_buddies.file_item = gtk_menu_item_new_with_label( "File" );
   gtk_widget_show( gtkfaim_window_buddies.file_item );
  
   gtk_menu_item_set_submenu( GTK_MENU_ITEM( gtkfaim_window_buddies.file_item ),
                              gtkfaim_window_buddies.file_menu );
   gtk_menu_bar_append( GTK_MENU_BAR( gtkfaim_window_buddies.menu_bar ),
                        gtkfaim_window_buddies.file_item );

   gtkfaim_window_buddies.people_item = gtk_menu_item_new_with_label( "People" );
   gtk_widget_show( gtkfaim_window_buddies.people_item );
   gtk_menu_item_set_submenu( GTK_MENU_ITEM( gtkfaim_window_buddies.people_item ),
                              gtkfaim_window_buddies.people_menu );
   gtk_menu_bar_append( GTK_MENU_BAR( gtkfaim_window_buddies.menu_bar ),
                        gtkfaim_window_buddies.people_item );

   /* File/Options submenu */
   gtkfaim_window_buddies.file_options_menu = gtk_menu_new();
   gtk_menu_item_set_submenu( GTK_MENU_ITEM( gtkfaim_window_buddies.file_item_options ),
                              gtkfaim_window_buddies.file_options_menu );

   gtkfaim_window_buddies.file_options_item_preferences = gtk_menu_item_new_with_label( "Edit preferences..." );

   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.file_options_item_preferences ),
                       "activate",
                       GTK_SIGNAL_FUNC( gtkfaim_create_prefs_window ), NULL );

   gtkfaim_window_buddies.file_options_item_chngpwd = gtk_menu_item_new_with_label( "Change password..." );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.file_options_item_chngpwd ),
                       "activate",
                       GTK_SIGNAL_FUNC( gtkfaim_password_change_cb ), NULL );
   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.file_options_menu ),
                    gtkfaim_window_buddies.file_options_item_preferences );
   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.file_options_menu ),
                    gtkfaim_window_buddies.file_options_item_chngpwd );
   gtk_widget_show( gtkfaim_window_buddies.file_options_item_preferences );
   gtk_widget_show( gtkfaim_window_buddies.file_options_item_chngpwd );

   /* People/Find submenu */
   gtkfaim_window_buddies.people_find_menu = gtk_menu_new();
   gtk_menu_item_set_submenu( GTK_MENU_ITEM( gtkfaim_window_buddies.people_item_find ),
                              gtkfaim_window_buddies.people_find_menu );
   gtkfaim_window_buddies.people_find_item_byname = gtk_menu_item_new_with_label( "By Name..." );
  
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.people_find_item_byname ),
                       "activate",
                       GTK_SIGNAL_FUNC( gtkfaim_usersearch_address_cb ),
                       (gpointer)1 );

   gtkfaim_window_buddies.people_find_item_byemail = gtk_menu_item_new_with_label( "By EMail Address..." );
   gtk_signal_connect( GTK_OBJECT( gtkfaim_window_buddies.people_find_item_byemail ),
                       "activate",
                       GTK_SIGNAL_FUNC( gtkfaim_usersearch_address_cb ), 0 );
   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.people_find_menu ),
                    gtkfaim_window_buddies.people_find_item_byname );
   gtk_menu_append( GTK_MENU( gtkfaim_window_buddies.people_find_menu ),
                    gtkfaim_window_buddies.people_find_item_byemail );
   gtk_widget_show( gtkfaim_window_buddies.people_find_item_byname );
   gtk_widget_show( gtkfaim_window_buddies.people_find_item_byemail );
#endif

   gtk_signal_connect( GTK_OBJECT(gtkfaim_window_buddies.window),
                       "delete_event", GTK_SIGNAL_FUNC( gtkfaim_delete_event ),
                       NULL );
   gtk_signal_connect( GTK_OBJECT(gtkfaim_window_buddies.window),
                       "destroy", GTK_SIGNAL_FUNC( gtkfaim_destroy_event ),
                       NULL );

   /* Add notebook */
   gtk_box_pack_start( GTK_BOX( gtkfaim_window_buddies.vbox ),
                       gtkfaim_window_buddies.notebook, TRUE, TRUE, 2 );


#ifdef GNOME
   gnome_app_set_contents( GNOME_APP( gtkfaim_window_buddies.window ),
                           gtkfaim_window_buddies.vbox );
#else
   gtk_container_add( GTK_CONTAINER( gtkfaim_window_buddies.window ),
                      gtkfaim_window_buddies.vbox );
#endif

   gtk_widget_show( gtkfaim_window_buddies.vbox );
   gtk_widget_show( gtkfaim_window_buddies.window );

   return 0;
}

#ifdef GNOME
void 
about_cb( GtkWidget *widget, void *data )
{
   GtkWidget *about;
   const gchar *authors[] =
   {
      "Adam Fritzler",
      "Thomas Muldowney",
      "Stephen Kiernan",
      "James Mastros",
      NULL
   };

   about = gnome_about_new( _("Gnomeified gtkFAIM"), "0.0.1",
                            _("(C) 1998 Thomas Muldowney"),
                            authors,
                            _("Just a test of it all.  "
                              "It almost lets me use AIM too."
                              "  Soon it will all be good."),
                            NULL );

   gtk_widget_show( about );
}
#endif

