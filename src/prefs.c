/*  
Thomas Muldowney (9/27/98) -- 
    New file to hold the preferences code
    Will mainly handle the window routines.
    Let config.c handle most of the actual
    parsing and saving stuff.
*/

#include <aim.h>
#include <gtkfaim.h>

void change_config_color(GtkWidget *widget, int clr_sel);
void gtkfaim_prefs_setup_cancel(GtkWidget *window);
#ifdef GNOME
void gtkfaim_prefs_setup_apply(GtkWidget *widget, int page, gpointer data);
#else
void gtkfaim_prefs_setup_apply(GtkWidget *window, gint Close);
#endif
void gtkfaim_color_sel_cancel_cb(GtkWidget *widget);
#ifdef GNOME
void gtkfaim_color_sel_cb(GnomeColorPicker *cp, gint r, gint g, gint b, gint a, gint clr_sel);
#else
void gtkfaim_color_sel_cb(GtkWidget *widget, gint clr_sel);
#endif

struct gtkfaim_config_struct *config;
struct gtkfaim_prefs_window *gtkfaim_prefswin = NULL;
static GtkWidget *colorseldlg;
typedef enum {
  CLR_NORM_FG=0,
  CLR_NORM_BG,
  CLR_SRC_BG,
  CLR_SRC_FG,
  CLR_DEST_BG,
  CLR_DEST_FG,
} InstantMessageColorType;

static void preview_insert_text(GtkWidget *text, 
                                guint bred, 
                                guint bgreen,
                                guint bblue, 
                                guint fred,
                                guint fgreen,
                                guint fblue,
                                char *msg)
{
  GdkColor fgcolor;
  GdkColor bgcolor;
  GdkColormap *colormap;

  colormap = gtk_widget_get_colormap(text);

  /* since GTK works in 16bit colour, we must shift */
  /*   set default background colour */
  bgcolor.red = bred << 8;
  bgcolor.green = bgreen << 8;
  bgcolor.blue = bblue << 8;
  gdk_color_alloc(colormap, &bgcolor);

  /*   set default forground colour */
  fgcolor.red = fred << 8;
  fgcolor.green =fgreen << 8;
  fgcolor.blue = fblue << 8;
  gdk_color_alloc(colormap, &fgcolor);

  /* freeze it so we can't see every single update */
  gtk_text_freeze(GTK_TEXT(text));

  gtk_text_insert(GTK_TEXT(text), NULL, &fgcolor, &bgcolor, msg, -1); 

  gtk_text_thaw(GTK_TEXT(text));
  gtk_widget_show(text);
}

void gtkfaim_create_prefs_window(void)
{
#ifndef GNOME
  GtkWidget *notebook;
#endif
  GtkWidget *prefswin;
  GtkWidget *book_page;
  GtkWidget *label;
  GtkWidget *button;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *txt_preview;
  GtkWidget *e_sn;
  GtkWidget *e_pass;
  GtkWidget *e_onlogin;
  GtkWidget *e_onleave;
  GtkWidget *e_onjoin;
  GtkWidget *e_onlogoff;
  GtkWidget *e_onoutmsg;
  GtkWidget *e_oninmsg;
  GtkWidget *e_profile;
  GtkWidget *e_gtkrc;
  GtkWidget *chk_autologin;
  GtkWidget *chk_sendonenter;
  GtkWidget *chk_timestamps;
  GtkWidget *frame;

  if (gtkfaim_prefswin != NULL)
  {
    gdk_window_raise( GTK_WINDOW( gtkfaim_prefswin->window ) );
    return;
  } 
  
  config = &gtkfaim_config;

#ifdef GNOME
  prefswin = gnome_property_box_new();
  gtk_window_set_title (GTK_WINDOW(prefswin), ("gtkFAIM Preferences") );
  gtk_signal_connect(GTK_OBJECT(prefswin), "apply", 
                     GTK_SIGNAL_FUNC(gtkfaim_prefs_setup_apply), 0);
  gtk_signal_connect(GTK_OBJECT(GNOME_PROPERTY_BOX(prefswin)->cancel_button), "clicked",
                     GTK_SIGNAL_FUNC(gtkfaim_prefs_setup_cancel), NULL);
#else
  prefswin = gtk_dialog_new();

  gtk_window_set_title (GTK_WINDOW(prefswin), ("gtkFAIM Preferences") );
  gtk_window_position (GTK_WINDOW(prefswin), GTK_WIN_POS_CENTER);
  
  notebook = gtk_notebook_new();
  gtk_notebook_set_tab_pos( GTK_NOTEBOOK( notebook ), GTK_POS_TOP );
  gtk_notebook_set_scrollable( GTK_NOTEBOOK( notebook ), TRUE );
  gtk_notebook_set_show_tabs( GTK_NOTEBOOK( notebook ), TRUE );
  gtk_notebook_set_show_border( GTK_NOTEBOOK( notebook ), TRUE );
  gtk_container_add( GTK_CONTAINER(GTK_DIALOG(prefswin)->vbox), notebook);
  gtk_widget_show(notebook);
#endif

  gtk_signal_connect(GTK_OBJECT(prefswin), "delete_event", 
                     GTK_SIGNAL_FUNC(gtkfaim_prefs_setup_cancel),NULL);
  gtk_signal_connect(GTK_OBJECT(prefswin), "destroy",
                     GTK_SIGNAL_FUNC(gtkfaim_prefs_setup_cancel), NULL);

  /* Work with our user tab */
  label = gtk_label_new	("User");
  book_page = gtk_vbox_new(FALSE,0);
#ifdef GNOME
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(prefswin),book_page,label);
#else
  gtk_notebook_append_page( GTK_NOTEBOOK(notebook ), book_page, label );
#endif
  gtk_widget_show( label );
  gtk_widget_show( book_page );
  
  /* This is our user options. */
  frame = gtk_frame_new ("User Options");
  gtk_box_pack_start( GTK_BOX(book_page), frame, TRUE,TRUE,0);
  gtk_widget_show(frame);
  
  vbox2 = gtk_vbox_new(FALSE,0);
  gtk_container_add( GTK_CONTAINER( frame ), vbox2);
  gtk_widget_show( vbox2 );

  hbox = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start( GTK_BOX(vbox2), hbox, FALSE,FALSE,0);
  gtk_widget_show( hbox );
  
  vbox = gtk_vbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE,FALSE,0);
  gtk_widget_show( vbox );

  label = gtk_label_new("Screenname:");
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );

  label = gtk_label_new("Password:");
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );

  vbox = gtk_vbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE,0);
  gtk_widget_show( vbox );

  e_sn = gtk_entry_new_with_max_length(10);
  gtk_box_pack_start(GTK_BOX(vbox), e_sn, TRUE,TRUE,0);
  if(config->screenname != NULL)
    gtk_entry_set_text( GTK_ENTRY( e_sn ), config->screenname );
  gtk_entry_set_editable(GTK_ENTRY(e_sn), FALSE);
  gtk_widget_show( e_sn );
  
  e_pass = gtk_entry_new_with_max_length(10);
  gtk_box_pack_start(GTK_BOX(vbox), e_pass, TRUE,TRUE,0);
  if (config->password != NULL )
    gtk_entry_set_text( GTK_ENTRY ( e_pass ), config->password );
  gtk_entry_set_visibility(GTK_ENTRY(e_pass), FALSE);
  gtk_entry_set_editable(GTK_ENTRY(e_pass), FALSE);
  gtk_widget_show( e_sn );
  gtk_widget_show( e_pass );

  chk_autologin = gtk_check_button_new_with_label("Auto login");
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(chk_autologin), config->autologin);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(chk_autologin), "toggled", 
             GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_box_pack_start(GTK_BOX(vbox2), chk_autologin, TRUE, TRUE,0);
  gtk_widget_show( chk_autologin );

  chk_sendonenter = gtk_check_button_new_with_label("Send IM's when enter is pressed");
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(chk_sendonenter), config->sendonenter);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(chk_sendonenter),"toggled", 
             GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_box_pack_start(GTK_BOX(vbox2), chk_sendonenter, TRUE,TRUE,0);
  gtk_widget_show( chk_sendonenter );

  chk_timestamps = gtk_check_button_new_with_label("Mark IM's with a timestamp.");
  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(chk_timestamps), config->timestamps);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(chk_timestamps), "toggled", 
           GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_box_pack_start(GTK_BOX(vbox2), chk_timestamps, TRUE,TRUE,0);
  gtk_widget_show( chk_timestamps );

  frame = gtk_frame_new("Buddies");
  gtk_box_pack_start( GTK_BOX(book_page), frame, TRUE,TRUE,0);
  gtk_widget_show(frame);

  hbox = gtk_hbox_new(FALSE,0);
  gtk_container_add( GTK_CONTAINER( frame ), hbox);
  gtk_widget_show( hbox );

  button = gtk_button_new_with_label( "Edit List" );
  gtk_box_pack_start( GTK_BOX( hbox ), button,TRUE,FALSE,0);
  gtk_widget_show( button );

  /* Deals with IM colors and other defaults */
  label = gtk_label_new	("Colors");
  book_page = gtk_vbox_new(FALSE,0);
#ifdef GNOME
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(prefswin), book_page, label);
#else
  gtk_notebook_append_page( GTK_NOTEBOOK(notebook ), book_page, label );
#endif
  gtk_widget_show( book_page );
  gtk_widget_show( label );
  
  frame = gtk_frame_new("Instant Message Colors");
  gtk_box_pack_start(GTK_BOX(book_page), frame, TRUE,TRUE,0);
  gtk_widget_show( frame );

  hbox = gtk_hbox_new(FALSE, 2);
  gtk_container_add(GTK_CONTAINER(frame), hbox);
  gtk_widget_show(hbox);
  
  txt_preview = gtk_text_new(NULL, NULL);
  gtk_box_pack_start(GTK_BOX(hbox),txt_preview, TRUE,TRUE,0);
  gtk_widget_show(txt_preview);

  preview_insert_text(GTK_WIDGET(txt_preview), config->imsg_bgcolor.red,
       config->imsg_bgcolor.green, config->imsg_bgcolor.blue,
       config->imsg_fgcolor.red, config->imsg_fgcolor.green,
       config->imsg_fgcolor.blue, "Normal Text\n\n");

  preview_insert_text(GTK_WIDGET(txt_preview), config->imsg_src_bgcolor.red,
       config->imsg_src_bgcolor.green, config->imsg_src_bgcolor.blue,
       config->imsg_src_fgcolor.red, config->imsg_src_fgcolor.green,
       config->imsg_src_fgcolor.blue, "Source Screenname\n\n");

  preview_insert_text(GTK_WIDGET(txt_preview), config->imsg_dest_bgcolor.red,
       config->imsg_dest_bgcolor.green, config->imsg_dest_bgcolor.blue,
       config->imsg_dest_fgcolor.red, config->imsg_dest_fgcolor.green,
       config->imsg_dest_fgcolor.blue, "Destination Screename\n\n");

  vbox = gtk_vbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE,0);
  gtk_widget_show(vbox);

#ifdef GNOME
  hbox = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox,TRUE,FALSE,0);
  gtk_widget_show(hbox);
  button = gnome_color_picker_new();
  gtk_box_pack_start(GTK_BOX(hbox), button,FALSE, FALSE, 0);
  gnome_color_picker_set_title(GNOME_COLOR_PICKER(button), "Normal foreground color");
  gnome_color_picker_set_i8(GNOME_COLOR_PICKER(button), config->imsg_fgcolor.red, config->imsg_fgcolor.green, config->imsg_fgcolor.blue,0);
  gtk_signal_connect(GTK_OBJECT(button), "color_set", GTK_SIGNAL_FUNC(gtkfaim_color_sel_cb), (int)CLR_NORM_FG);
  gtk_widget_show(button);
  label = gtk_label_new("Normal foreground color");
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE,FALSE,10);
  gtk_widget_show(label);

  hbox = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox,TRUE,FALSE,0);
  gtk_widget_show(hbox);
  button = gnome_color_picker_new();
  gtk_box_pack_start(GTK_BOX(hbox), button,FALSE,FALSE,0);
  gnome_color_picker_set_title(GNOME_COLOR_PICKER(button), "Normal background color");
  gnome_color_picker_set_i8(GNOME_COLOR_PICKER(button), config->imsg_bgcolor.red, config->imsg_bgcolor.green, config->imsg_bgcolor.blue,0);
  gtk_signal_connect(GTK_OBJECT(button), "color_set", GTK_SIGNAL_FUNC(gtkfaim_color_sel_cb), (int)CLR_NORM_BG);
  gtk_widget_show(button);
  label = gtk_label_new("Normal background color");
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE,FALSE,10);
  gtk_widget_show(label);

  button = gnome_color_picker_new();
  hbox = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox,TRUE,FALSE,0);
  gtk_widget_show(hbox);
  gtk_box_pack_start(GTK_BOX(hbox), button,FALSE,FALSE,0);
  gnome_color_picker_set_title(GNOME_COLOR_PICKER(button), "Normal source foreground color");
  gnome_color_picker_set_i8(GNOME_COLOR_PICKER(button), config->imsg_src_fgcolor.red, config->imsg_src_fgcolor.green, config->imsg_src_fgcolor.blue,0);
  gtk_signal_connect(GTK_OBJECT(button), "color_set", GTK_SIGNAL_FUNC(gtkfaim_color_sel_cb), (int)CLR_SRC_FG);
  gtk_widget_show(button);
  label = gtk_label_new("Normal source foreground color");
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE,FALSE,10);
  gtk_widget_show(label);

  hbox = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox,TRUE,FALSE,0);
  gtk_widget_show(hbox);
  button = gnome_color_picker_new();
  gtk_box_pack_start(GTK_BOX(hbox), button,FALSE,FALSE,0);
  gnome_color_picker_set_title(GNOME_COLOR_PICKER(button), "Normal source background color");
  gnome_color_picker_set_i8(GNOME_COLOR_PICKER(button), config->imsg_src_bgcolor.red, config->imsg_src_bgcolor.green, config->imsg_src_bgcolor.blue,0);
  gtk_signal_connect(GTK_OBJECT(button), "color_set", GTK_SIGNAL_FUNC(gtkfaim_color_sel_cb), (int)CLR_SRC_BG);
  gtk_widget_show(button);
  label = gtk_label_new("Normal source background color");
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE,FALSE,10);
  gtk_widget_show(label);

  hbox = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox,TRUE,FALSE,0);
  gtk_widget_show(hbox);
  button = gnome_color_picker_new();
  gtk_box_pack_start(GTK_BOX(hbox), button,FALSE,FALSE,0);
  gnome_color_picker_set_title(GNOME_COLOR_PICKER(button), "Normal destination foreground color");
  gnome_color_picker_set_i8(GNOME_COLOR_PICKER(button), config->imsg_dest_fgcolor.red, config->imsg_dest_fgcolor.green, config->imsg_dest_fgcolor.blue,0);
  gtk_signal_connect(GTK_OBJECT(button), "color_set", GTK_SIGNAL_FUNC(gtkfaim_color_sel_cb), (int)CLR_DEST_FG);
  gtk_widget_show(button);
  label = gtk_label_new("Normal destination foreground color");
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE,FALSE,10);
  gtk_widget_show(label);

  hbox = gtk_hbox_new(FALSE,0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox,TRUE,FALSE,0);
  gtk_widget_show(hbox);
  button = gnome_color_picker_new();
  gtk_box_pack_start(GTK_BOX(hbox), button,FALSE,FALSE,0);
  gnome_color_picker_set_title(GNOME_COLOR_PICKER(button), "Normal destination background color");
  gnome_color_picker_set_i8(GNOME_COLOR_PICKER(button), config->imsg_dest_bgcolor.red, config->imsg_dest_bgcolor.green, config->imsg_dest_bgcolor.blue,0);
  gtk_signal_connect(GTK_OBJECT(button), "color_set", GTK_SIGNAL_FUNC(gtkfaim_color_sel_cb), (int)CLR_DEST_BG);
  gtk_widget_show(button);
  label = gtk_label_new("Normal destination background color");
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE,FALSE,10);
  gtk_widget_show(label);
#else
  
  button=gtk_button_new_with_label("Change normal foreground color");
  gtk_signal_connect(GTK_OBJECT(button),"clicked", 
        GTK_SIGNAL_FUNC(change_config_color), (gpointer)CLR_NORM_FG);
  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, TRUE,0);
  gtk_widget_show(button);

  button=gtk_button_new_with_label("Change normal background color");
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
         GTK_SIGNAL_FUNC(change_config_color), (gpointer)CLR_NORM_BG);
  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, TRUE,0);
  gtk_widget_show(button);
 
  button=gtk_button_new_with_label("Change normal source foregroud color");
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
         GTK_SIGNAL_FUNC(change_config_color), (gpointer)CLR_SRC_FG);
  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, TRUE,0);
  gtk_widget_show(button);

  button=gtk_button_new_with_label("Change normal source background color");
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
         GTK_SIGNAL_FUNC(change_config_color), (gpointer)CLR_SRC_BG);
  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, TRUE,0);
  gtk_widget_show(button);

  button=gtk_button_new_with_label("Change normal destination foreground color");
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
         GTK_SIGNAL_FUNC(change_config_color), (gpointer)CLR_DEST_FG);
  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, TRUE,0);
  gtk_widget_show(button);

  button=gtk_button_new_with_label("Change normal destination background color");
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
         GTK_SIGNAL_FUNC(change_config_color), (gpointer)CLR_DEST_BG);
  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, TRUE,0);
  gtk_widget_show(button);
#endif
  /* Deals with programs run at different points */
  label = gtk_label_new	("Programs");
  book_page = gtk_vbox_new(FALSE,0);
#ifdef GNOME
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(prefswin), book_page, label);
#else
  gtk_notebook_append_page( GTK_NOTEBOOK(notebook ), book_page, label );
#endif
  gtk_widget_show( book_page );
  label = gtk_label_new	("Programs");
  gtk_widget_show( label );

  frame = gtk_frame_new ("Paths");
  gtk_box_pack_start( GTK_BOX(book_page), frame, TRUE,TRUE,0);
  gtk_widget_show( frame );

  hbox = gtk_hbox_new(FALSE, 2);
  gtk_container_add( GTK_CONTAINER( frame ), hbox);
  gtk_widget_show( hbox );

  vbox = gtk_vbox_new(FALSE,2);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);
  gtk_widget_show( vbox );

  label = gtk_label_new("On Login:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );

  label = gtk_label_new("On Logoff:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );
  
  label = gtk_label_new("On Join:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );

  label = gtk_label_new("On Leave:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );

  label = gtk_label_new("On Outgoing Msg:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );
  
  label = gtk_label_new("On Incoming Msg:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );
  
  label = gtk_label_new("GTKrc:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE,TRUE,0);
  gtk_widget_show( label );

  label = gtk_label_new("Profile:");
  gtk_box_pack_start( GTK_BOX(vbox), label, TRUE, TRUE,0);
  gtk_widget_show( label );

  vbox = gtk_vbox_new (FALSE,2);
  gtk_box_pack_start(GTK_BOX( hbox ), vbox, TRUE, TRUE,0);
  gtk_widget_show(vbox);

  e_onlogin = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_onlogin, TRUE, TRUE, 0);
  if( config->onlogin != NULL )
    gtk_entry_set_text(GTK_ENTRY(e_onlogin), config->onlogin);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_onlogin), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show( e_onlogin );

  e_onlogoff = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_onlogoff, TRUE, TRUE, 0);
  if( config->onlogoff != NULL )  
    gtk_entry_set_text(GTK_ENTRY(e_onlogoff), config->onlogoff);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_onlogoff), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show(e_onlogoff);

  e_onjoin = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_onjoin, TRUE, TRUE, 0);
  if(config->onjoin != NULL)  
    gtk_entry_set_text(GTK_ENTRY(e_onjoin), config->onjoin);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_onjoin), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show( e_onjoin );

  e_onleave = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_onleave, TRUE, TRUE, 0);
  if(config->onleave != NULL)    
    gtk_entry_set_text(GTK_ENTRY(e_onleave), config->onleave);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_onleave), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show( e_onleave );

  e_onoutmsg = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_onoutmsg, TRUE, TRUE, 0);
  if(config->onoutmessage != NULL)
    gtk_entry_set_text( GTK_ENTRY(e_onoutmsg), config->onoutmessage);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_onoutmsg), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show( e_onoutmsg );

  e_oninmsg = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_oninmsg, TRUE, TRUE, 0);
  if(config->oninmessage != NULL)
    gtk_entry_set_text( GTK_ENTRY(e_oninmsg), config->oninmessage);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_oninmsg), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show( e_oninmsg );

  e_gtkrc = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_gtkrc, TRUE,TRUE,0);
  if(config->gtkrc != NULL)
    gtk_entry_set_text( GTK_ENTRY(e_gtkrc), config->gtkrc);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_gtkrc), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show( e_gtkrc );

  e_profile = gtk_entry_new();
  gtk_box_pack_start( GTK_BOX(vbox), e_profile, TRUE, TRUE,0);
  if(config->profile_string != NULL)
    gtk_entry_set_text( GTK_ENTRY(e_profile), config->profile_string);
#ifdef GNOME
  gtk_signal_connect_object(GTK_OBJECT(e_profile), "changed",
         GTK_SIGNAL_FUNC(gnome_property_box_changed), GTK_OBJECT(prefswin));
#endif
  gtk_widget_show( e_profile );

  /* Here are our buttons */
#ifndef GNOME
  button = gtk_button_new_with_label (("Ok"));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG(prefswin)->action_area), button, TRUE, TRUE, 10);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
               GTK_SIGNAL_FUNC (gtkfaim_prefs_setup_apply), (gpointer)1 );
  gtk_widget_show (button);

  button = gtk_button_new_with_label (("Cancel"));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG(prefswin)->action_area), button, TRUE, TRUE, 10);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
               GTK_SIGNAL_FUNC (gtkfaim_prefs_setup_cancel),
               GTK_OBJECT (prefswin) );
  gtk_widget_show (button);

  button = gtk_button_new_with_label (("Apply"));
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG(prefswin)->action_area), button, TRUE, TRUE, 10);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
               GTK_SIGNAL_FUNC (gtkfaim_prefs_setup_apply), 0 );
  gtk_widget_show (button);
#endif

  gtkfaim_prefswin = g_new(struct gtkfaim_prefs_window,1);

  gtkfaim_prefswin->window = prefswin;
  gtkfaim_prefswin->txt_preview = txt_preview;
  gtkfaim_prefswin->e_onlogin = e_onlogin;
  gtkfaim_prefswin->e_onleave = e_onleave;
  gtkfaim_prefswin->e_onjoin = e_onjoin;
  gtkfaim_prefswin->e_onlogoff = e_onlogoff;
  gtkfaim_prefswin->e_onoutmsg = e_onoutmsg;
  gtkfaim_prefswin->e_oninmsg = e_oninmsg;
  gtkfaim_prefswin->e_profile = e_profile;
  gtkfaim_prefswin->e_gtkrc = e_gtkrc;
  gtkfaim_prefswin->chk_autologin = chk_autologin;
  gtkfaim_prefswin->chk_sendonenter = chk_sendonenter;
  gtkfaim_prefswin->chk_timestamps = chk_timestamps;

  gtk_widget_show_all(prefswin);
}

void gtkfaim_prefs_setup_cancel(GtkWidget *window)
{
  if(gtkfaim_prefswin != NULL)
  {
    gtk_widget_hide(gtkfaim_prefswin->window);
    gtk_widget_destroy(GTK_WIDGET(gtkfaim_prefswin->window));
  }
  gtkfaim_read_config();
  g_free(gtkfaim_prefswin);
  gtkfaim_prefswin = NULL;
}

#ifdef GNOME
void gtkfaim_prefs_setup_apply(GtkWidget *widget, int page, gpointer data)
#else
void gtkfaim_prefs_setup_apply(GtkWidget *window, gint Close)
#endif
{
  /* TODO: Call the function to write the settings when it is made */
#ifndef GNOME
  if (Close > 0)
  {
    gtk_widget_destroy(gtkfaim_prefswin->window);
    g_free(gtkfaim_prefswin);
    gtkfaim_prefswin = NULL;
  }
#endif
}

void change_config_color(GtkWidget *widget, int clr_sel)
{
  gdouble colors[3];

  colorseldlg = gtk_color_selection_dialog_new("Choose New Color");
  gtk_signal_connect(GTK_OBJECT(colorseldlg), "delete_event",
                     GTK_SIGNAL_FUNC(gtk_widget_destroy), NULL);
  gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(colorseldlg)->ok_button), "clicked",
                     GTK_SIGNAL_FUNC(gtkfaim_color_sel_cb), (gpointer)clr_sel);
  gtk_signal_connect(GTK_OBJECT(GTK_COLOR_SELECTION_DIALOG(colorseldlg)->cancel_button), "clicked", GTK_SIGNAL_FUNC(gtkfaim_color_sel_cancel_cb), NULL);
  switch(clr_sel)
  {
  case CLR_NORM_BG:
    colors[0] = config->imsg_bgcolor.red/255.0;
    colors[1] = config->imsg_bgcolor.green/255.0;
    colors[2] = config->imsg_bgcolor.blue/255.0; 
    break;
  case CLR_NORM_FG: 
    colors[0] = config->imsg_fgcolor.red / 255.0;
    colors[1] = config->imsg_fgcolor.green / 255.0;
    colors[2] = config->imsg_fgcolor.blue / 255.0;
    break;
  case CLR_SRC_BG:
    colors[0] = config->imsg_src_bgcolor.red/255.0;
    colors[1] = config->imsg_src_bgcolor.green/255.0;
    colors[2] = config->imsg_src_bgcolor.blue/255.0;
    break;
  case CLR_SRC_FG:
    colors[0] = config->imsg_src_fgcolor.red/255.0;
    colors[1] = config->imsg_src_fgcolor.green/255.0;
    colors[2] = config->imsg_src_fgcolor.blue/255.0;
  case CLR_DEST_FG:
    colors[0] = config->imsg_dest_fgcolor.red/255.0;
    colors[1] = config->imsg_dest_fgcolor.green/255.0;
    colors[2] = config->imsg_dest_fgcolor.blue/255.0;
    break;
  case CLR_DEST_BG:
    colors[0] = config->imsg_dest_bgcolor.red/255.0;
    colors[1] = config->imsg_dest_bgcolor.green/255.0;
    colors[2] = config->imsg_dest_bgcolor.blue/255.0; 
    break;
  } 

  gtk_color_selection_set_color(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(colorseldlg)->colorsel), colors);
  gtk_color_selection_set_color(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(colorseldlg)->colorsel), colors);

  gtk_widget_show(colorseldlg);

}

void gtkfaim_color_sel_cancel_cb(GtkWidget *widget)
{
  gtk_widget_destroy(colorseldlg);
}

#ifdef GNOME
void gtkfaim_color_sel_cb(GnomeColorPicker *cp, gint r, gint g, gint b, gint a, gint clr_sel)
#else
void gtkfaim_color_sel_cb(GtkWidget *widget, gint clr_sel)
#endif
{
  gdouble color[4];
#ifdef GNOME
  gnome_color_picker_get_d(GNOME_COLOR_PICKER(cp), &color[0], &color[1], &color[2],&color[3]);
#else
  gtk_color_selection_get_color(GTK_COLOR_SELECTION(GTK_COLOR_SELECTION_DIALOG(colorseldlg)->colorsel),color);
#endif

  switch(clr_sel)
  {
  case CLR_NORM_FG:
    config->imsg_fgcolor.red = color[0] * 255.0 + 0.5;
    config->imsg_fgcolor.green = color[1] * 255.0 + 0.5;
    config->imsg_fgcolor.blue = color[2] * 255.0 + 0.5;
    break;
  case CLR_NORM_BG:
    config->imsg_bgcolor.red = color[0] * 255.0 + 0.5;
    config->imsg_bgcolor.green = color[1] * 255.0 + 0.5;
    config->imsg_bgcolor.blue = color[2] * 255.0 + 0.5;
    break;
  case CLR_SRC_FG:
    config->imsg_src_fgcolor.red = color[0] * 255.0 + 0.5;
    config->imsg_src_fgcolor.green = color[1] * 255.0 + 0.5;
    config->imsg_src_fgcolor.blue = color[2] * 255.0 + 0.5;
    break;
  case CLR_SRC_BG:
    config->imsg_src_bgcolor.red = color[0] * 255.0 + 0.5;
    config->imsg_src_bgcolor.green = color[1] * 255.0 + 0.5;
    config->imsg_src_bgcolor.blue = color[2] * 255.0 + 0.5;
    break;
  case CLR_DEST_FG:
    config->imsg_dest_fgcolor.red = color[0] * 255.0 + 0.5;
    config->imsg_dest_fgcolor.green = color[1] * 255.0 + 0.5;
    config->imsg_dest_fgcolor.blue = color[2] * 255.0 + 0.5;
    break;
  case CLR_DEST_BG:
    config->imsg_dest_bgcolor.red = color[0] * 255.0 + 0.5;
    config->imsg_dest_bgcolor.green = color[1] * 255.0 + 0.5;
    config->imsg_dest_bgcolor.blue = color[2] * 255.0 + 0.5;
    break;
  } 
  gtkfaim_clear_textbox(gtkfaim_prefswin->txt_preview);
  preview_insert_text(GTK_WIDGET(gtkfaim_prefswin->txt_preview), config->imsg_bgcolor.red,
       config->imsg_bgcolor.green, config->imsg_bgcolor.blue,
       config->imsg_fgcolor.red, config->imsg_fgcolor.green,
       config->imsg_fgcolor.blue, "Normal Text\n\n");

  preview_insert_text(GTK_WIDGET(gtkfaim_prefswin->txt_preview), config->imsg_src_bgcolor.red,
       config->imsg_src_bgcolor.green, config->imsg_src_bgcolor.blue,
       config->imsg_src_fgcolor.red, config->imsg_src_fgcolor.green,
       config->imsg_src_fgcolor.blue, "Source Screenname\n\n");

  preview_insert_text(GTK_WIDGET(gtkfaim_prefswin->txt_preview), config->imsg_dest_bgcolor.red,
       config->imsg_dest_bgcolor.green, config->imsg_dest_bgcolor.blue,
       config->imsg_dest_fgcolor.red, config->imsg_dest_fgcolor.green,
       config->imsg_dest_fgcolor.blue, "Destination Screename\n\n");

#ifndef GNOME
  gtk_widget_destroy(colorseldlg);
#endif

}
