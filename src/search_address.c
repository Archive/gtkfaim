
#include <aim.h>
#include <gtkfaim.h>

gchar *gtkfaim_search_curname = NULL;

struct gtkfaim_search_selcb_struct {
  GtkWidget *window;
  GtkWidget *clist;
};

struct gtkfaim_search_cb_struct {
  GtkWidget *window;
  GtkWidget *widget;
};

int gtkfaim_usersearch_address(char *address)
{
  if (address)
    aim_usersearch_address(address);
  return 0;
}

void gtkfaim_search_selcb_close(GtkWidget *widget, gpointer data)
{
  if (data)
    {
      gtk_widget_hide(GTK_WIDGET(((struct gtkfaim_search_selcb_struct *)data)->window));
      gtk_widget_destroy(GTK_WIDGET(((struct gtkfaim_search_selcb_struct *) data)->window));
      free(data);
    }
  return;
}

void gtkfaim_search_selcb(GtkWidget *widget, gpointer data)
{
  if (gtkfaim_search_curname)
    {
      gtkfaim_add_imwindow(gtkfaim_search_curname);
      gtkfaim_search_selcb_close(widget, data);
    }

  return;
}

void gtkfaim_search_selcb_row(GtkWidget *widget, gint row, gint column, GdkEventButton *event, gpointer data)
{
  gtk_clist_get_text(GTK_CLIST(widget), row, column, &gtkfaim_search_curname);
  return;
}

#ifdef GNOME
void gtkfaim_usersearch_dosearch_address(gchar *string, gpointer data)
#else
void gtkfaim_usersearch_dosearch_address(GtkWidget *widget, gpointer data)
#endif
{
  gchar *text;

#ifdef GNOME
  text =  string;
  if(text == NULL)
    return;
#else
  if (!data)
    return;
  text = gtk_entry_get_text(GTK_ENTRY(((struct gtkfaim_search_cb_struct *)data)->widget));
#endif
  if(strlen(text)>1)
    {
      gtkfaim_usersearch_address(text);
#ifndef GNOME
      gtkfaim_genericok_cb(NULL, ((struct gtkfaim_search_cb_struct *)data)->window);
      free(data);
#endif
    }

  return;
}

#ifdef GNOME
void gtkfaim_usersearch_dosearch_name(gchar *string, gpointer data)
#else
void gtkfaim_usersearch_dosearch_name(GtkWidget *widget, gpointer data)
#endif
{
  gchar *text;

#ifndef GNOME
  if (!data)
    return;

  text = gtk_entry_get_text(GTK_ENTRY(((struct gtkfaim_search_cb_struct *)data)->widget));
#else
  text = string;
  if(text == NULL)
    return;
#endif

  if((strlen(text)>1))
    {
#if 0
      gtkfaim_usersearch_name(text);
#else
      gtkfaim_genericok("User search by name not yet implmeneted.");
#endif
#ifndef GNOME
      gtkfaim_genericok_cb(NULL, ((struct gtkfaim_search_cb_struct *)data)->window);
      free(data);
#endif
    }

  return;
}

void gtkfaim_usersearch_address_cb(GtkWidget *widget, gpointer data)
{
  GtkWidget *win;
#ifndef GNOME
  GtkWidget *vbox;
  GtkWidget *hboxbtns;
  GtkWidget *close;
  GtkWidget *dosearch;
  GtkWidget *entry;
  struct gtkfaim_search_cb_struct *cbst;

  cbst = (struct gtkfaim_search_cb_struct *)malloc(sizeof(struct gtkfaim_search_cb_struct));

  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  
  gtk_window_set_title(GTK_WINDOW(win), (int)data ? "gtkFAIM: User Search by Name" : "gtkFAIM: User Search by Address");
  
  gtk_window_position(GTK_WINDOW(win), GTK_WIN_POS_CENTER); 
  gtk_widget_set_usize(GTK_WIDGET(win), 270, 0);

  cbst->window = win;

  entry = gtk_entry_new();
  cbst->widget = entry;

  vbox = gtk_vbox_new(FALSE, 4);
  hboxbtns = gtk_hbox_new(TRUE, 4);
  close = gtk_button_new_with_label("Cancel");
  gtk_signal_connect(GTK_OBJECT(close), "clicked", GTK_SIGNAL_FUNC(gtkfaim_genericok_cb), win);
  dosearch = gtk_button_new_with_label("Search");
  if ((int)data==0)
    gtk_signal_connect(GTK_OBJECT(dosearch), "clicked", GTK_SIGNAL_FUNC(gtkfaim_usersearch_dosearch_address), cbst);
  else
    gtk_signal_connect(GTK_OBJECT(dosearch), "clicked", GTK_SIGNAL_FUNC(gtkfaim_usersearch_dosearch_name), cbst);

  gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(vbox), hboxbtns, FALSE, FALSE, 2);
 
  gtk_box_pack_start(GTK_BOX(hboxbtns), dosearch, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hboxbtns), close, TRUE, TRUE, 0);

  gtk_container_add(GTK_CONTAINER(win), vbox);

  gtk_widget_show(vbox);
  gtk_widget_show(hboxbtns);
  gtk_widget_show(entry);
  gtk_widget_show(dosearch);
  gtk_widget_show(close);
#else
  win = gnome_request_string_dialog((int)data ? "Screenname to search for" : "E-Mail address to search for", (int)data ? GTK_SIGNAL_FUNC(gtkfaim_usersearch_dosearch_name) : GTK_SIGNAL_FUNC(gtkfaim_usersearch_dosearch_address), NULL);
#endif
  gtk_widget_show(win);

#if 0
  gtkfaim_genericok("User search by address not yet implemented");
#endif
#if 0
  gtkfaim_usersearch_address("afritz@delphid.ml.org");
#endif



  return;
}

int gtkfaim_parse_search_address(struct command_rx_struct *command)
{
  char *origname = NULL;
  int i = 0;
  int j = 0;
  struct aim_tlv_t *names[5];
  char *names2[1];

  GtkWidget *us_win;
  GtkWidget *us_vbox1;
  GtkWidget *us_hboxbtns;
  GtkWidget *us_clist;
  GtkWidget *us_close;
  GtkWidget *us_openim;
  
  char title[] = "gtkFAIM: User Search Results";
  
  char ctitle[50] = "Screen Names for \'";
  
  struct gtkfaim_search_selcb_struct *selcbstruct;

  selcbstruct = (struct gtkfaim_search_selcb_struct *)malloc(sizeof(struct gtkfaim_search_selcb_struct));
  memset(selcbstruct, 0, sizeof(struct gtkfaim_search_selcb_struct));
  
  
  {
    u_long snacid = 0x000000000;
    struct aim_snac_t *snac = NULL;

    snacid = (command->data[6] << 24) & 0xFF000000;
    snacid+= (command->data[7] << 16) & 0x00FF0000;
    snacid+= (command->data[8] <<  8) & 0x0000FF00;
    snacid+= (command->data[9])       & 0x000000FF;
#if debug > 1
    GTKFAIM_DEBUG("search_address", "searching for SNAC...", TRUE);
#endif
    snac = aim_remsnac(snacid);
    if (snac)
      {
#if debug > 1
	GTKFAIM_DEBUG("search_address", "SNAC FOUND", TRUE);
	printf("search_address: reply to snacid 0x%08lx for sn %s\n", snacid, (char *)snac->data);
#endif
	origname = snac->data;
	free(snac);
      }
    else
      {
#if debug > 1
	GTKFAIM_DEBUG("search_address", "SNAC NOT FOUND", TRUE);
#endif
	return -1;
      }
  }

  strcat(ctitle, origname);
  strcat(ctitle, "\'");
  
  us_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(us_win), title);
  gtk_window_position(GTK_WINDOW(us_win), GTK_WIN_POS_CENTER);
  
  us_vbox1 = gtk_vbox_new(FALSE, 4);
  us_hboxbtns = gtk_hbox_new(TRUE, 4);
  us_clist = gtk_clist_new(1);
  gtk_clist_set_column_title(GTK_CLIST(us_clist), 0, ctitle);
  gtk_clist_column_titles_show(GTK_CLIST(us_clist));
  gtk_widget_set_usize(us_clist, 300, 125);

  /* we pass these on to the callbacks for ident */
  selcbstruct->clist = us_clist;
  selcbstruct->window = us_win;

  us_close = gtk_button_new_with_label("Close");
  gtk_signal_connect(GTK_OBJECT(us_close), "clicked", GTK_SIGNAL_FUNC(gtkfaim_search_selcb_close), selcbstruct);
  us_openim = gtk_button_new_with_label("Open IM Window");
  gtk_signal_connect(GTK_OBJECT(us_openim), "clicked", GTK_SIGNAL_FUNC(gtkfaim_search_selcb), selcbstruct);

  gtk_signal_connect(GTK_OBJECT(us_clist), "select_row", GTK_SIGNAL_FUNC(gtkfaim_search_selcb_row), NULL);
  
  gtk_box_pack_start(GTK_BOX(us_vbox1), us_clist, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(us_vbox1), us_hboxbtns, FALSE, FALSE, 2);
  
  gtk_box_pack_start(GTK_BOX(us_hboxbtns), us_openim, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(us_hboxbtns), us_close, TRUE, TRUE, 0);
  
  gtk_container_add(GTK_CONTAINER(us_win), us_vbox1);
  
  gtk_widget_show(us_vbox1);
  gtk_widget_show(us_hboxbtns);
  gtk_widget_show(us_openim);
  gtk_widget_show(us_close);
  gtk_widget_show(us_clist);
  gtk_widget_show(us_win);
  
  gtk_clist_freeze(GTK_CLIST(us_clist));

#if debug > 0
  GTKFAIM_DEBUG2("search_address", "got a response for the search on ", origname, TRUE);
#endif

  /* this does a run-through to parse TLVs.... clean... */
  i = 10;
  while (i < command->commandlen)
    {
      names[j] = aim_grabtlvstr(&(command->data[i]));
      names2[0] = names[j]->value;
      gtk_clist_append(GTK_CLIST(us_clist), names2);
      i += 2 + 2 + names[j]->length;
      j++;
      aim_freetlv(&names[j]);
    }
  gtk_clist_thaw(GTK_CLIST(us_clist));

  return 0;
}

int gtkfaim_parse_search_fail(struct command_rx_struct *command)
{
  gtkfaim_genericok("No screen names match your search criteria.");
  return 0;
}
