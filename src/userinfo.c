
#include <gtk/gtk.h>
#include <aim.h>
#include <gtkfaim.h>


struct gtkfaim_infowindow_struct *gtkfaim_infowindow = NULL;

/*

  TODO: seperate away message from normal profile!

   vbox1
   --------------------------------------------
   | hbox2                                    |
   | ---------------------------------------- |
   | | vbox4            | vbox5             | |
   | |                  |                   | |
   | |                  |                   | |
   | |                  |                   | |
   | |                  |                   | |  vbox 2
   | |                  |                   | |
   | |                  |                   | |
   | |                  |                   | |
   | |                  |                   | |
   | ---------------------------------------- |
   |------------------------------------------|
   |                                          |
   |                                          |
   |                hbox1                     |  vbox3
   |                                          |
   |                                          |
   --------------------------------------------
 */


struct gtkfaim_infowindow_struct *gtkfaim_create_infowindow(struct gtkfaim_buddy_struct *buddy, char *profile)
{
  struct gtkfaim_infowindow_struct *infowindow = NULL;
  char *tmpptr = NULL;
  char date1[25];
  char date2[25];
  char title[29] = "User Information: " ;
  
  if (buddy == NULL)
    return NULL;

  strcat(title, buddy->sn);

  infowindow = (struct gtkfaim_infowindow_struct *) malloc(sizeof(struct gtkfaim_infowindow_struct));

  infowindow->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(infowindow->window), title);

  infowindow->vbox1 = gtk_vbox_new(FALSE, 2);
  infowindow->vbox2 = gtk_vbox_new(TRUE, 0);
  infowindow->vbox3 = gtk_vbox_new(TRUE, 0);
  infowindow->vbox4 = gtk_vbox_new(TRUE, 2);
  infowindow->vbox5 = gtk_vbox_new(TRUE, 2);
  infowindow->hbox1 = gtk_hbox_new(FALSE, 0);
  infowindow->hbox2 = gtk_hbox_new(FALSE, 4);
  
  infowindow->sn_hbox = gtk_hbox_new(FALSE, 0);
  infowindow->warnlevel_hbox = gtk_hbox_new(FALSE, 0);
  infowindow->idletime_hbox = gtk_hbox_new(FALSE, 0);
  infowindow->class_hbox = gtk_hbox_new(FALSE, 0);
  infowindow->membersince_hbox = gtk_hbox_new(FALSE, 0);
  infowindow->onsince_hbox = gtk_hbox_new(FALSE, 0);

  infowindow->profile = gtk_text_new(NULL, NULL);
  infowindow->profile_vscroll = gtk_vscrollbar_new(GTK_TEXT(infowindow->profile)->vadj);

  infowindow->sn_caption = gtk_label_new("Screen Name:");
  infowindow->sn = gtk_label_new(buddy->sn);

  infowindow->warnlevel_caption = gtk_label_new("Warning Level:");
  {
    char newstr[5];
    sprintf(newstr, "%3d%%", buddy->warninglevel);
    infowindow->warnlevel = gtk_label_new(newstr);
  }

  infowindow->idletime_caption = gtk_label_new("Idle Time:");
  {
    char newstr[8];
    sprintf(newstr, "%d min", buddy->idletime);
    infowindow->idletime = gtk_label_new(newstr);
  }

#define AIM_CLASS_TRIAL 0x01
#define AIM_CLASS_INET  0x10
#define AIM_CLASS_AOL   0x04

  {
    char classes[50] = "";
    int classcnt = 0;

    infowindow->class_caption = gtk_label_new("Class:");

    if (buddy->class & AIM_CLASS_TRIAL)
      {
	if (classcnt)
	  strcat(classes, ",");
	strcat(classes, "Trial");
	classcnt++;
      }
    if (buddy->class & AIM_CLASS_INET)
      {
	if (classcnt)
	  strcat(classes, ",");
	strcat(classes, "Internet");
	classcnt++;
      }
    if (buddy->class & AIM_CLASS_AOL)
      {
	if (classcnt)
	  strcat(classes, ",");
	strcat(classes, "AOL");
	classcnt++;
      }

    if (classcnt == 0)
      {
	strcat(classes, "Unknown");
      }

    infowindow->class = gtk_label_new(classes);
  }

  infowindow->membersince_caption = gtk_label_new("Member Since:");

  {
    time_t t;
    t = (time_t)buddy->membersince;

    tmpptr = ctime(&t);
    strncpy(date1, tmpptr, 24);
    date1[24] = '\0';
    
    t = (time_t)buddy->onsince;
    tmpptr = ctime(&t);
    strncpy(date2, tmpptr, 24);
    date2[24] = '\0';
  }
  infowindow->membersince = gtk_label_new(date1);
  infowindow->onsince_caption = gtk_label_new("Online Since:");
  infowindow->onsince = gtk_label_new(date2);
  
  gtk_box_pack_start(GTK_BOX(infowindow->vbox1), infowindow->vbox2, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->vbox1), infowindow->vbox3, FALSE, FALSE, 2);

  gtk_box_pack_start(GTK_BOX(infowindow->vbox2), infowindow->hbox2, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->hbox2), infowindow->vbox4, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->hbox2), infowindow->vbox5, FALSE, FALSE, 2);
  
  gtk_box_pack_start(GTK_BOX(infowindow->vbox4), infowindow->sn_hbox, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->sn_hbox), infowindow->sn_caption, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->sn_hbox), infowindow->sn, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(infowindow->vbox5), infowindow->warnlevel_hbox, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->warnlevel_hbox), infowindow->warnlevel_caption, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->warnlevel_hbox), infowindow->warnlevel, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(infowindow->vbox5), infowindow->idletime_hbox, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->idletime_hbox), infowindow->idletime_caption, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->idletime_hbox), infowindow->idletime, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(infowindow->vbox4), infowindow->class_hbox, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->class_hbox), infowindow->class_caption, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->class_hbox), infowindow->class, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(infowindow->vbox4), infowindow->membersince_hbox, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->membersince_hbox), infowindow->membersince_caption, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->membersince_hbox), infowindow->membersince, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(infowindow->vbox5), infowindow->onsince_hbox, FALSE, FALSE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->onsince_hbox), infowindow->onsince_caption, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->onsince_hbox), infowindow->onsince, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(infowindow->vbox3), infowindow->hbox1, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(infowindow->hbox1), infowindow->profile, TRUE, TRUE, 2);
  gtk_box_pack_start(GTK_BOX(infowindow->hbox1), infowindow->profile_vscroll, FALSE, FALSE, 2);

  gtk_container_add(GTK_CONTAINER(infowindow->window), infowindow->vbox1);

  gtk_widget_show(infowindow->class_caption);
  gtk_widget_show(infowindow->class);
  gtk_widget_show(infowindow->membersince_caption);
  gtk_widget_show(infowindow->membersince);
  gtk_widget_show(infowindow->onsince_caption);
  gtk_widget_show(infowindow->onsince);
  gtk_widget_show(infowindow->sn_caption);
  gtk_widget_show(infowindow->sn);
  gtk_widget_show(infowindow->warnlevel_caption);
  gtk_widget_show(infowindow->warnlevel);
  gtk_widget_show(infowindow->idletime_caption);
  gtk_widget_show(infowindow->idletime);
  gtk_widget_show(infowindow->profile_vscroll);
  gtk_widget_show(infowindow->profile);
  gtk_widget_show(infowindow->sn_hbox);
  gtk_widget_show(infowindow->warnlevel_hbox);
  gtk_widget_show(infowindow->idletime_hbox);
  gtk_widget_show(infowindow->class_hbox);
  gtk_widget_show(infowindow->membersince_hbox);
  gtk_widget_show(infowindow->onsince_hbox);
  gtk_widget_show(infowindow->hbox1);
  gtk_widget_show(infowindow->hbox2);
  gtk_widget_show(infowindow->vbox5);
  gtk_widget_show(infowindow->vbox4);
  gtk_widget_show(infowindow->vbox3);
  gtk_widget_show(infowindow->vbox2);
  gtk_widget_show(infowindow->vbox1);
  gtk_widget_show(infowindow->window);

  if (profile != NULL)
    gtkfaim_html_insert(infowindow->profile, profile);
  else
    gtkfaim_html_insert(infowindow->profile, "[No Profile Available]");

  
  gtkfaim_infowindow = infowindow;

  return infowindow;
}

int gtkfaim_parse_userinfo(struct command_rx_struct *command)
{
  char *sn = NULL;
  char *prof_encoding = NULL;
  char *prof = NULL;
  u_short warnlevel = 0x0000;
  u_short idletime = 0x0000;
  u_short class = 0x0000;
  u_long membersince = 0x00000000;
  u_long onlinesince = 0x00000000;
  u_long snacid = 0x00000000;
  int tlvcnt = 0;
  int i = 0;

  struct gtkfaim_buddy_struct buddyinfo;
#if debug > 1
  GTKFAIM_DEBUG("userinfo", "parsing a user info snac",TRUE);
#endif

  {
    u_long snacid = 0x000000000;
    struct aim_snac_t *snac = NULL;

    snacid = (command->data[6] << 24) & 0xFF000000;
    snacid+= (command->data[7] << 16) & 0x00FF0000;
    snacid+= (command->data[8] <<  8) & 0x0000FF00;
    snacid+= (command->data[9])       & 0x000000FF;
#if debug > 1
    GTKFAIM_DEBUG("userinfo", "searching for SNAC...", TRUE);
#endif
    snac = aim_remsnac(snacid);
    if (snac)
      {
#if debug > 1
	GTKFAIM_DEBUG("userinfo", "SNAC FOUND", TRUE);
	printf("gtkfaim: reply to snacid 0x%08lx for sn %s\n", snacid, (char *)snac->data);
#endif

	free(snac->data);
	free(snac);
      }
    else
      {
#if debug > 1
	GTKFAIM_DEBUG("userinfo", "SNAC NOT FOUND", TRUE);
#endif
	return -1;
      }
  }

  snacid = ((command->data[6] << 24) & 0xFF000000);
  snacid += ((command->data[7] << 16)& 0x00FF0000);
  snacid += ((command->data[8] <<  8)& 0x0000FF00);
  snacid += ((command->data[9] )     & 0x000000FF);
#if debug > 1
  printf("gtkfaim: gtkfaim_parse_userinfo(): response to snacid 0x%08lx\n", snacid);
#endif

  sn = (char *) malloc(command->data[10]+1);
  memcpy(sn, &(command->data[11]), command->data[10]);
  sn[command->data[10]] = '\0';
  
  i = 11 + command->data[10];
  warnlevel = ((command->data[i++]) << 8) & 0xFF00;
  warnlevel += (command->data[i++]) & 0x00FF;

  tlvcnt = ((command->data[i++]) << 8) & 0xFF00;
  tlvcnt += (command->data[i++]) & 0x00FF;

  /* a mini TLV parser */
  {
    int curtlv = 0;
    int tlv1 = 0;

    while (curtlv < tlvcnt)
      {
	if ((command->data[i] == 0x00) &&
	    (command->data[i+1] == 0x01) )
	  {
	    if (tlv1)
	      break;
	    /* t(0001) = class */
	    if (command->data[i+3] != 0x02)
	      printf("gtkfaim: userinfo: **warning: strange v(%x) for t(1)\n", command->data[i+3]);
	    class = ((command->data[i+4]) << 8) & 0xFF00;
	    class += (command->data[i+5]) & 0x00FF;
	    i += (2 + 2 + command->data[i+3]);
	    tlv1++;
	  }
	else if ((command->data[i] == 0x00) &&
		 (command->data[i+1] == 0x02))
	  {
	    /* t(0002) = member since date  */
	    if (command->data[i+3] != 0x04)
	      printf("gtkfaim: userinfo: **warning: strange v(%x) for t(2)\n", command->data[i+3]);

	    membersince = ((command->data[i+4]) << 24) &  0xFF000000;
	    membersince += ((command->data[i+5]) << 16) & 0x00FF0000;
	    membersince += ((command->data[i+6]) << 8) &  0x0000FF00;
	    membersince += ((command->data[i+7]) ) &      0x000000FF;
	    i += (2 + 2 + command->data[i+3]);
	  }
	else if ((command->data[i] == 0x00) &&
		 (command->data[i+1] == 0x03))
	  {
	    /* t(0003) = on since date  */
	    if (command->data[i+3] != 0x04)
	      printf("gtkfaim: userinfo: **warning: strange v(%x) for t(3)\n", command->data[i+3]);

	    onlinesince = ((command->data[i+4]) << 24) &  0xFF000000;
	    onlinesince += ((command->data[i+5]) << 16) & 0x00FF0000;
	    onlinesince += ((command->data[i+6]) << 8) &  0x0000FF00;
	    onlinesince += ((command->data[i+7]) ) &      0x000000FF;
	    i += (2 + 2 + command->data[i+3]);
	  }
	else if ((command->data[i] == 0x00) &&
		 (command->data[i+1] == 0x04) )
	  {
	    /* t(0004) = idle time */
	    if (command->data[i+3] != 0x02)
	      printf("gtkfaim: userinfo: **warning: strange v(%x) for t(4)\n", command->data[i+3]);
	    idletime = ((command->data[i+4]) << 8) & 0xFF00;
	    idletime += (command->data[i+5]) & 0x00FF;
	    i += (2 + 2 + command->data[i+3]);
	  }  
	else
	  {
	    printf("gtkfaim: userinfo: **warning: unexpected TLV t(%02x%02x) l(%02x%02x)\n", command->data[i], command->data[i+1], command->data[i+2], command->data[i+3]);
	    i += (2 + 2 + command->data[i+3]);
	  }
	curtlv++;
      }
  }
  if (i < command->commandlen)
    {
      if ( (command->data[i] == 0x00) &&
	   (command->data[i+1] == 0x01) )
	{
	  int len = 0;
	  
	  len = ((command->data[i+2] << 8) & 0xFF00);
	  len += (command->data[i+3]) & 0x00FF;
	  
	  prof_encoding = (char *) malloc(len+1);
	  memcpy(prof_encoding, &(command->data[i+4]), len);
	  prof_encoding[len] = '\0';
	  
	  i += (2+2+len);
	}
      else
	{
	  printf("gtkfaim: userinfo: **warning: unexpected TLV after TLVblock t(%02x%02x) l(%02x%02x)\n", command->data[i], command->data[i+1], command->data[i+2], command->data[i+3]);
	  i += 2 + 2 + command->data[i+3];
	}
    }

  if (i < command->commandlen)
    {
      int len = 0;

      len = ((command->data[i+2]) << 8) & 0xFF00;
      len += (command->data[i+3]) & 0x00FF;

      prof = (char *) malloc(len+1);
      memcpy(prof, &(command->data[i+4]), len);
      prof[len] = '\0';
    }
#if debug > 1
  printf("gtkfaim: userinfo: sn = \"%s\" (len=%d)\n", sn, command->data[10]);
  printf("gtkfaim: userinfo: warnlevel = 0x%04x\n", warnlevel);
  printf("gtkfaim: userinfo: tlvcnt = 0x%04x\n", tlvcnt);
  printf("gtkfaim: userinfo: class = 0x%04x ", class);
  if (class == 0x0010)
    printf("(FREE)\n");
  else if (class == 0x0011)
    printf("(TRIAL)\n");
  else if (class == 0x0004)
    printf("(AOL)\n");
  else
    printf("(UNKNOWN)\n");
  printf("gtkfaim: userinfo: membersince = %lu\n", membersince);
  printf("gtkfaim: userinfo: onlinesince = %lu\n", onlinesince);
  printf("gtkfaim: userinfo: idletime = 0x%04x\n", idletime);

  if (prof_encoding != NULL)
    printf("gtkfaim: userinfo: prof_encoding = \"%s\" (%d)\n", prof_encoding, strlen(prof_encoding));
  else
    printf("gtkfaim: userinfo: prof_encoding = [none]\n");

  if (prof != NULL)
    printf("gtkfaim: userinfo: prof = \"%s\" (%d)\n", prof, strlen(prof));
  else
    printf("gtkfaim: userinfo: prof = [none]\n");
#endif

  buddyinfo.sn = sn;
  buddyinfo.warninglevel = warnlevel;
  buddyinfo.class = class;
  buddyinfo.idletime = idletime;
  buddyinfo.membersince = (time_t) membersince;
  buddyinfo.onsince = (time_t) onlinesince;

  gtkfaim_create_infowindow(&buddyinfo, prof);

  if (sn != NULL)
    free(sn);
  if (prof_encoding != NULL)
    free(prof_encoding);
  if (prof != NULL)
    free(prof);

  snac_outstanding = 0x00000000; /* reset it */

  return 0;
}

int gtkfaim_parse_snacerror(struct command_rx_struct *command)
{
  u_long snacid = 0x00000000;
  struct aim_snac_t *snac = NULL;

  snacid = ((command->data[6] << 24) & 0xFF000000);
  snacid += ((command->data[7] << 16)& 0x00FF0000);
  snacid += ((command->data[8] <<  8)& 0x0000FF00);
  snacid += ((command->data[9] )     & 0x000000FF);

  /* now this is beautifully clean... */
  snac = aim_remsnac(snacid);
  if (snac)
    {
      if (snac->data)
	{
	  char sentance[255] = "User information for ";
	  char sentance2[] =" not available.";
	  strcat(sentance, (char *)snac->data);
	  strcat(sentance, sentance2);

	  gtkfaim_genericok(sentance);
	}

      free(snac->data);
      free(snac);
    }
  else
    {
      gtkfaim_genericok("Got an error for an unknown SNAC!");
      return 0;
    }


#if 0
  if (snacid == snac_outstanding)
    {
      printf("gtkfaim: error 0x%02x%02x: ", command->data[10], command->data[11]);
      if (command->data[11] == 0x04)
	{
	  printf(" probably requested information for user not logged on or for an invalid name\n");
	  gtkfaim_genericok("User information requested either for an unknown user or for a user that is not online.\n");
	}
      else
	printf(" unknown.\n");
    }
  else
    printf("gtkfaim: received error for an unrequested SNACid! (snacid = %08lx)\n", snacid);
#if debug > 1
  printf("Received error in SNAC family 0x0002 (snacid = %08lx)\n", snacid);
#endif
#endif
  return 0; 
}
