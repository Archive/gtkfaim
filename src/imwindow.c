
#include <aim.h>
#include <gtkfaim.h>
#include <time.h>

/* list of open IM windows */
GList *gtkfaim_imwindowlist = NULL;


/*
 * Forward declarations
 */
void                gtkfaim_add_imwindow                ( char *destsn );
gtkfaim_imwindow_t *gtkfaim_create_imwindow             ( char *destsn );
void                gtkfaim_imwindow_close              ( GtkWidget *widget,
                                                          gpointer data );
#if debug > 1
int                 gtkfaim_imwindow_print              ( void );
#endif
gtkfaim_imwindow_t *gtkfaim_imwindow_search_button_close( GtkWidget *criteria );
gtkfaim_imwindow_t *gtkfaim_imwindow_search_button_send ( GtkWidget *criteria );
gtkfaim_imwindow_t *gtkfaim_imwindow_search_button_info ( GtkWidget *criteria );
void                gtkfaim_nonbuddy_openim             ( gchar *destsn );
int                 gtkfaim_send_im                     ( char *destsn,
                                                          char *msg );
int                 gtkfaim_send_im_away                ( char *destsn );
int                 gtkfaim_send_im_away_putinbox       ( char *destsn,
                                                          char *msg );



/*
 * Private functions
 */
gtkfaim_imwindow_t *
gtkfaim_search_imwindow( char *crit )
{
   gint                listlen = g_list_length( gtkfaim_imwindowlist );
   gint                i;
   gtkfaim_imwindow_t *tmpdata;
  
   for( i = 0; i < listlen; i++ )
   {
      if( ( tmpdata = (gtkfaim_imwindow_t *)g_list_nth_data( gtkfaim_imwindowlist, i ) ) &&
	  !sncmp( tmpdata->buddy->sn, crit ) )
         return tmpdata;
   }

   return NULL;
}

void gtkfaim_nonbuddy_openim( gchar *destsn )
{
   char newdestsn[11];

   if( ( destsn == NULL ) || ( strlen( destsn ) <= 4 ) )
   {
      printf( "gtkfaim_nonbuddy_openim(): SN MISSING or less than four characters!\n" );
      gtkfaim_genericok( "Destination screen name is invalid." );
      return;
   }
  
   if (strlen(destsn) > 10)
   {
      printf( "gtkfaim_nonbuddy_openim(): truncating \"%s\" to 10 bytes length\n", destsn );
      memcpy( newdestsn, destsn, 10 );
      newdestsn[10] = '\0';
   }
   else
   {
      memcpy( newdestsn, destsn, strlen( destsn ) );
      newdestsn[strlen(destsn)] = '\0';
   }

   if( gtkfaim_search_imwindow( newdestsn ) == NULL )
   {
#if debug > 1
      printf( "***opening window for %s\n", newdestsn );
#endif
      /* open up a blank IM window for this buddy */
      gtkfaim_add_imwindow( newdestsn );
   }
}



/*
 * Gtk Event Handlers
 */
static void
gtkfaim_nonbuddy_cancel( GtkWidget *widget, gpointer data )
{
   if( gtkfaim_nonbuddy_getsn )
   {
      gtk_widget_hide( gtkfaim_nonbuddy_getsn->window );
#ifndef GNOME
      gtk_widget_destroy( gtkfaim_nonbuddy_getsn->cancel );
      gtk_widget_destroy( gtkfaim_nonbuddy_getsn->imopen );
      gtk_widget_destroy( gtkfaim_nonbuddy_getsn->textbox );
      gtk_widget_destroy( gtkfaim_nonbuddy_getsn->vbox );
#endif
      gtk_widget_destroy( gtkfaim_nonbuddy_getsn->window );
      g_free( gtkfaim_nonbuddy_getsn );
      gtkfaim_nonbuddy_getsn = NULL;
   }
}

#ifdef GNOME
static void
gtkfaim_nonbuddy_openim_click( gchar *string, gpointer data )
#else
static void
gtkfaim_nonbuddy_openim_click( GtkWidget *widget, gpointer data )
#endif
{
   char *destsn = NULL;
  
   /* grab only the first 10 chars (the maximum AIM SN length) */
#ifdef GNOME
   destsn = string;
   if( destsn == NULL )
      return;
#else
   destsn = gtk_editable_get_chars( GTK_EDITABLE( gtkfaim_nonbuddy_getsn->textbox ), 0, -1 );
#endif

   gtkfaim_nonbuddy_openim( destsn );

   g_free( destsn );

   /* close the window */
   gtkfaim_nonbuddy_cancel( NULL, NULL );
   return;
}

void
gtkfaim_buddylist_sendim_click( GtkWidget *widget, gpointer data )
{
   GtkWidget *snwindow;
#ifndef GNOME
   GtkWidget *snwindow_vbox;
   GtkWidget *snwindow_imopen;
   GtkWidget *snwindow_cancel;
   GtkWidget *snwindow_textbox;
#endif
   GList *selection;

   /* we can only have one of these in existance at a time */
   if( gtkfaim_nonbuddy_getsn )
      return;
  
   selection = GTK_TREE( gtkfaim_window_buddies.monitor_tree )->selection;
   if( selection )
   {
      gtkfaim_group_t *group;
      gint             curnode;

      for( curnode = 0;
           ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
           curnode++ )
      {
         gtkfaim_buddylist_node_t *node;
         gint                      i;

         /*
          * If selected node is a group, break out of for loop
          */
         if( group->treenode == selection->data )
            break;

         for( i = 0;
              ( node = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, i ) );
              i++ )
         {
            if( node->treenode == selection->data )
            {
               gtkfaim_nonbuddy_openim( GTK_LABEL( GTK_BIN( node->treenode )->child )->label );
               return;
            }
         }
      }
   }
  
#ifdef GNOME
   snwindow = gnome_request_string_dialog("Screenname to send IM to",
               GTK_SIGNAL_FUNC(gtkfaim_nonbuddy_openim), NULL);
   gtk_signal_connect(GTK_OBJECT(snwindow), "close",
               GTK_SIGNAL_FUNC(gtkfaim_nonbuddy_cancel), NULL);
#else
   snwindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
   gtk_window_set_title((GtkWindow *) snwindow, "Send IM");
#endif
   gtk_window_position(GTK_WINDOW(snwindow), GTK_WIN_POS_CENTER);
  
#ifndef GNOME
   snwindow_vbox = gtk_vbox_new(FALSE, 0);
   snwindow_textbox = gtk_text_new(NULL, NULL);
   gtk_text_set_editable(GTK_TEXT(snwindow_textbox), TRUE);
   gtk_widget_set_usize(GTK_WIDGET(snwindow_textbox), 1, 20);
   gtk_box_pack_start(GTK_BOX(snwindow_vbox), snwindow_textbox, FALSE, FALSE, 2);

   snwindow_imopen = gtk_button_new_with_label("Open IM Window");
   gtk_signal_connect(GTK_OBJECT(snwindow_imopen), "clicked", GTK_SIGNAL_FUNC(gtkfaim_nonbuddy_openim_click), NULL);
   gtk_box_pack_start(GTK_BOX(snwindow_vbox), snwindow_imopen, FALSE, FALSE, 0);

   snwindow_cancel = gtk_button_new_with_label("Cancel");
   gtk_signal_connect(GTK_OBJECT(snwindow_cancel), "clicked", GTK_SIGNAL_FUNC(gtkfaim_nonbuddy_cancel), NULL);
   gtk_box_pack_start(GTK_BOX(snwindow_vbox), snwindow_cancel, FALSE, FALSE, 0);

   gtk_container_add(GTK_CONTAINER(snwindow), snwindow_vbox);
#endif
   /* go global */
   gtkfaim_nonbuddy_getsn = g_new(struct gtkfaim_window_nonbuddy_getsn_struct,1);

   gtkfaim_nonbuddy_getsn->window = snwindow;
#ifndef GNOME
   gtkfaim_nonbuddy_getsn->vbox = snwindow_vbox;
   gtkfaim_nonbuddy_getsn->imopen = snwindow_imopen;
   gtkfaim_nonbuddy_getsn->cancel = snwindow_cancel;
   gtkfaim_nonbuddy_getsn->textbox = snwindow_textbox;

   /* show it */
   gtk_widget_show(snwindow_textbox);
   gtk_widget_show(snwindow_cancel);
   gtk_widget_show(snwindow_imopen);
   gtk_widget_show(snwindow_vbox);
#endif
   gtk_widget_show(snwindow);
}

void
gtkfaim_imwindow_send( GtkWidget *widget, gpointer data )
{
   gtkfaim_imwindow_t *destwindow;
   char               *msg;

   /* figure out which window we're getting called by */
   if( ( destwindow = gtkfaim_imwindow_search_button_send( widget ) ) == NULL )
   {
      printf( "gtkfaim_imwindow_send(): attempting to send IM to an unknown buddy!\n" );
      return;
   }

   /* if we're passed some text, then use it -- probably from keycb */
   if( data )
      msg = (char *) data;
   else /* retrieve all the text of the box */
      msg = gtk_editable_get_chars( GTK_EDITABLE( destwindow->textbox_msg ), 0, -1 );
  
#ifndef ALLOWBLANKS
   if( strlen( msg ) < 1 )
      return;
#endif

   /* wipe out the message from visibility */
   gtkfaim_clear_textbox( destwindow->textbox_msg );

   /* we should probably add some HTML stuff here */

   /* continue the process in gtkfaim_send_im() */
   gtkfaim_send_im( destwindow->buddy->sn, msg );

   /* free up GTK+'s buffer -- but only if we grabbed the text */
   if( data == NULL )
      g_free( msg );
}





/*
 * Public functions
 */
void
gtkfaim_add_imwindow( char *destsn )
{
   gtkfaim_imwindow_t *imwindow;

   /* return if there's already a window for this SN */
   if( ( imwindow = gtkfaim_search_imwindow( destsn ) ) )
   {
#if debug > 1
      printf("gtkfaim_add_imwindow(): returning; window already exists\n");
#endif
      return;
   }

   /* make a new window widget set */
   if( ( imwindow = gtkfaim_create_imwindow( destsn ) ) == NULL )
   {
#if debug > 1
      printf("gtkfaim_add_imwindow(): returning; gtkfaim_create_imwindow() failed\n");
#endif
      return;
   }

   /* enlist it */
   gtkfaim_imwindowlist = g_list_prepend( gtkfaim_imwindowlist, imwindow );
#if debug > 1
   printf("\n\nafter add:\n");
   gtkfaim_imwindow_print();
#endif
   return;
}

/* how about this for a bit of simplistic trickery... */
void gtkfaim_imwindow_keycb(GtkWidget *widget, gpointer data)
{
  gchar *text = NULL;

  if (!data)
    {
      printf("gtkfaim: imwindow_keycb: got to callback without an attatched window!\n");
      return;
    }
 
  text = gtk_editable_get_chars(GTK_EDITABLE(widget), 0, -1);
  
  if (strchr(text, '\n') != NULL)
    gtkfaim_imwindow_send(GTK_WIDGET(data), strtok(text, "\n"));

  g_free(text);

  return;
}		 

void gtkfaim_imwindow_info(GtkWidget *widget, gpointer data)
{
  gtkfaim_imwindow_t *imwin = gtkfaim_imwindow_search_button_info(widget);

  if (imwin == NULL)
    return;
  
  aim_getinfo( imwin->buddy->sn );
   
  return;
}


/*
 * Dialog Construction Functions
 */
gtkfaim_imwindow_t *
gtkfaim_create_imwindow( char *destsn )
{
   gtkfaim_imwindow_t *newwindow;
   gtkfaim_buddy_t    *buddy;

#if debug > 2
   printf( "gtkfaim_create_imwindow(): entering for %s\n", destsn );
#endif

   /* allocate the struct */
   if( ( newwindow = g_new( gtkfaim_imwindow_t, 1 ) ) == NULL )
   {
      printf( "gtkfaim_create_imwindow(): alloc(gtkfaim_imwindow_struct) failed\n" );
      return NULL;
   }

   if( ( buddy = gtkfaim_buddy_find( destsn ) ) == 0 )
   {
      buddy = gtkfaim_buddy_new( destsn, NULL );
      if( gtkfaim_temporary_group == NULL )
         gtkfaim_temporary_group = gtkfaim_group_new( "Temporary" );

      /* create an entry in the buddy list, even if they're not really a buddy */
      /*  (it's ok for this to fail) */
      gtkfaim_group_add_buddy( gtkfaim_temporary_group, buddy );
   }

   newwindow->buddy = buddy;

   /* attempt create */
#ifdef GNOME
   newwindow->widgetptr = gnome_app_new( _("gtkFAIM"), destsn );
#else
   newwindow->widgetptr = gtk_window_new( GTK_WINDOW_TOPLEVEL );
   if( newwindow->widgetptr )
      /* set the window title to destsn */
      gtk_window_set_title( (GtkWindow *)newwindow->widgetptr, destsn );
#endif
  
   /* if create failed, return NULL */
   if( newwindow->widgetptr == NULL )
   {
      printf( "gtkfaim_create_imwindow(): gtk_window_new() failed\n" );
      g_free( newwindow );
      return NULL;
   }

#ifdef GTK_ACCELGROUP
   if( ( newwindow->accelgroup = gtk_accel_group_new() ) == NULL )
   {
      printf( "gtkfaim_create_imwindow(): gtk_accel_group_new() failed.\n" );
   }
   gtk_window_add_accel_group( GTK_WINDOW( newwindow->widgetptr ), newwindow->accelgroup );
#else
   if( ( newwindow->acceltable = gtk_accelerator_table_new() ) == NULL )
   {
      printf( "gtkfaim_create_imwindow(): gtk_accelerator_table_new() failed\n" );
   }
   gtk_window_add_accelerator_table( GTK_WINDOW( newwindow->widgetptr ), newwindow->acceltable );
#endif

   /* ok, we create a window-spanning virtual packing box, containing
      three horizonal boxes.  The first two hboxes contain one textbox+
      scrollbar each, and the third contains the four buttons */
 
   newwindow->vbox = gtk_vbox_new(FALSE, 10);
   newwindow->paned = gtk_vpaned_new();
   newwindow->hbox_text1 = gtk_hbox_new(FALSE, 0);
   newwindow->hbox_text2 = gtk_hbox_new(FALSE, 0);
   newwindow->hbox_buttons = gtk_hbox_new(TRUE, 10);

   /* text boxes */
   newwindow->textbox_conversation = gtk_text_new(NULL,NULL);
   newwindow->textbox_msg = gtk_text_new(NULL,NULL);
   /* scroll bars */
   newwindow->textbox_conversation_scroll = gtk_vscrollbar_new(GTK_TEXT(newwindow->textbox_conversation)->vadj);
   newwindow->textbox_msg_scroll = gtk_vscrollbar_new(GTK_TEXT(newwindow->textbox_msg)->vadj);

   /* text box properties */
   gtk_text_set_editable(GTK_TEXT(newwindow->textbox_conversation), FALSE);
   gtk_text_set_editable(GTK_TEXT(newwindow->textbox_msg), TRUE);
   gtk_text_set_word_wrap(GTK_TEXT(newwindow->textbox_conversation), TRUE);
   gtk_text_set_word_wrap(GTK_TEXT(newwindow->textbox_msg), TRUE);

   newwindow->button_send = gtk_button_new_with_label("Send");
   newwindow->button_close = gtk_button_new_with_label("Close");
   newwindow->button_info = gtk_button_new_with_label("Info");
#ifndef NOZAP
   newwindow->button_zap = gtk_button_new_with_label("Zap");
#endif

   if (gtkfaim_config.sendonenter)
      gtk_signal_connect(GTK_OBJECT(newwindow->textbox_msg), "changed", GTK_SIGNAL_FUNC( gtkfaim_imwindow_keycb ), newwindow->button_send);


   gtk_paned_add1(GTK_PANED(newwindow->paned), newwindow->hbox_text1);
   gtk_paned_add2(GTK_PANED(newwindow->paned), newwindow->hbox_text2);
   gtk_box_pack_start(GTK_BOX(newwindow->vbox), newwindow->paned, TRUE, TRUE, 0);

   gtk_box_pack_start(GTK_BOX(newwindow->hbox_text1), newwindow->textbox_conversation, TRUE, TRUE, 0);
   gtk_box_pack_start(GTK_BOX(newwindow->hbox_text1), newwindow->textbox_conversation_scroll, FALSE, TRUE, 0);

   gtk_box_pack_start(GTK_BOX(newwindow->hbox_text2), newwindow->textbox_msg, TRUE, TRUE, 0);
   gtk_box_pack_start(GTK_BOX(newwindow->hbox_text2), newwindow->textbox_msg_scroll, FALSE, TRUE, 0);

   gtk_widget_show(newwindow->hbox_text1);
   gtk_widget_show(newwindow->hbox_text2);
   gtk_widget_show(newwindow->paned);
   gtk_widget_show(newwindow->textbox_conversation);
   gtk_widget_show(newwindow->textbox_conversation_scroll);
   gtk_widget_show(newwindow->textbox_msg);
   gtk_widget_show(newwindow->textbox_msg_scroll);

   gtk_box_pack_start(GTK_BOX(newwindow->vbox), newwindow->hbox_buttons, FALSE, FALSE, 0);
   gtk_box_pack_start(GTK_BOX(newwindow->hbox_buttons), newwindow->button_info, FALSE, TRUE, 0);
#ifndef NOZAP
   gtk_box_pack_start(GTK_BOX(newwindow->hbox_buttons), newwindow->button_zap, FALSE, TRUE, 0);
#endif
   gtk_box_pack_start(GTK_BOX(newwindow->hbox_buttons), newwindow->button_send, FALSE, TRUE, 0);
   gtk_box_pack_start(GTK_BOX(newwindow->hbox_buttons), newwindow->button_close, FALSE, TRUE, 0);
   gtk_widget_show(newwindow->button_info);
#ifndef NOZAP
   gtk_widget_show(newwindow->button_zap);
#endif
   gtk_widget_show(newwindow->button_send);
   gtk_widget_show(newwindow->button_close);
   gtk_widget_show(newwindow->hbox_buttons);

#ifdef GNOME
   gnome_app_set_contents(GNOME_APP(newwindow->widgetptr), newwindow->vbox);
#else
   gtk_container_add(GTK_CONTAINER(newwindow->widgetptr), newwindow->vbox);
#endif
   gtk_widget_show(newwindow->vbox);

   /* show it */
   gtk_container_border_width(GTK_CONTAINER(newwindow->widgetptr), 10);

   /* attach a handler for the "send" button */
   gtk_signal_connect(GTK_OBJECT(newwindow->button_send), "clicked", GTK_SIGNAL_FUNC(gtkfaim_imwindow_send), NULL);
  
   gtk_signal_connect(GTK_OBJECT(newwindow->button_close), "clicked", GTK_SIGNAL_FUNC(gtkfaim_imwindow_close), NULL);

   gtk_signal_connect(GTK_OBJECT(newwindow->button_info), "clicked", GTK_SIGNAL_FUNC(gtkfaim_imwindow_info), NULL);

#ifdef GTK_ACCELGROUP
   gtk_accel_group_add(newwindow->accelgroup, 'S', GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE, GTK_OBJECT(newwindow->button_send), "clicked");
#else
   gtk_accelerator_table_install(newwindow->acceltable, GTK_OBJECT(newwindow->button_send), "clicked", 'S', GDK_CONTROL_MASK);
#endif
   /* show it */
   gtk_widget_show(newwindow->widgetptr);
#if debug > 2
   printf("createimwindow: done for %s\n", destsn);
#endif
   /* return */
   return newwindow;
}



/* NOTE: this is terrible code.  this will all be blissful once the
   new backend gets dropped in (ie, real TLV support) */
int gtkfaim_parse_incoming_im(struct command_rx_struct *command)
{
  int i = 0;
  int tlvcnt = 0;
  char *srcsn = NULL;
  char *msg = NULL;
  unsigned int msglen = 0;
  gtkfaim_imwindow_t *destwindow;
  char color[6];
  char color2[6];
  char date[25];
  int isautoreply = 0;
  int isnewwindow = 0;

  i = 20;
  
  srcsn = g_new(char, (command->data[i]) + 1);
  memcpy(srcsn, &(command->data[i+1]), command->data[i]);
  srcsn[command->data[i]] = '\0';
#ifdef USE_SNAC_FOR_IMS
  {
    u_long snacid = 0x00000000;
    struct aim_snac_t *snac = NULL;
    
    snacid = ((command->data[6] << 24) & 0xFF000000);
    snacid += ((command->data[7] << 16)& 0x00FF0000);
    snacid += ((command->data[8] <<  8)& 0x0000FF00);
    snacid += ((command->data[9] )     & 0x000000FF);
    
    snac = aim_remsnac(snacid);
    if (snac)
      {
	if (snac->data)
	  free(snac->data);
	free(snac);
      }
  }
#endif

  /* get ptr to destination IM window */
  if ((destwindow = gtkfaim_search_imwindow(srcsn)) == NULL)
    {
      gtkfaim_add_imwindow( srcsn );
      destwindow = gtkfaim_search_imwindow(srcsn);
      isnewwindow = 1;
    }

  /* set insertion point to end of text box */
  gtk_text_set_point(GTK_TEXT(destwindow->textbox_conversation), gtk_text_get_length(GTK_TEXT(destwindow->textbox_conversation)));
  
  /* if we're not at the beginning, insert a CR */
  if (gtk_text_get_point(GTK_TEXT(destwindow->textbox_conversation)) != 0)
    gtk_text_insert(GTK_TEXT(destwindow->textbox_conversation), NULL, NULL, NULL, "\n", -1);

  i += (int) command->data[i] + 1; /* add SN len */
  
  /* warning level */
  destwindow->buddy->warninglevel = (command->data[i] << 8);
  destwindow->buddy->warninglevel += (command->data[i+1]);
  i += 2;

  tlvcnt = ((command->data[i++]) << 8) & 0xFF00;
  tlvcnt += (command->data[i++]) & 0x00FF;
  
  /* a mini TLV parser */
  {
    int curtlv = 0;
    int tlv1 = 0;

    while (curtlv < tlvcnt)
      {
	if ((command->data[i] == 0x00) &&
	    (command->data[i+1] == 0x01) )
	  {
	    if (tlv1)
	      break;
	    /* t(0001) = class */
	    if (command->data[i+3] != 0x02)
	      printf("faim: userinfo: **warning: strange v(%x) for t(1)\n", command->data[i+3]);
	    destwindow->buddy->class = ((command->data[i+4]) << 8) & 0xFF00;
	    destwindow->buddy->class += (command->data[i+5]) & 0x00FF;
	    i += (2 + 2 + command->data[i+3]);
	    tlv1++;
	  }
	else if ((command->data[i] == 0x00) &&
		 (command->data[i+1] == 0x02))
	  {
	    /* t(0002) = member since date  */
	    if (command->data[i+3] != 0x04)
	      printf("faim: userinfo: **warning: strange v(%x) for t(2)\n", command->data[i+3]);

	    destwindow->buddy->membersince = ((command->data[i+4]) << 24) &  0xFF000000;
	    destwindow->buddy->membersince += ((command->data[i+5]) << 16) & 0x00FF0000;
	    destwindow->buddy->membersince += ((command->data[i+6]) << 8) &  0x0000FF00;
	    destwindow->buddy->membersince += ((command->data[i+7]) ) &      0x000000FF;
	    i += (2 + 2 + command->data[i+3]);
	  }
	else if ((command->data[i] == 0x00) &&
		 (command->data[i+1] == 0x03))
	  {
	    /* t(0003) = on since date  */
	    if (command->data[i+3] != 0x04)
	      printf("faim: userinfo: **warning: strange v(%x) for t(3)\n", command->data[i+3]);

	    destwindow->buddy->onsince = ((command->data[i+4]) << 24) &  0xFF000000;
	    destwindow->buddy->onsince += ((command->data[i+5]) << 16) & 0x00FF0000;
	    destwindow->buddy->onsince += ((command->data[i+6]) << 8) &  0x0000FF00;
	    destwindow->buddy->onsince += ((command->data[i+7]) ) &      0x000000FF;
	    i += (2 + 2 + command->data[i+3]);
	  }
	else if ((command->data[i] == 0x00) &&
		 (command->data[i+1] == 0x04) )
	  {
	    /* t(0004) = idle time */
	    if (command->data[i+3] != 0x02)
	      printf("faim: userinfo: **warning: strange v(%x) for t(4)\n", command->data[i+3]);
	    destwindow->buddy->idletime = ((command->data[i+4]) << 8) & 0xFF00;
	    destwindow->buddy->idletime += (command->data[i+5]) & 0x00FF;
	    i += (2 + 2 + command->data[i+3]);
	  }  
	else
	  {
	    printf("faim: userinfo: **warning: unexpected TLV t(%02x%02x) l(%02x%02x)\n", command->data[i], command->data[i+1], command->data[i+2], command->data[i+3]);
	    i += (2 + 2 + command->data[i+3]);
	  }
	curtlv++;
      }
  }

  {
    /* detect if this is an auto-response or not */
    /*   auto-responses can be detected by the presence of a *second* TLV with
	 t(0004), but of zero length (and therefore no value portion) */
    struct aim_tlv_t *tsttlv = NULL;
    tsttlv = aim_grabtlv((u_char *) &(command->data[i]));
    if (tsttlv->type == 0x04)
      isautoreply = TRUE;
    aim_freetlv(&tsttlv);
  }

#if debug > 1
  printf("faim: icbm: sn = \"%s\" (len=%d)\n", destwindow->buddy->sn, command->data[10]);
  printf("faim: icbm: warnlevel = 0x%04x\n", destwindow->buddy->warninglevel);
  printf("faim: icbm: tlvcnt = 0x%04x\n", tlvcnt);
  printf("faim: icbm: class = 0x%04x ", destwindow->buddy->class);
  if (destwindow->buddy->class == 0x0010)
    printf("(FREE)\n");
  else if (destwindow->buddy->class == 0x0011)
    printf("(TRIAL)\n");
  else if (destwindow->buddy->class == 0x0004)
    printf("(AOL)\n");
  else
    printf("(UNKNOWN)\n");
  printf("faim: icbm: membersince = %lu\n", destwindow->buddy->membersince);
  printf("faim: icbm: onlinesince = %lu\n", destwindow->buddy->onsince);
  printf("faim: icbm: idletime = 0x%04x\n", destwindow->buddy->idletime);
  printf("faim: icbm: isautoreply = %s\n", isautoreply ? "TRUE" : "FALSE");
#endif

  i += 2;
  
  i += 2; /* skip first msglen */
  i += 7; /* skip garbage */
  i -= 4;

  /* oh boy is this terrible...  this comes from a specific of the spec */
  while(1)
    {
      if ( (command->data[i] == 0x00) &&
	   (command->data[i+1] == 0x00) &&
	   (command->data[i+2] == 0x00) &&
	   (command->data[i+3] == 0x00) )
	break;
      else
	i++;
    }

  i -= 2;
  
  if ( (command->data[i] == 0x00) &&
       (command->data[i+1] == 0x00) )
    i += 2;

  msglen = ( (( (unsigned int) command->data[i]) & 0xFF ) << 8);
  msglen += ( (unsigned int) command->data[i+1]) & 0xFF; /* mask off garbage */
  i += 2;

  msglen -= 4; /* skip four 0x00s */
  i += 4;

  msg = g_new(char, msglen +1);

  memcpy(msg, &(command->data[i]), msglen);
  msg[msglen] = '\0'; 

  /* put the message in the coversation box */

  /* generate the color strings */
  sprintf(color, "%02x%02x%02x", gtkfaim_config.imsg_dest_fgcolor.red, gtkfaim_config.imsg_dest_fgcolor.green, gtkfaim_config.imsg_dest_fgcolor.blue);
  sprintf(color2, "%02x%02x%02x", gtkfaim_config.imsg_dest_bgcolor.red, gtkfaim_config.imsg_dest_bgcolor.green, gtkfaim_config.imsg_dest_bgcolor.blue);
 
  {
    char premsg[255] = "<html><font color=\"#";
    strcat(premsg, color);
    strcat(premsg, "\" back=\"#");
    strcat(premsg, color2);
    strcat(premsg, "\">");
    if (gtkfaim_config.timestamps)
      {
	time_t timet;
	struct tm *t;

	time(&timet);
	t = localtime(&timet);
	sprintf(date, "[%02d:%02d:%02d] ", t->tm_hour, t->tm_min, t->tm_sec);
	strcat(premsg, date);
      }
    if (isautoreply)
      strcat(premsg, "AutoResponse from ");
    strcat(premsg, srcsn);
    strcat(premsg, ": </font></html>");
    gtkfaim_html_insert(destwindow->textbox_conversation, premsg);
  }

  gtkfaim_html_insert(destwindow->textbox_conversation, msg);

  gtk_range_draw_step_forw(GTK_RANGE(destwindow->textbox_conversation_scroll));

  /* set insertion point to end of text box */
  gtk_text_set_point(GTK_TEXT(destwindow->textbox_conversation), gtk_text_get_length(GTK_TEXT(destwindow->textbox_conversation)));

  if (gtkfaim_away)
    {
      /* this is to prevent a nasty little loop I dreamed up */
      if (!isautoreply)
	{
	  gtkfaim_send_im_away(srcsn);
	}
    }

  /* Do the oninmessage */
  if( gtkfaim_config.oninmessage && !fork() )
    {
      /* Child */
      execlp( gtkfaim_config.oninmessage, 
	      /* FIXME: argv[0] should give pathname. */ 
             gtkfaim_config.oninmessage, 
	      /* FIXME: argv[1] should give isnewwindow. */
	      isnewwindow ? "1" : "0", srcsn, msg, NULL );
      /* if we still exist, there must of been an error */
      perror( "gtkfaim: executing oninmessage action" );
      exit( 1 );
    }
  
  /* free */
  g_free(srcsn);
  g_free(msg);
#ifdef USE_SNAC_FOR_IMS
  /* this is just a convenient place to put this */
  aim_cleansnacs(60); /* clean out all SNACs over 60sec old */
#endif
  
  return 0;
}

int gtkfaim_send_im_away_putinbox(char *destsn, char *msg)  
{
  gtkfaim_imwindow_t *destwindow = NULL;
  char color[6];
  char color2[6];
  char date[25];

  /* get ptr to destination IM window */
  destwindow = gtkfaim_search_imwindow(destsn);
  
  /* freeze the textbox events */
  gtk_text_freeze(GTK_TEXT(destwindow->textbox_conversation));

  /* set insertion point to end of text box */
  gtk_text_set_point(GTK_TEXT(destwindow->textbox_conversation), gtk_text_get_length(GTK_TEXT(destwindow->textbox_conversation)));
  
  /* if we're not at the beginning, insert a CR */
  if (gtk_text_get_point(GTK_TEXT(destwindow->textbox_conversation)) != 0)
    gtk_text_insert(GTK_TEXT(destwindow->textbox_conversation), NULL, NULL, NULL, "\n", -1);
  
  /* put the message in the coversation box */

  sprintf(color, "%02x%02x%02x", gtkfaim_config.imsg_src_fgcolor.red, gtkfaim_config.imsg_src_fgcolor.green, gtkfaim_config.imsg_src_fgcolor.blue);
  sprintf(color2, "%02x%02x%02x", gtkfaim_config.imsg_src_bgcolor.red, gtkfaim_config.imsg_src_bgcolor.green, gtkfaim_config.imsg_src_bgcolor.blue);
 
  /* source name */

  {
    char premsg[255] = "<html><font color=\"#";
    strcat(premsg, color);
    strcat(premsg, "\" back=\"#");
    strcat(premsg, color2);
    strcat(premsg, "\">");
    if (gtkfaim_config.timestamps)
      {
	time_t timet;
	struct tm *t;

	time(&timet);
	t = localtime(&timet);
	sprintf(date, "[%02d:%02d:%02d] ", t->tm_hour, t->tm_min, t->tm_sec);
	strcat(premsg, date);
      }
    strcat(premsg, "AutoResponse from ");
    strcat(premsg, aim_connection.sn);
    strcat(premsg, ": </font></html>");
    gtkfaim_html_insert(destwindow->textbox_conversation, premsg);
  }

  /* message */
  gtkfaim_html_insert(destwindow->textbox_conversation, msg);

  /* set insertion point to end of text box */
  gtk_text_set_point(GTK_TEXT(destwindow->textbox_conversation), gtk_text_get_length(GTK_TEXT(destwindow->textbox_conversation)));
  
  /* thaw the textbox events */
  gtk_text_thaw(GTK_TEXT(destwindow->textbox_conversation));

  /* Do the onoutmessage */
  if( gtkfaim_config.onoutmessage && !fork() )
  {
     /* Child */
     execlp( gtkfaim_config.onoutmessage, 
             /* FIXME: argv[0] should give pathname. */ 
             gtkfaim_config.onoutmessage, 
             destsn, msg, NULL );
     /* if we still exist, there must of been an error */
     perror( "gtkfaim: executing onoutmessage action" );
     exit( 1 );
  }
  return 0;
}

int gtkfaim_send_im_away(char *destsn)
{
  if (gtkfaim_config.awaymsg)
    {
      gtkfaim_send_im_away_putinbox(destsn, gtkfaim_config.awaymsg);
      aim_send_im_away(destsn, gtkfaim_config.awaymsg);
    }
  else
    {
      gtkfaim_send_im_away_putinbox(destsn, "I am away doing better things.");
      aim_send_im_away(destsn, "I am away doing better things. (at least thats what they think)");
    }

  return 0;
}

int
gtkfaim_send_im(char *destsn, char *msg)
{
  gtkfaim_imwindow_t *destwindow = NULL;
  char color[6];
  char color2[6];
  char date[25];

  /* get ptr to destination IM window */
  destwindow = gtkfaim_search_imwindow(destsn);
  
  /* freeze the textbox events */
  gtk_text_freeze(GTK_TEXT(destwindow->textbox_conversation));

  /* set insertion point to end of text box */
  gtk_text_set_point(GTK_TEXT(destwindow->textbox_conversation), gtk_text_get_length(GTK_TEXT(destwindow->textbox_conversation)));
  
  /* if we're not at the beginning, insert a CR */
  if (gtk_text_get_point(GTK_TEXT(destwindow->textbox_conversation)) != 0)
    gtk_text_insert(GTK_TEXT(destwindow->textbox_conversation), NULL, NULL, NULL, "\n", -1);
  
  /* put the message in the coversation box */

  sprintf(color, "%02x%02x%02x", gtkfaim_config.imsg_src_fgcolor.red, gtkfaim_config.imsg_src_fgcolor.green, gtkfaim_config.imsg_src_fgcolor.blue);
  sprintf(color2, "%02x%02x%02x", gtkfaim_config.imsg_src_bgcolor.red, gtkfaim_config.imsg_src_bgcolor.green, gtkfaim_config.imsg_src_bgcolor.blue);
 
  /* source name */
#if 0
  tmpchar = g_new(char, strlen("<html><font color=\"#\" back=\"#\">") + strlen(color) +strlen(color2) + strlen(aim_connection.sn) + strlen(": </font></html>") + 1);
  memcpy(&(tmpchar[0]), "<html><font color=\"#", 20);
  memcpy(&(tmpchar[20]), color, strlen(color));
  memcpy(&(tmpchar[26]), "\" back=\"#", 9);
  memcpy(&(tmpchar[35]), color2, strlen(color2));
  memcpy(&(tmpchar[41]), "\">", 2);
  memcpy(&(tmpchar[43]), aim_connection.sn, strlen(aim_connection.sn));
  memcpy(&(tmpchar[43+strlen(aim_connection.sn)]), ": </font></html>", 17);
  gtkfaim_html_insert(destwindow->textbox_conversation, tmpchar);
  g_free(tmpchar);
#endif

  {
    char premsg[255] = "<html><font color=\"#";
    strcat(premsg, color);
    strcat(premsg, "\" back=\"#");
    strcat(premsg, color2);
    strcat(premsg, "\">");
    if (gtkfaim_config.timestamps)
      {
	time_t timet;
	struct tm *t;

	time(&timet);
	t = localtime(&timet);
	sprintf(date, "[%02d:%02d:%02d] ", t->tm_hour, t->tm_min, t->tm_sec);
	strcat(premsg, date);
      }
    strcat(premsg, aim_connection.sn);
    strcat(premsg, ": </font></html>");
    gtkfaim_html_insert(destwindow->textbox_conversation, premsg);
  }

  /* message */
  gtkfaim_html_insert(destwindow->textbox_conversation, msg);

  /* set insertion point to end of text box */
  gtk_text_set_point(GTK_TEXT(destwindow->textbox_conversation), gtk_text_get_length(GTK_TEXT(destwindow->textbox_conversation)));
  
  /* thaw the textbox events */
  gtk_text_thaw(GTK_TEXT(destwindow->textbox_conversation));

  /* send the message for real */
  aim_send_im(destsn, msg);

  /* Do the onoutmessage */
  if( gtkfaim_config.onoutmessage && !fork() )
  {
     /* Child */
     execlp( gtkfaim_config.onoutmessage, 
             /* FIXME: argv[0] should give pathname. */ 
             gtkfaim_config.onoutmessage, destsn, msg, NULL );
     /* if we still exist, there must of been an error */
     perror( "gtkfaim: executing onoutmessage action" );
     exit( 1 );
  }
 
  return 0;
}

gtkfaim_imwindow_t *gtkfaim_imwindow_search_button_send(GtkWidget *criteria)
{
  gtkfaim_imwindow_t *tmpdata = NULL;
  gint listlen = g_list_length(gtkfaim_imwindowlist);
  gint i;
  
  for (i = 0; i < listlen; i++)
    {
      tmpdata = (gtkfaim_imwindow_t *)g_list_nth_data(gtkfaim_imwindowlist, i);
      if (tmpdata != NULL)
	{
	  if (tmpdata->button_send == criteria)
	    return tmpdata;
	}
    }

  return NULL;
}

gtkfaim_imwindow_t *gtkfaim_imwindow_search_button_close(GtkWidget *criteria)
{
  gtkfaim_imwindow_t *tmpdata = NULL;
  gint listlen = g_list_length(gtkfaim_imwindowlist);
  gint i;
  
  for (i = 0; i < listlen; i++)
    {
      tmpdata = (gtkfaim_imwindow_t *)g_list_nth_data(gtkfaim_imwindowlist, i);
      if (tmpdata != NULL)
	{
	  if (tmpdata->button_close == criteria)
	    return tmpdata;
	}
    }

  return NULL;
}

gtkfaim_imwindow_t *gtkfaim_imwindow_search_button_info(GtkWidget *criteria)
{
  gtkfaim_imwindow_t *tmpdata = NULL;
  gint listlen = g_list_length(gtkfaim_imwindowlist);
  gint i;
  
  for (i = 0; i < listlen; i++)
    {
      tmpdata = (gtkfaim_imwindow_t *)g_list_nth_data(gtkfaim_imwindowlist, i);
      if (tmpdata != NULL)
	{
	  if (tmpdata->button_info == criteria)
	    return tmpdata;
	}
    }

  return NULL;
}

void
gtkfaim_imwindow_close( GtkWidget *widget, gpointer data )
{
   gtkfaim_imwindow_t *window;

   /* figure out which window we're getting called for */
   if( (window = gtkfaim_imwindow_search_button_close( widget ) ) == NULL )
   {
      printf( "gtkfaim_imwindow_close(): caught a close signal for a noexistant window!\n" );
      return;
   }

   gtk_widget_hide( window->widgetptr );
   gtk_widget_destroy( window->widgetptr );
  
   /*
    * Remove buddy from temporary group, we call this for everyone because
    * gtkfaim_group_delete_buddy knows how to handle things even if the
    * buddy isn't really a "Temporary" buddy
    */
   if( gtkfaim_temporary_group )
      gtkfaim_group_delete_buddy( gtkfaim_temporary_group, window->buddy );
   gtkfaim_imwindowlist = g_list_remove( gtkfaim_imwindowlist, window );
}


void gtkfaim_free_imwindow(gpointer data, gpointer user)
{
  gtkfaim_imwindow_t *window;
  if (data != NULL)
    {
      window = (gtkfaim_imwindow_t *)data;
      gtk_widget_hide(window->widgetptr);
      gtk_widget_destroy(window->widgetptr);

      /* memory leaks? yep! they are aplenty, right here even... */

    }
}


/*
 * Debugging functions
 */
#if debug > 1
static void
GTKFAIM_imwindow_print2( gpointer data, gpointer user )
{
   if( data )
   {
      gtkfaim_imwindow_t *workingPtr2 = (gtkfaim_imwindow_t *)data;
      printf( "\t %1d ", workingPtr2->buddy->online );
      printf( "\t %s ", workingPtr2->buddy->sn );
      printf( "\n" );
   }
}

int
gtkfaim_imwindow_print( void )
{
   printf( "\n\nCurrent imwindow List:\n" );
  
   /* isn't glib neat... */
   g_list_foreach( gtkfaim_imwindowlist, GTKFAIM_imwindow_print2, NULL );

   printf( "\n\n" );

   return 0;
}
#endif


