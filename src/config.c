/************************************************************************\
 * gtkFAIM
 * -------
 *
 * Description: Configuration (.faimrc) reading/writing code
 *              Handles the fact that .faimrc can be shared across
 *              different FAIM implementations.
 *
\************************************************************************/

#include <aim.h>
#include <gtkfaim.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>

typedef struct conf_entry c_entry_t;

struct conf_entry
{
   char       *name;
   char       *group;
   gboolean    weuse;
   union
   {
      faim_conf_t *conf;
      char        *lines;
   }
   v;
   gboolean    matched;
   c_entry_t  *sibling;
   c_entry_t  *next;
};


static c_entry_t *conf_entries = NULL;
#define GLOBAL_GROUP "__GLOBAL__"

struct gtkfaim_config_struct gtkfaim_config;
#define FAIM_CONF_OFFSET(elem) (int)&gtkfaim_config. ## elem - (int)&gtkfaim_config

/*
 * Construct the comment and tuple structures
 */
#define FAIM_CONFIG_STRING(var,def,len,flg,com) static comment_t __ ## var ## comments[] = ## com
#define FAIM_CONFIG_BOOLEAN(var,def,flg,com) static comment_t __ ## var ## comments[] = ## com
#define FAIM_CONFIG_TUPLE(var,r_def,g_def,b_def,flg,com) static tuple_t __ ## var ## tuple = { r_def, g_def, b_def }; static comment_t __ ## var ## comments[] = ## com
#define FAIM_COMMENT_BEGIN {
#define FAIM_COMMENT_END NULL };
#define FAIM_COMMENT(str) str,
#include "keywords.h"
#undef FAIM_COMMENT
#undef FAIM_COMMENT_END
#undef FAIM_COMMENT_BEGIN
#undef FAIM_CONFIG_TUPLE
#undef FAIM_CONFIG_BOOLEAN
#undef FAIM_CONFIG_STRING


/*
 * Construct the configuration keyword structures
 */
#define FAIM_CONFIG_STRING(var,def,len,flg,com) { #var, FAIM_CONF_OFFSET(var), FAIM_CONF_TYPE_STRING, len, flg, (void *)def, __ ## var ## comments },
#define FAIM_CONFIG_BOOLEAN(var,def,flg,com) { #var,  FAIM_CONF_OFFSET(var), FAIM_CONF_TYPE_BOOLEAN, 1, flg, (void *)def, __ ## var ## comments },
#define FAIM_CONFIG_TUPLE(var,r_def,g_def,b_def,flg,com) { #var,  FAIM_CONF_OFFSET(var), FAIM_CONF_TYPE_TUPLE, 6, flg, (void *)&__ ## var ## tuple, __ ## var ## comments },
#define FAIM_COMMENT_BEGIN
#define FAIM_COMMENT_END
#define FAIM_COMMENT(str)
faim_conf_t faim_conf_keywords[] =
{
#include "keywords.h"
};
#undef FAIM_COMMENT
#undef FAIM_COMMENT_END
#undef FAIM_COMMENT_BEGIN
#undef FAIM_CONFIG_TUPLE
#undef FAIM_CONFIG_BOOLEAN
#undef FAIM_CONFIG_STRING

#define GTKFAIM_NUM_KEYWORDS (sizeof(faim_conf_keywords) / sizeof(faim_conf_t))


/*
 * Forward declarations
 */
char *gtkfaim_read_line ( FILE * );
void  gtkfaim_conf_store( char *, const char * );
void  gtkfaim_conf_flush( void );
void  gtkfaim_conf_save ( FILE *f, const char *, int, const char * );
void  readprofile_file  ( char * );


/*
 * Parsing macros
 */
#define EAT_WHITE(v) while( ( *(v) == ' ' ) || ( *(v) == '\t' ) ) v++
#define GET_WORD_DO(v,w,l,t) \
   do{ \
      char *m; int __quoted=0; \
      EAT_WHITE(v); \
      if((t)&&*v == '"') __quoted=1,v++; \
      for(w=m=v,l=0;*v&&(__quoted||((*v!=' ')&&(*v!='\t')));*m++=*v++,l++) { \
         int __esc=0; \
         if((t)&&(*v == '\\')){__esc=1;if(*(++v)=='\0')break;} \
         if(__quoted&&(*v == '"')&&!__esc) { v++; break; } \
      } \
      if(t){if(*v)v++;*m='\0';}\
   }while(0)
#define GET_WORD(v,w,l) GET_WORD_DO(v,w,l,TRUE)
#define GET_WORD1(v,w,l) GET_WORD_DO(v,w,l,FALSE)


/*
 * Name of loaded configuration file (used for saving)
 */
char *loaded_conffile = NULL;


/*
 * Find a configuration entry
 */
c_entry_t *
gtkfaim_find_conf_entry( const char *group, const char *name )
{
   c_entry_t *ceptr;

   for( ceptr = conf_entries; ceptr && strcmp( ceptr->group, group ); ceptr = ceptr->next );
   for( ; ceptr && strcmp( ceptr->name, name ); ceptr = ceptr->sibling );

   return ceptr;
}

#if 0
void gtkfaim_dump_conf_entries( void );
void
gtkfaim_dump_conf_entries()
{
   c_entry_t *ceptr, *sib_ceptr;

   for( ceptr = conf_entries; ceptr; ceptr = ceptr->next )
      for( sib_ceptr = ceptr; sib_ceptr; sib_ceptr = sib_ceptr->sibling )
      {
         printf( "---------------------------------------------\n" );
         printf( "  Group: %s\n   Name: %s\n", sib_ceptr->group, sib_ceptr->name );
         printf( " We use: %s\nMatched: %s\n", sib_ceptr->weuse?"true":"false", sib_ceptr->matched?"true":"false" );
         printf( "Sibling: %p\n   Next: %p\n", sib_ceptr->sibling, sib_ceptr->next );
      }

   printf( "---------------------------------------------\n" );
}
#endif


/*
 * Parse configuration file (also writes new file if *inconf is !NULL)
 */
int
gtkfaim_parse_config( char *fe, char **inconf )
{
   struct gtkfaim_config_struct *conf = &gtkfaim_config;
   char *realline;
   char *curline;
   char *word;
   char *lfe;
   int   lfelen;
   int   len;
   int   i;
   FILE *fp;
   FILE *ofp = NULL;
   const char *conffile = *inconf;

   /*
    * Do not write out configuration if flagged read-only
    */
   if( conffile && conf->readonly )
      return;

   if( conffile )
   {
      /*
       *Save old faimrc in backup file (used for reading)
       * Open new faimrc with same name as loaded config (used for writing)
       */
      char *temp = g_new( char, strlen( conffile ) + sizeof( ".bak" ) + 1 );
      sprintf( temp, "%s.bak", conffile );
      unlink( temp );
      if( rename( conffile, temp ) < 0 )
      {
         GTKFAIM_DEBUG( "conf", "error renaming old conf:", FALSE );
         perror( NULL );
         return 0;
      }
      else if( ( ( ofp = fopen( conffile, "w" ) ) == NULL ) ||
               ( ( fp = fopen( temp, "r" ) ) == NULL ) )
      {
         GTKFAIM_DEBUG( "conf", "error writing conf:", FALSE );
         perror( NULL );

         if( ofp )
         {
            fclose( ofp );
            unlink( conffile );
         }
         rename( temp, conffile );
         return 0;
      }

      /*
       * Write out tagline
       */
      fprintf( ofp, "#\n" );
      fprintf( ofp, "# Automatically generated by gtkFAIM v0x%02x.%02x%s\n",
               GTKFAIM_MAJOR, GTKFAIM_MINOR, GTKFAIM_PATCHLEVEL );
      fprintf( ofp, "#\n\n" );
   }
   else
   {
      /* Try to open config file in current directory */
      if( ( fp = fopen( "faimrc", "r" ) ) == NULL )
      {
         /* construct the full path of the config file */
         char *homedir = getenv( "HOME");
         *inconf = g_new( char, strlen( homedir ) + strlen( "/.faimrc" ) + 1 );
         memcpy( *inconf, homedir, strlen( homedir ) + 1 );
         strcat( *inconf, "/.faimrc" );

         /* open the file read-only */
#if debug > 0
         GTKFAIM_DEBUG2( "gtkfaim_read_config()", "opening ", *inconf, TRUE );
#endif
         if( ( fp = fopen( *inconf, "r" ) ) == NULL )
         {
            GTKFAIM_DEBUG( "config", "could not open ~/.faimrc:", FALSE );
            perror( NULL );
            GTKFAIM_DEBUG( "config", "assuming defaults", TRUE );
            g_free( *inconf );
            *inconf = NULL;
            return 0;
         }
      }
      else
         *inconf = "faimrc";
      conffile = *inconf;
   }

   for( i = 0; i < GTKFAIM_NUM_KEYWORDS; i++ )
   {
      char *mem = (char *)conf + faim_conf_keywords[i].offset;
      void *def = faim_conf_keywords[i].deflt;

      if( ofp )
      {
         /*
          * Set up configuration entry
          */
         c_entry_t *ceptr;
         c_entry_t *newent = g_new( c_entry_t, 1 );

         bzero( newent, sizeof( *newent ) );
         if( faim_conf_keywords[i].flags & GTKFAIM_LOCAL )
            newent->group = fe;
         else
            newent->group = GLOBAL_GROUP;
         newent->name = (char *)faim_conf_keywords[i].name;
         newent->weuse = TRUE;
         newent->v.conf = &faim_conf_keywords[i];

         for( ceptr = conf_entries; ceptr && ( ceptr->group != newent->group ) && ceptr->next; ceptr = ceptr->next );
         if( ceptr && ( ceptr->group == newent->group ) )
         {
            for( ; ceptr && ceptr->sibling; ceptr = ceptr->sibling );
            ceptr->sibling = newent;
         }
         else if( ceptr )
            ceptr->next = newent;
         else
            conf_entries = newent;
      }
      else
      {
         /*
          * Initialize configuration structure with defaults
          */
         switch( faim_conf_keywords[i].type )
         {
            case FAIM_CONF_TYPE_STRING:
               if( def )
               {
                  *((char **)mem) = g_new( gchar, strlen( (char *)def ) + 1 );
                  memcpy( *((char **)mem), def, strlen( (char *)def ) + 1 );
               }
               else
                  *((char **)mem) = NULL;
               break;
            case FAIM_CONF_TYPE_BOOLEAN:
               *(gboolean *)mem = (gboolean)def;
               break;
            case FAIM_CONF_TYPE_TUPLE:
               *(tuple_t *)mem = *(tuple_t *)def;
               break;
         }
      }
   }

   while( ( realline = curline = gtkfaim_read_line( fp ) ) != NULL )
   {
      EAT_WHITE( curline );

      /*
       * Process comments and blank lines
       */
      if( ( curline[0] == '#' ) || ( curline[0] == '\0' ) )
      {
         if( ofp ) gtkfaim_conf_store( realline, "" );
         goto nextline;
      }

      /*
       * Determine client-specific configuration variables
       */
      if( strchr( curline, '.' ) &&
          ( strchr( curline, '.' ) < strchr( curline, ' ' ) ) )
      {
         lfe = curline;
         curline = strchr( curline, '.' ) + 1;

         if( strncmp( lfe, fe, curline - lfe - 1 ) )
         {
            GET_WORD1( curline, word, len );

            /* NOT equiv */
            if( ofp )
            {
               gtkfaim_conf_store( realline, "" );
               gtkfaim_conf_save( ofp, lfe, lfelen, word );
            }
            goto nextline;
         }
         lfe = fe;
      }
      else
         lfe = GLOBAL_GROUP;

      /*
       * Process configuration variable
       */
      GET_WORD1( curline, word, len );
      if( !strncmp( word, "buddy", len ) )
      {
         /*
          * Buddylist entries are a special case
          */
         if( !ofp )
         {
            gtkfaim_group_t *groupPtr;
            gtkfaim_buddy_t *buddyPtr;
            char            *buddy;
            char            *group;
            char            *alias;

            GET_WORD( curline, buddy, len );
            GET_WORD( curline, group, len );
            GET_WORD( curline, alias, len );

            groupPtr = gtkfaim_group_new( *group?group:"Buddies" );
            buddyPtr = gtkfaim_buddy_new( buddy, *alias?alias:NULL );
            gtkfaim_group_add_buddy( groupPtr, buddyPtr );
         }
         goto flushnext;
      }
      else
      {
         /*
          * Check for valid keyword
          */
         for( i = 0; i < GTKFAIM_NUM_KEYWORDS; i++ )
         {
            if( !strncmp( word, faim_conf_keywords[i].name, len ) )
            {
               if( ( lfe == GLOBAL_GROUP ) &&
                   ( faim_conf_keywords[i].flags & GTKFAIM_LOCAL ) )
                  goto badkeyword;

               if( ofp )
               {
                  /*
                   * Write out configuration entry
                   */
                  int j;
                  char *mem = (char *)conf + faim_conf_keywords[i].offset;
                  c_entry_t *ceptr = gtkfaim_find_conf_entry( lfe, faim_conf_keywords[i].name );

                  ceptr->matched = TRUE;

                  for( j = 0; faim_conf_keywords[i].comments[j]; j++ )
                     fprintf( ofp, "%s\n", faim_conf_keywords[i].comments[j] );
         
                  switch( faim_conf_keywords[i].type )
                  {
                     case FAIM_CONF_TYPE_STRING:
                     {
                        char *arg = *((char **)mem);

                        if( arg )
                        {
                           if( faim_conf_keywords[i].flags & GTKFAIM_LOCAL )
                              fprintf( ofp, "%s.", fe );

                           if( strchr( arg, ' ' ) || strchr( arg, '\t' ) )
                              fprintf( ofp, "%s \"%s\"\n", faim_conf_keywords[i].name, arg );
                           else
                              fprintf( ofp, "%s %s\n", faim_conf_keywords[i].name, arg );
                        }
                        else
                        {
                           if( faim_conf_keywords[i].flags & GTKFAIM_LOCAL )
                              fprintf( ofp, "#%s.%s <value>\n", fe, faim_conf_keywords[i].name );
                           else
                              fprintf( ofp, "#%s <value>\n", faim_conf_keywords[i].name );
                        }

                        break;
                     }
                     case FAIM_CONF_TYPE_BOOLEAN:
                     {
                        if( faim_conf_keywords[i].flags & GTKFAIM_LOCAL )
                           fprintf( ofp, "%s.", fe );

                        fprintf( ofp, "%s %d\n", faim_conf_keywords[i].name, *((gboolean *)mem) );
                        break;
                     }
                     case FAIM_CONF_TYPE_TUPLE:
                     {
                        tuple_t arg = *((tuple_t *)mem);

                        if( faim_conf_keywords[i].flags & GTKFAIM_LOCAL )
                           fprintf( ofp, "%s.", fe );

                        fprintf( ofp, "%s %2.2x%2.2x%2.2x\n", faim_conf_keywords[i].name, arg.red, arg.green, arg.blue );
                        break;
                     }
                  }
               }
               else
               {
                  /*
                   * Set entry value in configuration structure
                   */
                  char *mem = (char *)conf + faim_conf_keywords[i].offset;

                  switch( faim_conf_keywords[i].type )
                  {
                     case FAIM_CONF_TYPE_STRING:
                     {
                        char *arg;

                        GET_WORD( curline, arg, len );
                        if( *arg )
                        {
                           if( faim_conf_keywords[i].len && ( len > faim_conf_keywords[i].len ) )
                           {
                              *((char **)mem) = g_new( gchar, faim_conf_keywords[i].len + 1 );
                              strncpy( *((char **)mem), arg, faim_conf_keywords[i].len );
                           }
                           else
                           {
                              *((char **)mem) = g_new( gchar, strlen( arg ) + 1 );
                              memcpy( *((char **)mem), arg, strlen( arg ) + 1 );
                           }
                        }
                        else
                        {
                           GTKFAIM_DEBUG( "config", "warning: missing string argument", TRUE );
                        }
                        break;
                     }
                     case FAIM_CONF_TYPE_BOOLEAN:
                     {
                        int arg;

                        EAT_WHITE( curline );

                        arg = atoi( curline );
                        if( ( ( arg == 0 ) && ( *curline != '0' ) ) ||
                            ( arg < 0 ) || ( arg > 1 ) )
                        {
                           GTKFAIM_DEBUG( "config", "warning: invalid boolean argument", TRUE );
                        }
                        else
                           *(gboolean *)mem = (gboolean)arg;
                        break;
                     }
                     case FAIM_CONF_TYPE_TUPLE:
                     {
                        tuple_t arg;

                        if( sscanf( curline, "%02x%02x%02x", &arg.red, &arg.green, &arg.blue ) < 3 )
                        {
                           GTKFAIM_DEBUG( "config", "warning: error in tuple specification", TRUE );
                        }
                        else
                           *(tuple_t *)mem = arg;
                        break;
                     }
                  }
               }

               goto flushnext;
            }
         }
      }

badkeyword:
      /*
       * Invalid keyword encountered
       */
      GTKFAIM_DEBUG2( "config", "warning: unknown action in .faimrc: ", realline, TRUE );
      if( ofp )
      {
         gtkfaim_conf_store( realline, "#" );
         gtkfaim_conf_save( ofp, lfe, lfelen, word );
         GTKFAIM_DEBUG("config", "\tcommenting out.", TRUE);
      }
      else
         GTKFAIM_DEBUG("config", "\tignoring.", TRUE);
       
flushnext:
      /*
       * Flush saved configuration lines
       */
      gtkfaim_conf_flush();

nextline:
      /*
       * Free memory used by line
       */
      g_free( realline );
   }

   /*
    * Do post-processing work
    */
   if( !ofp )
   {
      /*
       * Read profile, if specified
       */
      if( conf->profile )
         readprofile_file( conf->profile );
   }
   else
   {
      /*
       * Write out remaining configuration options we use
       */
      c_entry_t *ceptr, *sib_ceptr;

#if 0
      gtkfaim_dump_conf_entries();
#endif

      for( ceptr = conf_entries; ceptr; ceptr = ceptr->next )
         for( sib_ceptr = ceptr; sib_ceptr; sib_ceptr = sib_ceptr->sibling )
            if( sib_ceptr->weuse && !sib_ceptr->matched )
            {
               char *mem = (char *)conf + sib_ceptr->v.conf->offset;
               int   j;

               for( j = 0; sib_ceptr->v.conf->comments[j]; j++ )
                     fprintf( ofp, "%s\n", sib_ceptr->v.conf->comments[j] );
         
               switch( sib_ceptr->v.conf->type )
               {
                  case FAIM_CONF_TYPE_STRING:
                  {
                     char *arg = *((char **)mem);

                     if( arg )
                     {
                        if( sib_ceptr->v.conf->flags & GTKFAIM_LOCAL )
                           fprintf( ofp, "%s.", sib_ceptr->group );

                        if( strchr( arg, ' ' ) || strchr( arg, '\t' ) )
                           fprintf( ofp, "%s \"%s\"\n", sib_ceptr->name, arg );
                        else
                           fprintf( ofp, "%s %s\n", sib_ceptr->name, arg );
                     }
                     else
                     {
                        if( sib_ceptr->v.conf->flags & GTKFAIM_LOCAL )
                           fprintf( ofp, "#%s.%s <value>\n", sib_ceptr->group, sib_ceptr->name );
                        else
                           fprintf( ofp, "#%s <value>\n", sib_ceptr->name );
                     }
                     break;
                  }
                  case FAIM_CONF_TYPE_BOOLEAN:
                     if( sib_ceptr->v.conf->flags & GTKFAIM_LOCAL )
                        fprintf( ofp, "%s.", sib_ceptr->group );

                     fprintf( ofp, "%s %d\n", sib_ceptr->name, *((gboolean *)mem) );
                     break;
                  case FAIM_CONF_TYPE_TUPLE:
                  {
                     tuple_t arg = *((tuple_t *)mem);

                     if( sib_ceptr->v.conf->flags & GTKFAIM_LOCAL )
                        fprintf( ofp, "%s.", sib_ceptr->group );

                     fprintf( ofp, "%s %2.2x%2.2x%2.2x\n", sib_ceptr->name, arg.red, arg.green, arg.blue );
                     break;
                  }
               }
            }

      /*
       * Write out buddy list
       */
      fprintf( ofp, "#\n" );
      fprintf( ofp, "# buddy list format:\n" );
      fprintf( ofp, "#  buddy <buddy sn> [[<buddy group>] [<realname>]]\n" );
      fprintf( ofp, "#    <buddy sn> is the screen name of the buddy\n" );
      fprintf( ofp, "#    <buddy group> is the group they belong in\n" );
      fprintf( ofp, "#    <realname> is an optional alias (Real Name)\n" );
      fprintf( ofp, "#   <buddy group> and <realname> are optional\n" );
      fprintf( ofp, "#   <realname> can only be present if <buddy group> is specified\n" );

      {
         gtkfaim_group_t *group;
	 gint             curnode;

         for( curnode = 0;
              ( group = (gtkfaim_group_t *)g_list_nth_data( gtkfaim_groups, curnode ) );
              curnode++ )
         {
            if( group != gtkfaim_temporary_group )
            {
               gtkfaim_buddylist_node_t *node;
               gint                      buddynode;

               for( buddynode = 0;
                    ( node = (gtkfaim_buddylist_node_t *)g_list_nth_data( group->buddies, buddynode ) );
                    buddynode++ )
               {
                  fprintf( ofp, "buddy \"%s\" \"%s\"", node->buddy->sn, group->name );
                  if( node->buddy->alias )
                     fprintf( ofp, " \"%s\"", node->buddy->alias );
                  fprintf( ofp, "\n" );
               }
            }
         }
      }

      /*
       * Free memory used by configuration entries
       */
      for( ceptr = conf_entries; ceptr; )
      {
         c_entry_t *next_ceptr = ceptr->next;
         for( sib_ceptr = ceptr; sib_ceptr; )
         {
            c_entry_t *next_sib_ceptr = sib_ceptr->sibling;

            if( !sib_ceptr->weuse )
               g_free( sib_ceptr->v.lines );

            if( ( sib_ceptr->group != GLOBAL_GROUP ) &&
                ( sib_ceptr->group != fe ) &&
                ( next_sib_ceptr == NULL ) )
               g_free( sib_ceptr->group );

            if( !sib_ceptr->weuse )
               g_free( sib_ceptr->name );

            g_free( sib_ceptr );

            sib_ceptr = next_sib_ceptr;
         }
         ceptr = next_ceptr;
      }
      conf_entries = NULL;
      fclose( ofp );
   }

   return 0;
}


static char *stored_conf_lines = NULL;
static int stored_size = 0;

/*
 * Store line for possible use later
 */
void
gtkfaim_conf_store( char *str, const char *pre )
{
   int size = stored_size + (stored_size?0:1) + strlen( pre ) + strlen( str ) + 1;
   if( stored_size )
      stored_conf_lines = (char *)realloc( stored_conf_lines, size );
   else
      stored_conf_lines = (char *)malloc( size );
   sprintf( stored_conf_lines + stored_size - (stored_size?1:0), "%s%s\n", pre, str );
   stored_size = size;
}

/*
 * Save stored lines to configuration entry and output file
 *
 * Note: Saving the lines as a configuration entry is not necessary, but
 *       can help when debugging conf reading/writing problems when used
 *       with gtkfaim_dump_conf_entries().
 */
void
gtkfaim_conf_save( FILE *fp, const char *group, int len, const char *name )
{
   c_entry_t *ceptr;
   c_entry_t *newent = g_new( c_entry_t, 1 );
   bzero( newent, sizeof( *newent ) );

   for( ceptr = conf_entries; ceptr && strcmp( ceptr->group, group ) && ceptr->next; ceptr = ceptr->next );
   if( ceptr && !strcmp( ceptr->group, group ) )
   {
      newent->group = ceptr->group;
      ceptr->sibling = newent;
   }
   else
   {
      newent->group = g_new( char, strlen( group ) + 1 );
      memcpy( newent->group, group, strlen( group ) + 1 );
      if( ceptr )
         ceptr->next = newent;
      else
         conf_entries = newent;
   }
      
   newent->name = g_new( char, strlen( name ) + 1 );
   memcpy( newent->name, name, strlen( name ) + 1 );

   newent->v.lines = stored_conf_lines;

   stored_conf_lines = NULL;
   stored_size = 0;

   fprintf( fp, newent->v.lines );
}

/*
 * Free memory used by stored lines
 */
void
gtkfaim_conf_flush()
{
   if( stored_size )
   {
      free( stored_conf_lines );
      stored_size = 0;
   }
}

/*
 * Read a line from the input file (stop at line-feed)
 */
char *
gtkfaim_read_line( FILE *fileptr )
{
   int i = 0;
   char tmpchar;
#define CONF_MAXLINE_LEN 256
   char *newbuf;
  
   newbuf = g_new( char, CONF_MAXLINE_LEN );

   for( i = 0; i < (CONF_MAXLINE_LEN - 1); )
   {
      tmpchar = getc(fileptr);
      if( tmpchar == -1 )
         break;
      newbuf[i++] = tmpchar;
      if( tmpchar == '\n' )
         break;
   }
   if( i == 0 )
   {
      g_free( newbuf );
      return NULL;
   }
  
   newbuf[i-1] = '\0';
  
   return newbuf;
}

/*
 * This will read a file into faim_config.profile_string
 */
void
readprofile_file( char *filename )
{
   struct stat st;
   int fd;
   char *newprof = NULL;

   if( ( fd = open( filename, O_RDONLY ) ) < 0 )
   {
      GTKFAIM_DEBUG2( "profile", "error opening profile file:", filename, TRUE );
      return;
   }

   if( fstat( fd, &st ) < 0 )
   {
      GTKFAIM_DEBUG2( "profile", "error getting stat for profile file:", filename, TRUE );
      return;
   }

   newprof = (char *)malloc( st.st_size + 1 );
   if( read( fd, newprof, st.st_size ) < st.st_size )
   {
      GTKFAIM_DEBUG( "profile", "error reading profile:", FALSE );
      perror( NULL );
   }

   newprof[st.st_size] = '\0';

   close( fd );

   gtkfaim_config.profile_string = newprof;

   return;
}

/*
  Tries to load a gtkrc in the following order:
    - the one specified by the "gtkrc" line in faimrc
    - ~/.gtkrc
    - /usr/local/etc/gtkrc
    - /usr/etc/gtkrc
    - fail

 */
void
gtkfaim_loadgtkrc()
{
   struct stat st;

   if( ( gtkfaim_config.gtkrc != NULL ) &&
       ( stat( gtkfaim_config.gtkrc, &st ) == 0 ) )
      gtk_rc_parse( gtkfaim_config.gtkrc );
   else
   {
      char *homedir, *conffile;
      
      homedir = getenv( "HOME" );
      conffile = g_new( char, strlen( homedir ) + strlen( "/.gtkrc" ) + 1 );
      memcpy( conffile, homedir, strlen( homedir ) + 1 );
      strcat( conffile, "/.gtkrc" );
      
      if( stat( conffile, &st ) == 0 )
         gtk_rc_parse(conffile);
      else if( stat( "/usr/local/etc/gtkrc", &st ) == 0 )
         gtk_rc_parse( "/usr/local/etc/gtkrc" );
      else if( stat( "/usr/etc/gtkrc", &st ) == 0 )
         gtk_rc_parse( "/usr/etc/gtkrc" );
   }
}

