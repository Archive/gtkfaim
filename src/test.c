
#include <stdio.h>

#define TRUE 1
#define FALSE 0

typedef char* comment_t;

typedef struct
{
   unsigned short red;
   unsigned short green;
   unsigned short blue;
}
tuple_t;

typedef enum
{
   FAIM_CONF_TYPE_STRING,
   FAIM_CONF_TYPE_BOOLEAN,
   FAIM_CONF_TYPE_TUPLE
}
conf_type_t;

typedef struct
{
   const char  *name;
   conf_type_t  type;
   void        *def;
   comment_t   *comments;
}
conf_t;

#define FAIM_CONFIG_STRING(var,def,com) comment_t var ## comments[] = ## com
#define FAIM_CONFIG_BOOLEAN(var,def,com) comment_t var ## comments[] = ## com
#define FAIM_CONFIG_TUPLE(var,r_def,g_def,b_def,com) tuple_t var ## tuple = { r_def, g_def, b_def }; comment_t var ## comments[] = ## com
#define FAIM_COMMENT_BEGIN {
#define FAIM_COMMENT_END };
#define FAIM_COMMENT(str) str,
#include "loader.h"
#undef FAIM_COMMENT
#undef FAIM_COMMENT_END
#undef FAIM_COMMENT_BEGIN
#undef FAIM_CONFIG_TUPLE
#undef FAIM_CONFIG_BOOLEAN
#undef FAIM_CONFIG_STRING

#define FAIM_CONFIG_STRING(var,def,com) { #var, FAIM_CONF_TYPE_STRING, (void *)#def, var ## comments },
#define FAIM_CONFIG_BOOLEAN(var,def,com) { #var, FAIM_CONF_TYPE_BOOLEAN, (void *)def, var ## comments },
#define FAIM_CONFIG_TUPLE(var,r_def,g_def,b_def,com) { #var, FAIM_CONF_TYPE_TUPLE, (void *)&var ## tuple, var ## comments },
#define FAIM_COMMENT_BEGIN
#define FAIM_COMMENT_END
#define FAIM_COMMENT(str)
conf_t config[] =
{
#include "loader.h"
};
#undef FAIM_COMMENT
#undef FAIM_COMMENT_END
#undef FAIM_COMMENT_BEGIN
#undef FAIM_CONFIG_TUPLE
#undef FAIM_CONFIG_BOOLEAN
#undef FAIM_CONFIG_STRING

int main( int argc, char *argv[] )
{
   return 0;
}

