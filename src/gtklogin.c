/* 
 * Started Aug 16 1998 by James Mastros.
 *   Moving functions from gtkfaim.c
 * 
 * TODO: 
 * (In roughly order of probable implementation.)
 * Show status during login
 * Add support for saving username, and (optionaly) password.
 * Add multiple login support.
 * Add other protocol support.
 */

#include <aim.h>
#include <errno.h>
#include <signal.h>
#include <gtkfaim.h>
#include <gdk/gdk.h>
#include <gdk/gdkprivate.h> /* for the event box/logo resizing hack */

struct gtkfaim_window_login_struct *gtkfaim_loginwindow = NULL;

int gtkfaim_parse_login_phase3d_f(struct command_rx_struct *command);

void gtkfaim_login_quit_click(GtkWidget *widget, gpointer data);
void gtkfaim_login_login_click(GtkWidget *widget, gpointer data);

sig_t gtkfaim_old_sigint_handler = NULL;

struct gtkfaim_window_login_struct
{
   GtkWidget *window;
   GtkWidget *vbox1; /* main vbox */
   GtkWidget *vbox2; /* top half */
   GtkWidget *vbox3; /* bottom half */
   GtkWidget *hbox1; /* top half of window */
   GtkWidget *hbox2; /* bottom half */
   GtkWidget *hbox3; /* sn: line */
   GtkWidget *hbox4; /* passwd: line */
   GtkWidget *hbox5; /* buttons */
   GtkWidget *banner_logo;
   GtkWidget *banner_label;
   GtkWidget *label_sn;
   GtkWidget *label_pwd;
   GtkWidget *entry_sn;
   GtkWidget *entry_pwd;
   GtkWidget *button_login;
   GtkWidget *button_quit;
};

void gtkfaim_sigint_signoff(void)
{
   gtkfaim_logoff();
   gtkfaim_write_config();
   gtk_main_quit();
   exit( 1 );
}

void gtkfaim_login_close(void) {
#ifndef CLOSEBROKE
    if (gtkfaim_loginwindow == NULL)
	GTKFAIM_DEBUG("login", "\a\aWARNING: gtkfaim_loginwindow = NULL!!!", TRUE);
    /* See refcounting.txt -- all the child windows will be
     * automaticly killed. */
    /* gtkfaim_loginwindow->window seems to be invalid here? */
    gtk_widget_destroy(gtkfaim_loginwindow->window);
    
    /* GTK should g_free itself when the refcount reaches zero. */
    /* g_free(gtkfaim_loginwindow); */
#else
    GTKFAIM_DEBUG("login", "login_close: compiled with CLOSEBROKE -- skipping all this stuff! \aBEEP", TRUE);
#endif
    gtkfaim_loginwindow = NULL;
    
    return;
}

void gtkfaim_login_quit_click(GtkWidget *widget, gpointer data)
{
  gtkfaim_login_close();
  gtkfaim_destroy_event(widget, data);
  return;
}

/* this is extremely funky because of what I THINK is a bug in GTK+ */
void gtkfaim_login_login_click(GtkWidget *widget, gpointer data)
{
  char *tmpPtr = NULL;
  char *sn;
  char *passwd; /* what's the real limit here?? */
  int ok = 1;

  sn = g_new(char,11);
  passwd = g_new(char,16);

  sn[0] = '\0';
  passwd[0] = '\0';

  tmpPtr = gtk_entry_get_text(GTK_ENTRY(gtkfaim_loginwindow->entry_sn));
  if ( (strlen(tmpPtr) >= 4) &&
       (tmpPtr[0] >= 0x41) &&
       (tmpPtr[0] <= 0x7a) )
    memcpy(sn, tmpPtr, strlen(tmpPtr)+1);
  else
    ok = 0;
  /* g_free(tmpPtr); */

  tmpPtr = gtk_entry_get_text(GTK_ENTRY(gtkfaim_loginwindow->entry_pwd));
  if (strlen(tmpPtr) >= 4)
    memcpy(passwd, tmpPtr, strlen(tmpPtr)+1);
  else
    ok = 0;
  
  /* g_free(tmpPtr); */
#if debug > 2
  GTKFAIM_DEBUG2("login", "sn: ", sn, TRUE);
  GTKFAIM_DEBUG2("login", "pw: ", passwd, TRUE);
#endif

   if( ok == 1 )
   { 
      int ret;

      g_free( gtkfaim_config.screenname );
      g_free( gtkfaim_config.password );
      gtkfaim_config.screenname = sn;
      gtkfaim_config.password = passwd;
      gtkfaim_away = 0;

      if( ( ret = aim_login( gtkfaim_config.screenname, gtkfaim_config.password ) ) != 0 )
      {
         GTKFAIM_DEBUG( "login", "**There were fatal errors during phase 1 of login!**", TRUE );

         switch( ret )
         {
            case AIM_CONNECT_ERROR:
            {
               const char *premsg = "Unable to connect to host: ";
               char *msg = g_new( char, strlen( premsg ) + strlen( strerror( errno ) ) + 1 );

               GTKFAIM_DEBUG2( "login",  "Connect error: ", strerror( errno ), TRUE );
               sprintf( msg, "%s%s", premsg, strerror( errno ) );
               gtkfaim_genericok( msg );
               g_free( msg );

               break;
            }
            case AIM_SIGNON_TOO_SOON:
               GTKFAIM_DEBUG( "login", "Attempted to signon too soon.", TRUE );
               gtkfaim_genericok( "Attempted to signon too soon." );
               break;
            case AIM_SERVICE_FULL:
               GTKFAIM_DEBUG( "login", "Service full.", TRUE );
               gtkfaim_genericok( "Service is currently full." );
               break;
            default:
               GTKFAIM_DEBUG( "login", "Error during authentication.", TRUE );
               gtkfaim_genericok( "Error during authentication." );
               exit( 1 );
         }
         return;
      }

      /* setup the callback for select() equivelent */
      gdk_input_add( aim_connection.fd, GDK_INPUT_READ, datawaiting_callback, 0);

      gtkfaim_login_close();

      g_free( gtkfaim_config.screenname );
      gtkfaim_config.screenname = aim_connection.sn;

   }
   else
   {
      printf("\a\nUsername and password must be at least 4 characters!\n\n");
      gtkfaim_genericok("Username and password must be at least 4 characters!");
   }
}

void
gtkfaim_genericok_click_signaled(void)
{
   gtk_main_quit();
}

int gtkfaim_autologin(void)
{
   int ret;

   if( !( ( strlen( gtkfaim_config.screenname ) >= 4 ) &&
          ( gtkfaim_config.screenname[0] >= 0x41 ) &&
          ( gtkfaim_config.screenname[0] <= 0x7a ) ) )
   {
      GTKFAIM_DEBUG( "login", "autologin: invalid screen name", TRUE );
      return -1;
   }

   if( !( strlen( gtkfaim_config.password ) >= 4 ) )
   {
      GTKFAIM_DEBUG( "login", "autologin: invalid password", TRUE );
      return -1;
   }

   if( ( ret = aim_login( gtkfaim_config.screenname, gtkfaim_config.password ) ) != 0 )
   {
      GTKFAIM_DEBUG("login", "**There were fatal errors during phase 1 of login!**", TRUE);

      switch( ret )
      {
         case AIM_CONNECT_ERROR:
         {
            const char *premsg = "Unable to connect to host: ";
            char *msg = g_new( char, strlen( premsg ) + strlen( strerror( errno ) ) + 1 );

            GTKFAIM_DEBUG2( "login",  "Connect error: ", strerror( errno ), TRUE );
            sprintf( msg, "%s%s", premsg, strerror( errno ) );
            gtkfaim_genericok_signal( msg, GTK_SIGNAL_FUNC( gtkfaim_genericok_click_signaled ) );
            g_free( msg );

            break;
         }
         case AIM_SIGNON_TOO_SOON:
            GTKFAIM_DEBUG( "login", "Attempted to signon too soon.", TRUE );
            gtkfaim_genericok_signal( "Attempted to signon too soon.", GTK_SIGNAL_FUNC( gtkfaim_genericok_click_signaled ) );
            break;
         case AIM_SERVICE_FULL:
            GTKFAIM_DEBUG( "login", "Service full.", TRUE );
            gtkfaim_genericok_signal( "Service is currently full.", GTK_SIGNAL_FUNC( gtkfaim_genericok_click_signaled ) );
            break;
         default:
            GTKFAIM_DEBUG( "login", "Error during authentication.", TRUE );
            gtkfaim_genericok_signal( "Error during authentication.", GTK_SIGNAL_FUNC( gtkfaim_genericok_click_signaled ) );
            exit( 1 );
      }

      gtk_main();

      return -1;
   }

   /* setup signal handlers to cleanly disconnect us */
   gtkfaim_old_sigint_handler = signal( SIGINT, (sig_t)gtkfaim_sigint_signoff );

   /* setup the callback for select() equivelent */
   gdk_input_add( aim_connection.fd, GDK_INPUT_READ, datawaiting_callback, 0 );
  
   g_free( gtkfaim_config.screenname );
   gtkfaim_config.screenname = aim_connection.sn;

   return 0;
}

/* Main entry point to this file. */
void
gtkfaim_create_loginwindow(void)
{
  char name[25] = "gtkFAIM ";
  struct gtkfaim_window_login_struct *loginwindow = NULL;
  GtkWidget *logobox;

  sprintf(&(name[8]), "v0x%02x.%02x%s", GTKFAIM_MAJOR, GTKFAIM_MINOR, GTKFAIM_PATCHLEVEL);

  loginwindow = g_new(struct gtkfaim_window_login_struct,1);

  if (loginwindow == NULL)
    GTKFAIM_DEBUG("login", "loginwindow struct was not created", TRUE);
#ifdef GNOME
  loginwindow->window = gnome_app_new(_("gtkFAIM"), _("gtkfaim_login"));
#else
  loginwindow->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  if (loginwindow->window == NULL)
    GTKFAIM_DEBUG("login", "loginwindow was not created", TRUE);
  gtk_widget_set_name(GTK_WIDGET(loginwindow->window), "gtkfaim_login");
#endif
  gtk_window_position(GTK_WINDOW(loginwindow->window), GTK_WIN_POS_CENTER);
#if 0
  gtk_widget_set_usize(loginwindow->window, 200, 300);
#endif
  gtk_window_set_title(GTK_WINDOW(loginwindow->window), "gtkFAIM Login");
  loginwindow->vbox2 = gtk_vbox_new(FALSE, 0);
  loginwindow->vbox3 = gtk_vbox_new(FALSE, 0);
  loginwindow->hbox1 = gtk_hbox_new(TRUE, 0);
  loginwindow->hbox2 = gtk_hbox_new(TRUE, 0);
  loginwindow->hbox3 = gtk_hbox_new(TRUE, 1);
  loginwindow->hbox4 = gtk_hbox_new(TRUE, 1);
  loginwindow->hbox5 = gtk_hbox_new(FALSE, 0);
  
  logobox = gtk_event_box_new();
  gtk_widget_set_name(GTK_WIDGET(logobox), "gtkfaim_logo");
  gtk_widget_show(logobox);
  
  {
    /* 
       So you want to know what this does...
         Well, first, it figures out if your're loading a logo or not and
	 sets the window size if you're not.  If you are, it gets it's width
	 and height, resizes the event box and then assumes the window will
	 resize itself.  Neat, eh?

	 The packing parameter for vbox1 also depends on this!
     */
    int height,width;
    if (GTK_WIDGET(logobox)->style->bg_pixmap[0])
      {
	height =(((GdkWindowPrivate *)(GTK_WIDGET(logobox)->style->bg_pixmap[0]))->height);
	width = (((GdkWindowPrivate *)(GTK_WIDGET(logobox)->style->bg_pixmap[0]))->width);
	gtk_widget_set_usize(logobox, width, height);
	loginwindow->vbox1 = gtk_vbox_new(FALSE, 2);
      }
    else
      {
	gtk_widget_set_usize(loginwindow->window, 200, 300);
	loginwindow->vbox1 = gtk_vbox_new(TRUE, 2);
      }
    
  }

  loginwindow->banner_label = gtk_label_new(name);
  loginwindow->label_sn = gtk_label_new("Screen Name:");
  loginwindow->label_pwd = gtk_label_new("Password:");
  loginwindow->entry_sn = gtk_entry_new_with_max_length(10);
  if (gtkfaim_config.screenname != NULL)
    gtk_entry_set_text(GTK_ENTRY(loginwindow->entry_sn), gtkfaim_config.screenname);
  
  loginwindow->button_login = gtk_button_new_with_label("Login");
  loginwindow->button_quit = gtk_button_new_with_label("Quit");
  
  gtk_widget_set_usize(loginwindow->entry_sn, 80, 0);
  
  /* what's the limit??? */
  loginwindow->entry_pwd = gtk_entry_new_with_max_length(15); 
  if (gtkfaim_config.password != NULL)
    gtk_entry_set_text(GTK_ENTRY(loginwindow->entry_pwd),
		       gtkfaim_config.password);
  
  gtk_widget_set_usize(loginwindow->entry_pwd, 80, 0);
  gtk_entry_set_visibility(GTK_ENTRY(loginwindow->entry_pwd), FALSE);
  
  
  gtk_box_pack_start(GTK_BOX(loginwindow->vbox1), loginwindow->hbox1, 
		     FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(loginwindow->vbox1), loginwindow->hbox2, 
		     FALSE, FALSE, 0);
  
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox1), loginwindow->vbox2, 
		     FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(loginwindow->vbox2), 
		     logobox, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(loginwindow->vbox2), 
		     loginwindow->banner_label, FALSE, FALSE, 0);
  
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox2), loginwindow->vbox3, 
		       FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(loginwindow->vbox3), loginwindow->hbox3, 
		     FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(loginwindow->vbox3), loginwindow->hbox4, 
		     TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(loginwindow->vbox3), loginwindow->hbox5, 
		     TRUE, TRUE, 10);
  
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox3), loginwindow->label_sn, 
		     TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox3), loginwindow->entry_sn, 
		     TRUE, TRUE, 1);
  
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox4), loginwindow->label_pwd, 
		     TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox4), loginwindow->entry_pwd, 
		     TRUE, TRUE, 1);
  
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox5), 
		     loginwindow->button_login, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(loginwindow->hbox5), 
		     loginwindow->button_quit, TRUE, TRUE, 1);
  
  
  gtk_signal_connect(GTK_OBJECT(loginwindow->button_login), "clicked", 
		     GTK_SIGNAL_FUNC(gtkfaim_login_login_click), NULL);
  gtk_signal_connect(GTK_OBJECT(loginwindow->button_quit), "clicked", 
		     GTK_SIGNAL_FUNC(gtkfaim_login_quit_click), NULL);
  
#ifdef GNOME
  gnome_app_set_contents(GNOME_APP(loginwindow->window), loginwindow->vbox1);
#else
  gtk_container_add(GTK_CONTAINER(loginwindow->window), 
		    loginwindow->vbox1);
#endif
  
  gtk_widget_show(loginwindow->button_login);
  gtk_widget_show(loginwindow->button_quit);
  gtk_widget_show(loginwindow->entry_sn);
  gtk_widget_show(loginwindow->entry_pwd);
  gtk_widget_show(loginwindow->label_sn);
  gtk_widget_show(loginwindow->label_pwd);
  gtk_widget_show(loginwindow->banner_label);
  gtk_widget_show(loginwindow->hbox5);
  gtk_widget_show(loginwindow->hbox4);
  gtk_widget_show(loginwindow->hbox3);
  gtk_widget_show(loginwindow->hbox2);
  gtk_widget_show(loginwindow->hbox1);
  gtk_widget_show(loginwindow->vbox3);
  gtk_widget_show(loginwindow->vbox2);
  gtk_widget_show(loginwindow->vbox1);
  gtk_widget_show(loginwindow->window);
  
  gtkfaim_loginwindow = loginwindow;
}

int gtkfaim_parse_login_phase3d_f(struct command_rx_struct *command)
{
  char *buddies = NULL;
  /* this is the new profile */
  char profile[] = "No profile specified.";

  buddies = gtkfaim_buddylist_create_array();

  /* send the buddy list and profile (required, even if empty) */
#if debug > 1
  GTKFAIM_DEBUG("login", "doing buddylist packet...", TRUE);
#endif
  if (gtkfaim_config.profile_string != NULL)
    aim_send_login_phase3c_hi(buddies, gtkfaim_config.profile_string); /* conn2: send H-I*/
  else
    aim_send_login_phase3c_hi(buddies, profile);

#if debug > 1
  GTKFAIM_DEBUG("login", "done with buddylist packet.", TRUE);
#endif

  g_free(buddies);

  /* send final login command (required) */
#if debug > 1
  GTKFAIM_DEBUG("login", "sending Client Ready", TRUE);
#endif
  aim_send_login_phase4_a(); /* conn2: send 120b A*/  

  /* you should now be properly logged on */
#if debug > 1
  GTKFAIM_DEBUG("login", "Now officially online.", TRUE);
#endif

  if( gtkfaim_config.onlogin && !fork() )
    {
      /* Child */
#if debug > 0
      GTKFAIM_DEBUG2("event", "Attempting to exectute onlogin", gtkfaim_config.onlogin, TRUE);
      GTKFAIM_DEBUG2("event", "\targv[1]=", aim_connection.sn, TRUE);
#endif
      execlp( gtkfaim_config.onlogin, 
	      /* FIXME: argv[0] should give pathname. */ 
	      gtkfaim_config.onlogin, aim_connection.sn, NULL );
      /* if we still exist, there must of been an error */
      GTKFAIM_DEBUG( "event", "error executing onlogin:", FALSE );
      perror( NULL );
      exit( 0 );
    }
  
  /* bring up the main window (buddy list) */
#if debug > 1
  GTKFAIM_DEBUG("buddylist", "creating buddy window...", TRUE);
#endif
  gtkfaim_window_buddies_create();
#if 0
  GTKFAIM_DEBUG("buddylist", "done with buddy window. returning 0", TRUE);
#endif

  return 0;
}
