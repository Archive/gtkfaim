/*
  -------------------
  gtkFAIM vX.XXxx
  ------------------- 

  This is it.  There very first complete AIM clone ever attempted (by me).
  It's coming along quite well, if you ask me.  It still needs
  alot of work (both on the surface and down further).  But,
  it's not very old yet, and there's no reason to expect it
  to be anywhere near perfect at this point.  It's slow when
  you're the first one!  

  Features that should be easily implementable:
    * Avoid adding non-buddies to the buddy list if you get a message
      from them.  This has been partially fixed by adding the
      invisible "Temporary" group and a new field (temponly) to the
      buddy_struct.  Hopefully, this is enough.
    * Basic HTML interpretation and, someday, rendering.  At the
      moment, all known HTML tags get tossed away, but the hooks
      are there for future use.  The exceptions to the throw-away
      rule: the typeface tags <b>, <u> and <i>.  They're left in
      so you can interpret them as the user.
      TODO: size changes and attribute (UIB) changes
    * Fix many of the stupid focus problems in the IM windows,
      and the lack of automatic scrolling in the conversation
      window.

  Other features that should get added soon:
    * Ability to dynamically add and remove from the buddy list
      at run time.
    * Make the menus work.
    * Add a context menu for right-clicking on a buddy

 */

#include <gtk/gtk.h>
#include <aim.h>
#include <gtkfaim.h>

#include <ctype.h> /* for sn-related routines */
#include <stdlib.h>

#include <signal.h>

/* holder of ptrs for buddy window */
gtkfaim_window_buddies_t gtkfaim_window_buddies;

gtkfaim_window_nonbuddy_getsn_t    *gtkfaim_nonbuddy_getsn    = NULL;
gtkfaim_window_nonbuddy_getinfo_t  *gtkfaim_nonbuddy_getinfo  = NULL;
gtkfaim_window_nonbuddy_addbuddy_t *gtkfaim_nonbuddy_addbuddy = NULL;
gtkfaim_window_nonbuddy_addgroup_t *gtkfaim_nonbuddy_addgroup = NULL;

u_long snac_outstanding = 0x00000000;
u_long snac_last = 0x00000000;

int gtkfaim_away = FALSE;

#ifdef GNOME
static gint save_state  (GnomeClient         *client,
				 gint                 phase,
				 GnomeRestartStyle    save_style,
				 gint                 shutdown,
				 GnomeInteractStyle   interact_style,
				 gint                 fast,
				 gpointer             client_data);

static gint die         (GnomeClient         *client,
			     gpointer             client_data);

int restarted = 0;   /* This is for session state */

int just_exit = 0;  /* Use this if for parser to see if we have to work */

/*  What the parser understands */
struct poptOption prog_options[] = {
  {"discard-session", 'd', POPT_ARG_STRING, &just_exit, 0,
   N_("Discard session"), N_("ID")},
  {NULL, '\0', 0, NULL, 0}
};

#endif

int main (int argc, char *argv[])
{
  rxcallback_t gtkfaim_callbacks[] = {
    gtkfaim_parse_incoming_im, /* incoming IM */
    gtkfaim_parse_oncoming_buddy, /* oncoming buddy */
    gtkfaim_parse_offgoing_buddy, /* offgoing buddy */
    gtkfaim_parse_missed_im, /* last IM was missed 1 */
    gtkfaim_parse_missed_im, /* last IM was missed 2 */
    NULL, /* login phase 4 packet C command 1*/
    NULL, /* login phase 4 packet C command 2 */
    NULL, /* login phase 2, first resp */
    NULL, /* login phase 2, second resp */
    NULL, /* login phase 3 packet B */
    NULL, /* login phase 3D packet A */
    NULL, /* login phase 3D packet B */
    NULL, /* login phase 3D packet C */
    NULL, /* login phase 3D packet D */
    NULL, /* login phase 3D packet E */
    gtkfaim_parse_login_phase3d_f, /* login phase 3C packet F */
    gtkfaim_parse_snacerror, /* last command bad */
    gtkfaim_parse_missed_im, /* missed some messages */
    NULL, /* completely unknown command */
    gtkfaim_parse_userinfo,
    gtkfaim_parse_search_address,
    gtkfaim_parse_search_name,
    gtkfaim_parse_search_fail,
    0x00
  };
#ifdef GNOME
/* Use the gnome startup */
GnomeClient *client;

gnome_init_with_popt_table("gtkFAIM", "0x0.0c", argc, argv, prog_options, 0, NULL);

if (just_exit)  {

  gnome_config_clean_file(just_exit);
  gnome_config_sync();
  
  gnome_client_disable_master_connection();
  
}

client= gnome_master_client ();

gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
		    GTK_SIGNAL_FUNC (save_state), (gpointer) argv[0]);
gtk_signal_connect (GTK_OBJECT (client), "die",
		    GTK_SIGNAL_FUNC (die), NULL);

if (GNOME_CLIENT_CONNECTED (client))
{
  GnomeClient *cloned= gnome_cloned_client ();
  if (cloned)
  {
    restarted= 1;
    g_print("Should reload our data here...\n");
  }
}

if (! just_exit)
{

if (restarted) {
/* if we've restarted, set our options */
}
#else
  gtk_init(&argc, &argv);
#endif

  signal(SIGSEGV, SIG_DFL);  /* override gtk/glib */
  signal(SIGBUS, SIG_DFL);  /* override gtk/glib */

  gtkfaim_imwindowlist = g_list_alloc();

  gtkfaim_read_config();
  
  gtkfaim_loadgtkrc(); /* reads in stuff for gtk */

  if (gtkfaim_config.autologin)
    {
      if ((gtkfaim_config.screenname == NULL) || (gtkfaim_config.password == NULL))
	{
	  GTKFAIM_DEBUG("main", "\a***conflicting options: autologin is true, yet no sn and/or no password specified!", TRUE);
	  return -1;
	}
    }

  aim_register_callbacks(gtkfaim_callbacks);

  if( !gtkfaim_config.autologin || ( gtkfaim_autologin() < 0 ) )
     gtkfaim_create_loginwindow();

  gtk_main();

  gtkfaim_write_config();

  /* this is a stupid place to have this */
  g_free(gtkfaim_config.screenname);
  g_free(gtkfaim_config.password);

  return 0;
}
#ifdef GNOME
}
#endif

/*
  This is what gets called when gtk thinks we have data on a socket.  Use
  same procedure as if it was in a select() loop.
 */
void datawaiting_callback(gpointer data, gint source, GdkInputCondition condition)
{
  aim_get_command(); /* fetch a command from the wire */
  aim_rxdispatch(); /* dispatch it/them */
}

/*
  Called when buddy list window gets a delete event.
 */
gint gtkfaim_delete_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{
  /* what should i do here ? */
  gtkfaim_logoff();
  gtk_main_quit();
  /* maybe a quit verify dialog here? */
  return FALSE;
}
/*
  Called when buddy list window gets a destroy even.
 */
void gtkfaim_destroy_event(GtkWidget *widget, gpointer data)
{
  gtkfaim_logoff();
  gtk_main_quit();
}

/*
  Should wipe a GtkText widget clean.  Hopefully without doing anything
  bad.
 */
int gtkfaim_clear_textbox(GtkWidget *widget)
{
  int nchars = 0;

  gtk_text_freeze(GTK_TEXT(widget));
  gtk_text_set_point(GTK_TEXT(widget), 0);
  nchars = gtk_text_get_length(GTK_TEXT(widget));

  gtk_text_forward_delete(GTK_TEXT(widget), nchars);

  gtk_text_thaw(GTK_TEXT(widget));

  return 0;
}

/*
  Coverts a hex numeral in character form into an int.
 */
int gtkfaim_hexchar_to_int(char hex)
{
  if ( (hex >= 0x41) && (hex <= 0x46) )
    return (hex - 0x41) + 0xa; /* is upper-case A-F */
  else if ( (hex >= 0x61) && (hex <= 0x66) )
    return (hex - 0x61) + 0xa; /* is lower-case a-f */
  else if ( (hex >= 0x30) && (hex <= 0x39) )
    return (hex - 0x30); /* is numeric digit */
  else
    return 0;
}

/*
  Inserts text (an string, possibly in HTML) into a GtkText (destwidget).

  There need to be many improvements to this.  Frankly, I don't know how
  to do most of them.  Maybe someone else does.  (ASF)
 */
int gtkfaim_html_insert(GtkWidget *destwidget, char *text)
{
  char *curPtr = NULL;
  int i = 0;
  int j = 0;
  char tmpstr[2] = " ";
  GdkColor bgcolour;
  GdkColor fgcolour;
  GdkColormap *colourmap;
  unsigned int bgcolour_changed = FALSE;
  unsigned int fgcolour_changed = FALSE;

#if debug > 2
  GTKFAIM_DEBUG2("html_insert()", "text: ", text, TRUE);
#endif

  if (text==NULL)
    return -1;

  /* get this widget's colour map */
  colourmap = gtk_widget_get_colormap(destwidget);

  /* since GTK works in 16bit colour, we must shift */
  /*   set default background colour */
  bgcolour.red = (gtkfaim_config.imsg_bgcolor.red << 8);
  bgcolour.blue = (gtkfaim_config.imsg_bgcolor.green << 8);
  bgcolour.green = (gtkfaim_config.imsg_bgcolor.blue << 8);
  gdk_color_alloc(colourmap, &bgcolour);

  /*   set default forground colour */
  fgcolour.red = gtkfaim_config.imsg_fgcolor.red << 8;
  fgcolour.blue = gtkfaim_config.imsg_fgcolor.green << 8;
  fgcolour.green = gtkfaim_config.imsg_fgcolor.blue << 8;
  gdk_color_alloc(colourmap, &fgcolour);

  /* freeze it so we can't see every single update */
  gtk_text_freeze(GTK_TEXT(destwidget));
  
  curPtr = text;

  for (i = 0; (*curPtr) != '\0'; i++)
    {
      if ( *(curPtr) == '<')
	{
	  if (!strncasecmp(curPtr+1, "html", 4))
	    {
	      curPtr += 6;
	    }
	  else if (!strncasecmp(curPtr+1, "/html", 5))
	    {
	      curPtr += 7;
	    }
	  else if (!strncasecmp(curPtr+1, "pre", 3))
	    {
	      curPtr += 5;
	    }
	  else if (!strncasecmp(curPtr+1, "/pre", 4))
	    {
	      curPtr += 6;
	    }
	  else if (!strncasecmp(curPtr+1, "font", 4))
	    {
	      curPtr += 5;
	      j = 1;
	      while ( *(curPtr+j) != '>' )
		{
		  for ( ; (*(curPtr+j) == ' ') && (*(curPtr+j) != '>'); j++)
		    ;
		  if (!strncasecmp(&(curPtr[j]), "back", 4))
		    {
		      for ( ; (*(curPtr+j) != '=') && (*(curPtr+j)!='>'); j++)
			;
		      j += 2; /* opening equals+quote */
		      if (*(curPtr+j) == '#')
			{
			  j++; /* pound sign */
			  /* remember: shift from 8bits to 16bits */
			  bgcolour.red = ( gtkfaim_hexchar_to_int(*(curPtr+j)) << 12);
			  bgcolour.red += ( gtkfaim_hexchar_to_int(*(curPtr+j+1)) << 8);
			  bgcolour.green = ( gtkfaim_hexchar_to_int(*(curPtr+j+2)) << 12);
			  bgcolour.green += gtkfaim_hexchar_to_int(*(curPtr+j+3)) << 8;
			  bgcolour.blue = ( gtkfaim_hexchar_to_int(*(curPtr+j+4)) << 12);
			  bgcolour.blue += gtkfaim_hexchar_to_int(*(curPtr+j+5)) << 8;
			  gdk_color_alloc(colourmap, &bgcolour);
			  bgcolour_changed = TRUE;
			}
#if debug > 0
		      else
			GTKFAIM_DEBUG("html_insert", "unhandled colorname on a back!", TRUE);
#endif
		      for ( ; (*(curPtr+j) != ' ') && (*(curPtr+j)!='>'); j++)
			;
		    }
		  else if (!strncasecmp(&(curPtr[j]), "color", 5))
		    {
		      for ( ; (*(curPtr+j) != '=') && (*(curPtr+j)!='>'); j++)
			;
		      j += 2; /* opening equals+quote */
		      if (*(curPtr+j) == '#')
			{
			  j++; /* octothorpe */
			  /* remember: shift from 8bits to 16bits */
			  fgcolour.red = ( gtkfaim_hexchar_to_int(*(curPtr+j)) << 12);
			  fgcolour.red += ( gtkfaim_hexchar_to_int(*(curPtr+j+1)) << 8);
			  fgcolour.green = ( gtkfaim_hexchar_to_int(*(curPtr+j+2)) << 12);
			  fgcolour.green += gtkfaim_hexchar_to_int(*(curPtr+j+3)) << 8;
			  fgcolour.blue = ( gtkfaim_hexchar_to_int(*(curPtr+j+4)) << 12);
			  fgcolour.blue += gtkfaim_hexchar_to_int(*(curPtr+j+5)) << 8;
			  gdk_color_alloc(colourmap, &fgcolour);
			  fgcolour_changed = TRUE;
			}
#if debug > 0
		      else
			GTKFAIM_DEBUG("html_insert", "unhandled colorname on a fgcolor!", TRUE);
#endif
		      for( ; (*(curPtr+j) != ' ') &&
			     (*(curPtr+j)!='>'); j++)
			;
		    }
		  else if (!strncasecmp(&(curPtr[j]), "ptsize", 6))
		    {
		      for ( ; ( *(curPtr+j) != ' ' ) &&
			      ( *(curPtr+j) != '>'); j++)
			;
#if debug > 0
		      GTKFAIM_DEBUG("html_insert", "unhandled font ptsize change", TRUE);
#endif
		    }
		  else if (!strncasecmp(&(curPtr[j]), "size", 4))
		    {
		      for ( ; ( *(curPtr+j) != ' ' ) &&
			      ( *(curPtr+j) != '>'); j++)
			;
#if debug > 0
		      GTKFAIM_DEBUG("html_insert", "unhandled font size change", TRUE);
#endif
		    }
		  else
		    {
#if debug > 0
		      GTKFAIM_DEBUG2("html_insert", "unknown subtag ", (curPtr+j), TRUE);
#endif
		      j++;
		    }
		}
	      curPtr += j+1;
	    }
	  else if (!strncasecmp(curPtr+1, "body", 4))
	    {
	      curPtr += 5;
	      j = 1;
	      while ( *(curPtr+j) != '>' )
		{
		  for ( ; (*(curPtr+j) == ' ') && (*(curPtr+j) != '>'); j++)
		    ;

		  /* I know that this may not be the proper behavior (it 
		     should be the textbox's background color instead of 
		     just the text's bgcolor), but this seems more logical 
		     for conversations */
		  if ( !strncasecmp(&(curPtr[j]), "bgcolor", 7))
		    {
		      for ( ; (*(curPtr+j) != '=') && (*(curPtr+j)!='>'); j++)
			;
		      j += 2; /* opening equals+quote */
		      if (*(curPtr+j) == '#')
			{
			  j++; /* pound sign */
			  /* remember: shift from 8bits to 16bits */
			  bgcolour.red = ( gtkfaim_hexchar_to_int(*(curPtr+j)) << 12);
			  bgcolour.red += ( gtkfaim_hexchar_to_int(*(curPtr+j+1)) << 8);
			  bgcolour.green = ( gtkfaim_hexchar_to_int(*(curPtr+j+2)) << 12);
			  bgcolour.green += gtkfaim_hexchar_to_int(*(curPtr+j+3)) << 8;
			  bgcolour.blue = ( gtkfaim_hexchar_to_int(*(curPtr+j+4)) << 12);
			  bgcolour.blue += gtkfaim_hexchar_to_int(*(curPtr+j+5)) << 8;
			  gdk_color_alloc(colourmap, &bgcolour);
			  bgcolour_changed = TRUE;
			}
#if debug > 0
		      else
			GTKFAIM_DEBUG("html_insert", "unhandled colorname on a bgcolor!", TRUE);
#endif
		      for ( ; (*(curPtr+j) != ' ') && (*(curPtr+j)!='>'); j++)
			;
		    }
		  else
		    {
#if debug > 0
		      GTKFAIM_DEBUG2("html_insert", "unknown subtag ", (curPtr+j), TRUE);
#endif
		      j++;
		    }
		}
	      curPtr += j+1;
	    }
	  else if (!strncasecmp(curPtr+1, "/font", 5))
	    {
	      if (bgcolour_changed)
		{
		  bgcolour.red = (gtkfaim_config.imsg_bgcolor.red << 8);
		  bgcolour.blue = (gtkfaim_config.imsg_bgcolor.green << 8);
		  bgcolour.green = (gtkfaim_config.imsg_bgcolor.blue << 8);
		  gdk_color_alloc(colourmap, &bgcolour);
		}
	      if (fgcolour_changed)
		{
		  /*   set default forground colour */
		  fgcolour.red = gtkfaim_config.imsg_fgcolor.red << 8;
		  fgcolour.blue = gtkfaim_config.imsg_fgcolor.green << 8;
		  fgcolour.green = gtkfaim_config.imsg_fgcolor.blue << 8;
		  gdk_color_alloc(colourmap, &fgcolour);
		}
	      curPtr += 7;
	    }
	  else if (!strncasecmp(curPtr+1, "/body", 5))
	    {
	      curPtr += 7;
	    }
	  else if (!strncasecmp(curPtr+1, "br", 2))
	    {
	      tmpstr[0] = '\n';
	      tmpstr[1] = '\0';
	      gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, tmpstr, -1);
	      curPtr += 4;
	    }
	  else if (!strncasecmp(curPtr+1, "b", 1))
	    {
#if debug > 0
	      GTKFAIM_DEBUG("html_insert", "unhandled <b> tag", TRUE);
#endif
	      curPtr += 3;
	    }
	  else if (!strncasecmp(curPtr+1, "u", 1))
	    {
#if debug > 0
	      GTKFAIM_DEBUG("html_insert","unhandled <u> tag", TRUE);
#endif
	      curPtr += 3;
	    }
	  else if (!strncasecmp(curPtr+1, "i", 1))
	    {
#if debug > 0
	      GTKFAIM_DEBUG("html_insert", "unhandled <i> tag", TRUE);
#endif
	      curPtr += 3;
	    }
	  else if (!strncasecmp(curPtr+1, "/b", 2))
	    {
	      curPtr += 4;
	    }
	  else if (!strncasecmp(curPtr+1, "/u", 2))
	    {
	      curPtr += 4;
	    }
	  else if (!strncasecmp(curPtr+1, "/i", 2))
	    {
	      curPtr += 4;
	    }
	  else if (!strncasecmp(curPtr+1, "hr", 2))
	    {
	      gtkfaim_html_insert(destwidget, "<br>---<br>");
	      curPtr += 4;
	    }
	  else
	    {
	      tmpstr[0] = *(curPtr);
	      tmpstr[1] = '\0';
	      gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, tmpstr, -1);
	      curPtr++;
	    }
	}
      else if ( *(curPtr) == '>')
	{
	  tmpstr[0] = *(curPtr);
	  tmpstr[1] = '\0';
	  gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, tmpstr, -1);
	  curPtr++;
	}
      else if ( *(curPtr) == '&')
	{
	  curPtr++; /* skip over & */
	  if (strncmp(curPtr, "lt;", 3) == 0)
	    {
	      gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, "<", -1);
	      curPtr += 3;
	    }
	  else if (strncmp(curPtr, "gt;", 3) == 0)
	    {
	      gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, ">", -1);
	      curPtr += 3;
	    }
	  else if (strncmp(curPtr, "amp;", 4) == 0)
	    {
	      gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, "&", -1);
	      curPtr += 4;
	    }
	  /* let a non-entity amp slip through... */
	  else
	    {
	      gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, "&", -1);
	    }
	}
      else
	{
	  tmpstr[0] = *(curPtr);
	  tmpstr[1] = '\0';
	  gtk_text_insert(GTK_TEXT(destwidget), NULL, &fgcolour, &bgcolour, tmpstr, -1);
	  curPtr++; /* pointer increment */
	}
    }

  {
    /* force a scroll-to-bottom -- is this proper?? */
    gfloat newadj = 0;
    newadj = GTK_TEXT(destwidget)->vadj->upper;
    gtk_adjustment_set_value(GTK_TEXT(destwidget)->vadj, GTK_TEXT(destwidget)->vadj->upper - 1);
  }

  gtk_text_thaw(GTK_TEXT(destwidget));

  return 0;
}

void gtkfaim_logoff(void)
{
#if debug > 1
  GTKFAIM_DEBUG("signoff", "signing off...", TRUE);
#endif
  aim_logoff();

  /* Do the onlogoff */

  if( gtkfaim_config.onlogoff && !fork() )
    {
      /* Child */
      execlp( gtkfaim_config.onlogoff, 
	      /* FIXME: argv[0] should give pathname. */ 
	      gtkfaim_config.onlogoff, aim_connection.sn, NULL );
      /* if we still exist, there must of been an error */
  perror( "gtkfaim: executing onlogoff action" );
  exit( 1 );
    }


  return;
}

void gtkfaim_genericok_cb(GtkWidget *widget, gpointer data)
{
   GtkSignalFunc func;
   
   func = (GtkSignalFunc)gtk_object_get_data(GTK_OBJECT(widget), "genericok_callback");
   if( func )
      (*func)();

   if( data )
   {
      gtk_widget_hide(GTK_WIDGET(data));
      gtk_widget_destroy(GTK_WIDGET(data));
   }
}

void gtkfaim_genericok_signal( char *msg, GtkSignalFunc func )
{
   GtkWidget *window;
#ifndef GNOME
   GtkWidget *button;
   GtkWidget *label;
   GtkWidget *vbox;
#endif

   if( !msg )
      return;

#ifdef GNOME
   window = gnome_ok_dialog(msg);
#else
   window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

   gtk_window_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  
   vbox = gtk_vbox_new(FALSE, 2);
   label = gtk_label_new(msg);
   button = gtk_button_new_with_label("Ok");
  
   gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(gtkfaim_genericok_cb), window);
   gtk_object_set_data(GTK_OBJECT(button), "genericok_callback", func);

   gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, TRUE, 2);
   gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 2);

   gtk_container_add(GTK_CONTAINER(window), vbox);
   gtk_widget_show(label);
   gtk_widget_show(button);
   gtk_widget_show(vbox);
#endif
   gtk_widget_show(window);
}

void gtkfaim_genericok( char *msg )
{
   gtkfaim_genericok_signal( msg, NULL );
}

int gtkfaim_parse_missed_im(struct command_rx_struct *command)
{
  if ( (command->data[0] == 0x00) &&
       (command->data[1] == 0x01) &&
       (command->data[2] == 0x00) &&
       (command->data[3] == 0x0a) )
    {
      gtkfaim_genericok("It is recommend that you STOP SENDING/RECIEVING MESSAGES SO FAST!");
    }
  else if ( (command->data[0] == 0x00) &&
	    (command->data[1] == 0x04) &&
	    (command->data[2] == 0x00) &&
	    (command->data[3] == 0x01) )
    {
#ifdef USE_SNAC_FOR_IMS
      u_long snacid = 0x00000000;
      struct aim_snac_t *snac = NULL;
      
      snacid = ((command->data[6] << 24) & 0xFF000000);
      snacid += ((command->data[7] << 16)& 0x00FF0000);
      snacid += ((command->data[8] <<  8)& 0x0000FF00);
      snacid += ((command->data[9] )     & 0x000000FF);
      
      snac = aim_remsnac(snacid);
      if (snac)
	{
	  if (snac->data)
	    {
	      gtkfaim_imwindow_t *imwinlist = NULL;
	      char sentance[255] = "<hr>User ";
	      char sentance2[] =" not available.<hr>";
	      strcat(sentance, (char *)snac->data);
	      strcat(sentance, sentance2);
	      
	      if ((imwinlist = gtkfaim_search_imwindow((char *)snac->data)))
		gtkfaim_html_insert(imwinlist->textbox_conversation, sentance);
	      else
		gtkfaim_genericok(sentance);

	      free(snac->data);
	    }
	   else
	    {
	      g_print("PARSE ERROR:  SNAC had no data\n");
	    }
	  free(snac);
	}
      else
        {
          g_print("PARSE ERROR: No SNAC to parse\n");
        }
#else
      gtkfaim_genericok("Something tells me that your LAST IM DIDN\'T MAKE IT BECAUSE THE BUDDY IS NOT ONLINE");
#endif 
    }
  else if ( (command->data[0] == 0x00) &&
	    (command->data[1] == 0x04) &&
	    (command->data[2] == 0x00) &&
	    (command->data[3] == 0x0a) )
    {
      char msg1[255] = "You missed some messages from ";
      char msg2[] = " because they were sent too fast.";
      strcat(msg1, &(command->data[13]));
      strcat(msg1, msg2);

      gtkfaim_genericok(msg1);
    }

  return 0;
}

#ifdef GNOME
static gint
save_state (GnomeClient          *client,
	    gint                  phase,
	    GnomeRestartStyle     save_style,
	    gint                  shutdown,
	    GnomeInteractStyle    interact_style,
	    gint                  fast,
	    gpointer              client_data)
{
  gchar *session_id;
  gchar *argv[3];
  gint x,y,w,h;

  session_id= gnome_client_get_id (client);

  /* TODO:  do whatever we have to do for session stuff in here... or call the functions
     to do it all.  */
  
  return TRUE;
}

static gint
die (GnomeClient        *client,
     gpointer            client_data)
{
  gtk_exit(0);
  
  return FALSE;
} 
#endif
