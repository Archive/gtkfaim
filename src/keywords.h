
FAIM_CONFIG_STRING(screenname, NO_DEFAULT, 14, GTKFAIM_GLOBAL|GTKFAIM_READ_ONLY,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# User Information")
      FAIM_COMMENT("#  -- NOTE: even if autologin is not enabled, the values listed here")
      FAIM_COMMENT("#          for screenname and password WILL BE LOADED INTO the login")
      FAIM_COMMENT("#          window regardless.  You will not be able to see the password")
      FAIM_COMMENT("#          when it's loaded, leading to possible false invalid password")
      FAIM_COMMENT("#          errors.  Comment both of these lines out if in doubt.")
      FAIM_COMMENT("# screen name (only required for autologin)")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(password, NO_DEFAULT, 14, GTKFAIM_GLOBAL|GTKFAIM_READ_ONLY,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# unencrypted password (only required for autologin)")
   FAIM_COMMENT_END
)

FAIM_CONFIG_BOOLEAN(autologin, FALSE, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# autologin =")
      FAIM_COMMENT("#  1 - login using specified sn/pswd WITHOUT ASKING FIRST")
      FAIM_COMMENT("#  0 - always ask (bring up login window)")
   FAIM_COMMENT_END
)

FAIM_CONFIG_BOOLEAN(readonly, FALSE, GTKFAIM_LOCAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# readonly =")
      FAIM_COMMENT("#  1 - do not write out configuration on exit")
      FAIM_COMMENT("#  0 - write out configuration on exit")
   FAIM_COMMENT_END
)

FAIM_CONFIG_BOOLEAN(sendonenter, FALSE, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# sendonenter =")
      FAIM_COMMENT("#  1 - send IM when enter key is pressed (\\n)")
      FAIM_COMMENT("#  0 - send IM when ctrl-s or \"Send\" button is pressed")
   FAIM_COMMENT_END
)

FAIM_CONFIG_BOOLEAN(timestamps, TRUE, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# timestamps =")
      FAIM_COMMENT("#  1 - each IM is marked with a timestamp")
      FAIM_COMMENT("#  0 - no timestamps shown")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(gtkrc, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# gtkrc")
      FAIM_COMMENT("#   If you want a gtkFAIM-only gtkrc, specify its full path here")
      FAIM_COMMENT("#     NOTE: If you have a system-wide gtkrc, it will get used even if")
      FAIM_COMMENT("#           you do not specify its name here.  But, a specified gtkrc")
      FAIM_COMMENT("#           will take priority over a system-wide gtkrc!")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(onlogin, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Command to run when logged in.  Run with param #1=SN.")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(onlogoff, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Command to run when logging off.  Run with param #1=SN.")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(oninmessage, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Command to run when an IM is recived.  Param #1=1 if a new window")
      FAIM_COMMENT("# was opened, else 0.  Param #2=the SN from which it was recived.")
      FAIM_COMMENT("# Param #3 is the full text of the message.")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(onoutmessage, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Command to run when an IM is sent.")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(onjoin, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Command to run when a buddy joins.  Param #1=the offical SN of the buddy.")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(onleave, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Command to run when a buddy leaves.  Param #1=the offical SN of the buddy.")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(profile, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Specify where to read your profile from")
   FAIM_COMMENT_END
)

FAIM_CONFIG_STRING(awaymsg, NO_DEFAULT, 0, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# Specify the message sent to users when they message")
      FAIM_COMMENT("#     you while you're away  (currently only one line possible)")
   FAIM_COMMENT_END
)

FAIM_CONFIG_TUPLE(imsg_bgcolor, 0xff, 0xff, 0xff, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# background text color (NOT the background box)")
   FAIM_COMMENT_END
)

FAIM_CONFIG_TUPLE(imsg_fgcolor, 0x02, 0x02, 0x02, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# foreground text color")
   FAIM_COMMENT_END
)

FAIM_CONFIG_TUPLE(imsg_src_bgcolor, 0xff, 0xff, 0xff, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# source SN text bgcolor")
   FAIM_COMMENT_END
)

FAIM_CONFIG_TUPLE(imsg_src_fgcolor, 0x44, 0x44, 0xff, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# source SN text fgcolor")
   FAIM_COMMENT_END
)

FAIM_CONFIG_TUPLE(imsg_dest_bgcolor, 0xff, 0xff, 0xff, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# dest SN text bgcolor")
   FAIM_COMMENT_END
)

FAIM_CONFIG_TUPLE(imsg_dest_fgcolor, 0xff, 0x44, 0xff, GTKFAIM_GLOBAL,
   FAIM_COMMENT_BEGIN
      FAIM_COMMENT("# dest SN text fgcolor")
   FAIM_COMMENT_END
)

